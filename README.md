Ant Design Vue
====
当前最新版本： 0.0.1

Overview
----

基于 [Ant Design of Vue](https://vuecomponent.github.io/ant-design-vue/docs/vue/introduce-cn/) 实现的 Ant Design Pro  Vue 版
采用前后端分离方案，提供强大代码生成器的快速开发平台。



#### 前端技术
 
- 基础框架：[ant-design-vue](https://github.com/vueComponent/ant-design-vue) - Ant Design Of Vue 实现 ant-design-vue ^1.3.16
- JavaScript框架：Vue
- Webpack
- node 10.15.3
- yarn
- eslint


项目下载和运行
----

- 安装依赖
```
yarn install
```

- 开发模式运行
```
yarn run serve
```

- 编译项目
```
yarn run build
```

<!-- - Lints and fixes files
```
yarn run lint
``` -->

目录规范
----
```
public - 项目入口目录,本目录中文件不会被编译压缩
    - index.html - 程序入口文件，唯一

src - 程序资源文件夹
    - api - 接口api管理目录，存放平台或不 常变动的公用api、例如：登录登出api、 权限字典api
      - *.js 具体业务api管理文件
    - assets - 静态资源路径
    - components - 组件库
      - avic - 公用vue组件目录 
        - bpm - 流程类公共组件目录
        - pt - pt提供的公共组件目录
    - config - 常用设置参数文件目录
    - lang 18in1国际化
    - mixins - VUE混入函数管理目录
      - avic 
        - pt 具体业务用
    - mocker - 开发用虚拟API文件目录
    - router - 路由方法管理目录
    - store - VUEX方法管理目录
    - utils - 工具类目录
    - views - 具体页面目录
      - avic   平台页面
      - exception   异常错误页面
      - login   登录页面
```
##### 公用组件存放方式说明
> 与本开发框架相关公用组件直接放在components根目录下,例如Layouts
>其余具体功能类组件按具体功能分类存放

说明文档规范
----
  - 每个单独目录下需要有README.MD对当前目录文件夹、文件的功能进行说明

命名规范
----

>文件或文件夹的命名遵循以下原则：
>
>index.js 或者 index.vue，统一使用小写字母开头的(kebab-case)命名规范
>
>属于组件或类的，统一使用大写字母开头的(PascalCase)命名规范
>
>其他非组件或类的，统一使用小写字母开头的(kebab-case)命名规范

#### 1.组件命名
 >单文件组件文件的大小写强烈推荐单文件组件的文件名应该要么始终是单词大写开头 (PascalCase)，要么始终是横线连接 (kebab-case)。
 >
 >单词大写开头对于代码编辑器的自动补全最为友好，因为这使得我们在 JS(X) 和模板中引用组件的方式尽可能的一致。然而，混用文件命名方式有的时候会导致大小写不敏感的文件系统的问题，这也是横线连接命名同样完全可取的原因。

#### 2. 文件夹命名规范
>属于components文件夹下的组件文件夹，使用大写字母开头的PascalBase风格
>
>1.全局通用的组件放在 /src/components/common下
>
>2.其他业务页面中的组件，放在各自页面下的 ./components文件夹下
>
>3.每个components文件夹下最多只有一层文件夹，且文件夹名称为组件的名称，文件夹下必须有index.vue或index.js，其他.vue文件统一大写开头（Pascal case），components下的子文件夹名称统一大写开头（PascalCase）
>
>4.其他文件夹统一使用kebab-case的风格


#### 全局公共组件：/src/components示例

```
- [components]
    - [AvicCommonSelect]
      - index.vue
    - [Layouts]
      - index.js
```

#### 业务页面内部封装的组件：以 /src/views/avicit/demo/components示例

```
-[src]
  - [views]
    - [avicit]
      - [demo]
        - [layout]
          - [components]
            - [Sidebar]
              - index.vue
              - Item.vue
              - SidebarItem.vue
            - AppMain.vue
            - index.js
            - Navbar.vue`
```

#### index.js 中导出组件方式如下：

```javascript
export { default as AppMain } from './AppMain'
export { default as Navbar } from './Navbar'
export { default as Sidebar } from './Sidebar'
```

>看index.js中最后一行代码，不难发现，为什么components下的子文件夹要使用PascalCase命名：
>
>```javascript
>export { default as Sidebar } from './sidebar' // 使用kebab-case命名的文件夹
>export { default as Sidebar } from './Sidebar' // 使用 PascalCase命名的文件夹
>```
>对于组件的导出/导入，我们一般都是使用大写字母开头的PascalCase风格，
>以区别于.vue组件内部的其他camelCase声明的变量，
>
>[Sidebar]作为【侧边栏组件】的一个整体被导出，文件夹的命名也采用PascalCase，
>有利于index.js中export时的前后统一，避免很多情况下不注意区分大小写
>
>

#### 3. 文件命名规范
##### 3.1. *.js文件命名规范

>
>属于类的.js文件，除index.js外，使用PascalBase风格
>
>其他类型的.js文件，使用kebab-case风格
>
>属于Api的，统一放入api文件夹中管理
>

##### 3.2. *.vue文件命名规范

>除index.vue之外，其他.vue文件统一用PascalBase风格,首字母大写

##### 3.3. *.less文件命名规范
>统一使用kebab-case命名风格

#### 4.views文件夹下的命名规范
##### 4.1 文件夹命名规范
>1.views 下面的文件夹代表着模块的名字由名词组成（car、order、cart）
>
>2.单词只能有一个（good: car order cart）（bad: carInfo carpage）
>
>3.尽量是名词（good: car）（bad: greet good）
>
>4.以小写开头（good: car）（bad: Car）
>
>5.views下模块中的组件依然适用组件命名规范

##### 4.2 views 下的 vue 文件命名
>1.views 下面的 vue 文件代表着页面的名字
>
>2.放在模块文件夹之下
>
>3.只有一个文件的情况下不会出现文件夹，而是直接放在 views 目录下面，如 Login Home
>
>4.尽量是名词
>
>5.大写开头，开头的单词就是所属模块名字（CarDetail、CarEdit、CarList）
>
>6.名字至少两个单词（good: CarDetail）（bad: Car）
>
>7.常用结尾单词有（Detail、Edit、List、Info、Report）
>
>8.以 Item 结尾的代表着组件（CarListItem、CarInfoItem）
>

#### 5.vue 方法命名
##### 5.1 vue 方法放置顺序
```
1.components

2.props

3.data

4.created

5.mounted

6.activited

7.update

8.beforeRouteUpdate

9.metods

10.filter

11.computed

12.watch
```

##### 5.2 method 自定义方法命名
>
>1.动宾短语（good：jumpPage、openCarInfoDialog）（bad：go、nextPage、show、open、login）
>
>2.ajax 方法以 get、post 开头，以 data 结尾（good：getListData、postFormData）（bad：takeData、confirmData、getList、postForm）
>
>3.事件方法以 on 开头（onTypeChange、onUsernameInput）
>
>4.init、refresh 单词除外
>
>5.尽量使用常用单词开头（set、get、open、close、jump）
>
>6.驼峰命名（good: getListData）（bad: get_list_data、getlistData）
>

##### 5.3 data props 方法注意点
>
>1.使用 data 里的变量时请先在 data 里面初始化
>
>2.props 指定类型，也就是 type
>
>3.props 改变父组件数据 基础类型用 $emit ，复杂类型直接改
>
>4.ajax 请求数据用上 isLoading、isError 变量
>
>5.不命名多余数据，现在是详情页、你的数据是 ajax 请求的，那就直接声明一个对象叫 d，而不是每个字段都声明
>
>6.表单数据请包裹一层 form
>

##### 5.4 生命周期方法注意点
>
>1.不在 mounted、created 之类的方法写逻辑，取 ajax 数据，
>
>2.在 created 里面监听 Bus 事件
>

登录流程讲解
----
```
1.进入登录页模板 - src\views\login\Login.vue

2.触发登录方法 
  - src\store\modules\user.js
    - Login()

3.登录成功，用户信息存入

3.路由跳转至首页 
  - src\views\login\Login.vue
    - this.$router.push({ name: "dashboard" })

4.路由权限校验
  - src\permission.js
    - src\store\modules\user.js
      - CheckPermissionStatue() 路由是否更新
      或者
      - GetPermissionList() 获取路由字典

5.进入首页\权限不足引导登出
```

VUEX默认Getter说明
----
> src\store\getter.js
>
>为方便开发和数据调用，全局Store中已内置的对象有
```javascript
const getters = {
  device: state => state.app.device, //设备信息
  theme: state => state.app.theme, //当前生效主题皮肤
  color: state => state.app.color,//当前生效主题色
  token: state => state.user.token,//用户token信息
  avatar: state => {state.user.avatar = Vue.ls.get(USER_INFO).avatar; return state.user.avatar}//用户头像信息,
  username: state => state.user.username,//用户名
  nickname: state => {state.user.realname = Vue.ls.get(USER_INFO).realname; return state.user.realname}//用户真实姓名(如果有),
  welcome: state => state.user.welcome//欢迎信息,
  permissionList: state => state.user.permissionList//权限路由列表,
  userInfo: state => {state.user.info = Vue.ls.get(USER_INFO); return state.user.info}//用户信息(全部),
  addRouters: state => state.permission.addRouters//接口获取的新路由信息(除预设路由之外的)
}
```

路由菜单说明
----
#### 配置文件路径
@/config/router.config.js
  - [array]asyncRouterMap 预设权限菜单
  - [array]constantRouterMap 预设基础路由(白名单)

#### 路由path命名尽量简洁，遵循RestFul可读原则, 所有字母全小写
>(推荐)   "path": "treetable/treetablemanage"
>
>(不推荐)  "path": "/avic/pt/system/treeTable/TreeTableManager"
>

#### 具体规则和格式可参考 - [src\router\README.md](src\router\README.md)

>`!!! 推荐在src\mocker\permission-list.js中配置json格式权限路由字典，以便统一管理,
>
>mock中文件只在开发环境有用，将来推送权限更改至平台同步时将会用到 !!!`
>

全局常量说明
----
#### 全局公用API网关
##### 设置位置
  - public\index.html
```javascript
window._CONFIG = {};
window._CONFIG['domainURL'] = 'http://localhost:8080/';
window._CONFIG['imgDomainURL'] = 'http://localhost:8080/sys/common/view';
```
##### 使用方式
  - 全局api请勿使用绝对地址方式，推荐使用api常量+相对地址方式请求
```javascript
const api = window._CONFIG['domainURL'] + "api/dosomething?key=1111"
```

##### 环境变量配置说明
  - .env.development
    - 开发环境变量管理
  - .env.production
    - 发布环境变量管理
  - .env.test
    - 测试环境变量管理
>public/index.html中使用环境变量请先在vue.config.js中的HtmlWebpackPlugin对象中注册

其他说明
----

- 项目使用的 [vue-cli3](https://cli.vuejs.org/guide/), 请更新您的 cli

- 关闭 Eslint (不推荐) 移除 `package.json` 中 `eslintConfig` 整个节点代码

- 修改 Ant Design 配色，在文件 `vue.config.js` 中，其他 less 变量覆盖参考 [ant design](https://ant.design/docs/react/customize-theme-cn) 官方说明
```javascript
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          /* less 变量覆盖，用于自定义 ant design 主题 */

          'primary-color': '#F5222D',
          'link-color': '#F5222D',
          'border-radius-base': '4px',
        },
        javascriptEnabled: true,
      }
    }
  }
```



附属文档
----
- [Ant Design Vue](https://vuecomponent.github.io/ant-design-vue/docs/vue/introduce-cn)

- [报表 viser-vue](https://viserjs.github.io/demo.html#/viser/bar/basic-bar)

- [Vue](https://cn.vuejs.org/v2/guide)

- 其他待补充...


备注
----

> @vue/cli 升级后，eslint 规则更新了。由于影响到全部 .vue 文件，需要逐个验证。既暂时关闭部分原本不验证的规则，后期维护时，在逐步修正这些 rules