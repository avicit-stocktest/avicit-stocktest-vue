﻿const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

function resolve(dir) {
  return path.join(__dirname, dir);
}

const CopyWebpackPlugin = require('copy-webpack-plugin');

/* 启动本地mock服务 */
// let apiMocker = require('mocker-api');
// const mocker = path.resolve(__dirname, 'src/mocker/index.js');

// vue.config.js
module.exports = {
  // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
  productionSourceMap: false,
  /*
  pages: {
    index: {
      entry: 'src/main.js',
      chunks: ['chunk-vendors', 'chunk-common', 'index']
    }
  },
  */
  chainWebpack: config => {
    config.resolve.alias
      .set('@public', resolve('public'))
      .set('@$', resolve('src'))
      .set('@config', resolve('src/config'))
      .set('@api', resolve('src/api'))
      .set('@assets', resolve('src/assets'))
      .set('@comp', resolve('src/components'))
      .set('@views', resolve('src/views'))
      .set('@theme', resolve('src/views/avic/pt/portal/theme'))
      .set('@portal', resolve('src/views/avic/pt/portal'))
      .set('@static', resolve('src/static'))
      .set('@mixins', resolve('src/mixins'))
      .set('@utils', resolve('src/utils'));

    if (process.env.NODE_ENV === 'production') {
      // 为生产环境修改配置...process.env.NODE_ENV !== 'development'
      // config.optimization.splitChunks({
      //   chunks: 'async',// async表示抽取异步模块，all表示对所有模块生效，initial表示对同步模块生效
      // })
    } else {
      // 为开发环境修改配置...
    }

    const svgRule = config.module.rule('svg');

    svgRule.uses.clear();

    svgRule.use('vue-svg-loader').loader('vue-svg-loader');

    config.when(process.env.NODE_ENV !== 'development', config => {
      config.optimization.splitChunks({
        chunks: 'all',
        cacheGroups: {
          libs: {
            name: 'chunk-libs',
            test: /[\\/]node_modules[\\/]/,
            priority: 12,
            minChunks: 3,
            chunks: 'all'
          },
          cryptojs: {
            name: 'chunk-crypto-js',
            test: /[\\/]node_modules[\\/]_?crypto-js(.*)/
          },
          xlsx: {
            name: 'chunk-xlsx',
            test: /[\\/]node_modules[\\/]_?xlsx(.*)/
          },
          mxgraph: {
            name: 'chunk-mxgraph',
            test: /[\\/]node_modules[\\/]_?mxgraph(.*)/
          },
          antv: {
            name: 'chunk-antv',
            priority: 21,
            test(module) {
              // module.context 当前文件模块所属的目录 该目录下包含多个文件
              // module.resource 当前模块文件的绝对路径
              if (/antv/.test(module.context)) {
                // let chunkName = ''; // 引用该chunk的模块名字
                // chunks.forEach(item => {
                //   chunkName += item.name + ',';
                // });
                return true;
              }
            }
          },
          vender: {
            name: 'chunk-vender',
            test: /[\\/]node_modules[\\/]/,
            priority: 11,
            chunks: 'initial'
            // chunks:function(chunk){
            //     // 这里的name 可以参考在使用`webpack-ant-icon-loader`时指定的`chunkName`
            //     return chunk.name !== 'antd-icon';
            // },
          },
          commons: {
            name: 'chunk-commons',
            test: resolve('src/components'), // can customize your rules
            minChunks: 2, //  minimum common number
            priority: 2,
            reuseExistingChunk: true
          }
        }
      });
      // config.optimization.runtimeChunk('single')
    });
  },
  configureWebpack: () => {
    if (process.env.NODE_ENV === 'production') {
      // 为生产环境修改配置...
    } else {
      // 为开发环境修改配置...
    }

    return {
      resolve: {
        alias: {
          vue$: 'vue/dist/vue.esm.js' // 使用带编译器的版本解决动态异步组件问题
        }
      },
      plugins: [
        new HtmlWebpackPlugin({
          domainURL: process.env.domainURL,
          BASE_URL: process.env.BASE_URL,
          template: 'public/index.html'
        }),
        new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /zh-cn|en-gb/),
        new CopyWebpackPlugin([
          {
            from: path.resolve(__dirname, './src/assets/'),
            to: 'assets',
            ignore: ['.*']
          }
        ])
      ]
    };
  },
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          /* less 变量覆盖，用于自定义 ant design 主题 */
          /* primary-color为全局主色调，但本框架可以换色，建议不在这里设置primary-color */
          // 'primary-color': '#13c2c2',
          'border-radius-base': '2px'
          // 'border-radius-base': 0
        },
        javascriptEnabled: true
      }
    }
  },

  devServer: {
    port: 3000,
    // 映射关系，越前优先级越高
    proxy: {
      /* 组件配置示例 */
      /*
      '/api/demo/stock': {
      target: 'http://localhost:10008', // mock API接口系统
        ws: false,
        changeOrigin: true
      },
      */
			/* 电子表单服务配置 */
      '/api/eform': {
        target: 'http://127.0.0.1:10087', // 电子表单服务地址
        ws: false,
        changeOrigin: true
      },
      /* 流程服务配置 */
      '/api/bpm': {
        target: 'http://127.0.0.1:10086', // 流程服务地址
        ws: false,
        changeOrigin: true
      },
      /* 系统服务配置 */
      '/api/appsys': {
        target: 'http://127.0.0.1:10088', // 系统服务地址
        ws: false,
        changeOrigin: true
      }
    },
    before(app) {
      // 如需mock才要开启，本地java服务模式请关闭
      /*
      apiMocker(app, mocker, {
        changeHost: true
      });
      */
    }
  },

  // lintOnSave: process.env.NODE_ENV !== 'production'
  lintOnSave: false
};
