
/**
 * 参考注释用，实际规则ide会自动读取.stylelintrc
 * 本处设置默认采用官方推荐规则
 * 官网规则说明 https://stylelint.io/user-guide/rules/list
 * 规则说明中文版 https://cloud.tencent.com/developer/section/1489630
 */
module.exports = {
  extends: ['stylelint-config-standard', 'stylelint-config-prettier'],
  // null (关闭规则)
  rules: {
    // 要求或禁止在注释之前有空行。
    'comment-empty-line-before': null,
    // 要求或禁止在声明语句之前有空行。
    'declaration-empty-line-before': null,
    // 在函数的逗号之后要求有一个换行符或禁止有空白。
    'function-comma-newline-after': null,
    // 指定函数名称的大小写。使用驼峰式大小写，如 translateX。
    'function-name-case': null,
    // 在函数的括号内要求有一个换行符或禁止有空白。
    'function-parentheses-newline-inside': null,
    // 限制函数中相邻的空行数量。
    'function-max-empty-lines': null,
    // 要求在函数之后有空白。
    // 例子：a { transform: translate(1, 1) scale(3); }
    'function-whitespace-after': 'always',
    // 两个空格作为缩进
    indentation: 2,
    // 禁止小于 1 的小数的前导 0。
    // 例子：a { line-height: 0.5; }
    'number-leading-zero': 'always',
    // 禁止数字中的拖尾 0。
    'number-no-trailing-zeros': true,
    // 要求在 at 规则之前有空行。
    'at-rule-empty-line-before':  [
      "always",
      {
        ignoreAtRules: ["import"]
      }
    ],
    // 在关系选择符之后要求有一个空格。
    'selector-combinator-space-after': 'always',
    // 要求选择器列表的逗号之后有一个换行符或禁止在逗号之后有空白。
    'selector-list-comma-newline-after': null,
    // 指定伪元素使用双冒号。
    // 例子：a::before { color: pink; }
    'selector-pseudo-element-colon-notation': 'double',
    // 禁止使用未知单位。
    'unit-no-unknown': true,
    // 限制值列表中相邻空行数量。
    'value-list-max-empty-lines': null,
    // 禁止在字体系列名称列表中缺少通用系列。
    'font-family-no-missing-generic-family-keyword': null,
    // 禁止低优先级的选择器出现在高优先级的选择器之后。
    'no-descending-specificity': null,
    'selector-pseudo-class-no-unknown': null,
    'selector-type-no-unknown': null
  }
};
