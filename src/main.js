import Vue from 'vue';
import 'babel-polyfill';
import 'ant-design-vue/dist/antd.less';
import App from './App.vue';
import Storage from 'vue-ls';
import router from './router';
import store from './store/';

import { VueAxios } from '@/utils/avic/http/request';

import Antd from 'ant-design-vue';
// import Viser from 'viser-vue'

import '@/permission'; // permission control
import '@/utils/avic/common/filter'; // base filter
// import VueApexCharts from 'vue-apexcharts'

import {
  ACCESS_TOKEN,
  // AUTO_LOGIN,
  // DEFAULT_COLOR,
  // DEFAULT_THEME,
  // DEFAULT_LAYOUT_MODE,
  // DEFAULT_COLOR_WEAK,
  // SIDEBAR_TYPE,
  // DEFAULT_FIXED_HEADER,
  // DEFAULT_FIXED_HEADER_HIDDEN,
  // DEFAULT_FIXED_SIDEMENU,
  // DEFAULT_CONTENT_WIDTH_TYPE,
  // DEFAULT_CONTENT_HEIGHT_TYPE,
  // DEFAULT_TOPSIDE_MULTI_TYPE,
  USER_INFO
} from '@/store/mutationTypes';
import config from '@config/defaultSettings';

// import { updateTheme } from '@theme/tools/setting';

import hasPermission from '@/utils/avic/authority/hasPermission';
import draggable from '@/utils/avic/common/drag';
import { disabledAuthFilter, visiableFilter } from '@/utils/avic/authority/authFilter';
// import { i18n } from '@/utils/avic/lang/i18n' // 国际化

require('@assets/styles/skin/skin-loader.less');

// 解决vue-i18n的silent报错问题
window.Vue = Vue;

Vue.config.productionTip = false;
Vue.use(Storage, config.storageOptions);
Vue.use(Antd);
Vue.use(VueAxios, router);
// Vue.use(Viser)
Vue.use(draggable);
Vue.use(hasPermission);

// Vue.use(VueApexCharts)
// Vue.component('apexchart', VueApexCharts)

// console.log('disabledAuthFilter this', disabledAuthFilter)
Vue.prototype.disabledAuthFilter = disabledAuthFilter;
Vue.prototype.visiableFilter = visiableFilter;
Vue.prototype.homePageRoutePath = config.homePageConfig.path;
new Vue({
  router,
  store,
  // i18n,
  mounted() {
    // //读取默认皮肤
    // if (Vue.ls.get(DEFAULT_COLOR)) {
    //   if (Vue.ls.get(DEFAULT_COLOR).theme !== undefined && Vue.ls.get(DEFAULT_COLOR).theme !== 'min-style') {
    //     updateTheme(Vue.ls.get(DEFAULT_COLOR), true);
    //   }
    // }
    // require('@assets/theme/'+themename+'.less')

    // store.commit('SET_SIDEBAR_TYPE', Vue.ls.get(SIDEBAR_TYPE, true))
    // store.commit('TOGGLE_THEME', Vue.ls.get(DEFAULT_THEME, config.navTheme))
    // store.commit('TOGGLE_FIXED_HEADER', Vue.ls.get(DEFAULT_FIXED_HEADER, config.fixedHeader))
    // store.commit('TOGGLE_FIXED_SIDERBAR', Vue.ls.get(DEFAULT_FIXED_SIDEMENU, config.fixSiderbar))
    // store.commit('TOGGLE_CONTENT_WIDTH', Vue.ls.get(DEFAULT_CONTENT_WIDTH_TYPE, config.contentWidth))
    // store.commit('TOGGLE_CONTENT_HEIGHT', Vue.ls.get(DEFAULT_CONTENT_HEIGHT_TYPE, config.contentHeight))
    // store.commit('TOGGLE_TOPSIDE_MULTI', Vue.ls.get(DEFAULT_TOPSIDE_MULTI_TYPE, config.topSideMulti))
    // store.commit('TOGGLE_FIXED_HEADER_HIDDEN', Vue.ls.get(DEFAULT_FIXED_HEADER_HIDDEN, config.autoHideHeader))
    // store.commit('TOGGLE_WEAK', Vue.ls.get(DEFAULT_COLOR_WEAK, config.colorWeak))
    // store.commit('TOGGLE_LAYOUT_MODE', Vue.ls.get(DEFAULT_LAYOUT_MODE, config.layout))
    // store.commit('TOGGLE_COLOR', Vue.ls.get(DEFAULT_COLOR, config.primaryColor))
    store.commit('SET_TOKEN', Vue.ls.get(ACCESS_TOKEN));

    //读取已设置皮肤
    let localCfg = Vue.ls.get(store.state.customConfig.customCfgKeys.G_USER_CFG);
    if (localCfg && localCfg.userId && localCfg.userId === Vue.ls.get(USER_INFO, {}).id) {
      /* 根据个人设置加载主题 */
      let layoutMode =
        localCfg.data[store.state.customConfig.customCfgKeys.G_CFG][
          store.state.customConfig.customCfgKeys.G_GLOBAL_FMT + 'LAYOUT_MODE'
        ];
      if (layoutMode) {
        store.commit('TOGGLE_LAYOUT_MODE', layoutMode);
      } else {
        store.commit('TOGGLE_LAYOUT_MODE', config.layout);
      }
      /* 根据个人设置加载皮肤 */
      let skinColor =
        localCfg.data[store.state.customConfig.customCfgKeys.G_CFG][
          store.state.customConfig.customCfgKeys.G_GLOBAL_FMT + 'SKIN_COLOR'
        ];
      if (skinColor) {
        store.commit('TOGGLE_COLOR', skinColor);
      } else {
        store.commit('TOGGLE_COLOR', config.primaryColor);
      }
    } else {
      store.commit('TOGGLE_COLOR', config.primaryColor);
      store.commit('TOGGLE_LAYOUT_MODE', config.layout);
    }

    /** 全局监听esc事件 **/
    document.addEventListener('keyup', function (e) {
      if (e.keyCode === 27) {
        if (window._CONFIG['permissionChangeAlert'] === true) {
          store.dispatch('Logout').then(() => {
            window._CONFIG['permissionChangeAlert'] = false;
            window.location.reload();
          });
        }
      }
    });
  },
  render: h => h(App)
}).$mount('#app');
