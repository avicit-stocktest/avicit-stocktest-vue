/**
 * 树组件工具类
 */
/**
 * 把平台返回的tree数据转换成 ant.design tree数据
 * @param {*} data  原始数据
 * @param {*} scopedSlot   可通过 slot-scope 属性支持的属性
 * @param {*} scopedSlot   可通过 slot-scope 属性支持的属性
 */
const processTreeData = (data, scopedSlot = {}, custom) => {
  let result = [];
  if (data && data.length > 0) {
    data.map(tim => {
      let tree = {
        id: tim.id,
        nodeType: tim.nodeType,
        title: tim.text,
        key: tim.id,
        isLeaf: !tim.isParent,
        children: tim.children,
        scopedSlots: scopedSlot,
        _parentId: tim._parentId
      };
      if (custom) {
        custom(tree);
      }
      result.push(tree);
    });
  }
  return result;
};
/**
 * 给树列表设置属性
 * @param {*} scopedSlot   可通过 slot-scope 属性支持的属性
 * @param {*} scopedSlot   可通过 slot-scope 属性支持的属性
 */
const setTreeData = (data, scopedSlot = {}, custom) => {
  if (data && data.length > 0) {
    data.map(tim => {
      tim.scopedSlots = scopedSlot;
      if (custom) {
        custom(tim);
      }
    });
  }
  return data;
};
/**
 * 设置树列表的子集
 * @param {*} data
 */
const setTreeChildren = (data, id, childrenData) => {
  if (data.length > 0) {
    data.map(tim => {
      if (tim.id == id) {
        if (tim.children && tim.children.length > 0) {
          tim.children.map(chal => {
            childrenData.map(newChal => {
              if (chal.id == newChal.id) {
                newChal.children = chal.children;
              }
            });
          });
        }
        if (tim.isLeaf) {
          tim.isLeaf = false;
        }
        tim.children = childrenData;
      } else if (tim.children && tim.children.length > 0) {
        setTreeChildren(tim.children, id, childrenData);
      }
    });
  }
};
/**
 * 把平台返回的tree数据转换成 ant.design tree数据 处理包含所有子元素
 * @param {*} data
 * @param {*} scopedSlot   可通过 slot-scope 属性支持的属性
 */
const processTreeByChildrenData = (data, scopedSlot = {}, custom) => {
  if (data.length > 0) {
    let result = [];
    data.map((tim, i) => {
      let children = [];
      if (tim.children.length > 0) {
        children = processTreeByChildrenData(tim.children, custom);
      }
      let tree = {
        id: tim.id,
        nodeType: tim.nodeType,
        title: tim.text,
        key: tim.id,
        isLeaf: !tim.isParent,
        children,
        _parentId: tim._parentId,
        scopedSlots: scopedSlot
      };
      if (custom) {
        custom(tree);
      }
      result.push(tree);
    });
    return result;
  }
};
/**
 * 通过ID 在树结构数据中获取id对应的数据
 * @param {*} tree
 * @param {*} id
 */
const getTreeById = (tree, id) => {
  if (tree && tree.length > 0 && id) {
    for (let i = 0; i < tree.length; i++) {
      const tim = tree[i];
      if (tim.id == id) {
        return tim;
      } else {
        if (tim.children) {
          let res = getTreeById(tim.children, id);
          if (res) {
            return res;
          }
        }
      }
    }
  }
};
export { processTreeData, setTreeChildren, processTreeByChildrenData, getTreeById, setTreeData };
