/**
 * 邮箱
 * @param {*} s
 */
export function isEmail(s) {
  return /^([a-zA-Z0-9._-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(s);
}

/**
 * 手机号码
 * @param {*} s
 */
export function isMobile(value, callback) {
  if (value) {
    let reg = /^((0\d{2,3}-\d{7,8})|(1[3456789]\d{9}))$/;
    if (!reg.test(value)) {
      callback('联系电话有误');
      return;
    }
  }
  callback();
}

/**
 * 电话号码
 * @param {*} s
 */
export function isPhone(s) {
  return /^([0-9]{3,4}-)?[0-9]{7,8}$/.test(s);
}

/**
 * URL地址
 * @param {*} s
 */
export function isURL(s) {
  return /^http[s]?:\/\/.*/.test(s);
}

export function validateCheckboxMaxlength(rule, value, callback, maxLength) {
  if (value) {
    if (typeof value === 'string') {
      if (value.replace(/[^\x00-\xff]/g, '***').length > maxLength) {
        callback('不可超过' + maxLength + '个字符！');
        return;
      }
      // if (value.length > maxLength) {
      //   callback('不可超过' + maxLength + '个字符！');
      //   return;
      // }
    } else {
      if (value.join(',').replace(/[^\x00-\xff]/g, '***').length > maxLength) {
        callback('不可超过' + maxLength + '个字符！');
        return;
      }
      // if (value.join(',').length > maxLength) {
      //   callback('不可超过' + maxLength + '个字符！');
      //   return;
      // }
    }
  }
  callback();
}

export function validateInputMaxlength(rule, value, callback, maxLength) {
  if (value) {
    if (value.replace(/\s+/g, '').length == 0) {
      callback('内容不可为空！');
      return;
    }
    if (value.replace(/[^\x00-\xff]/g, '***').length > maxLength) {
      callback('不可超过' + maxLength + '个字符！');
      return;
    }
    // if (value.length > maxLength) {
    //   callback('不可超过' + maxLength + '个字符！');
    //   return;
    // }
  }
  callback();
}

export function isTestProduct(value, callback) {
  if (value) {
    if (value.indexOf('有效', value.length - '有效'.length) === -1) {
      callback('试验件必须以有效结尾，如【xxx件有效】。');
      return;
    }
  }
  callback();
}
