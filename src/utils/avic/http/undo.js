/**
 * 撤销请求删除
 */
import Vue from 'vue';
import { httpAction } from '@/api/manage';
import { message, Button } from 'ant-design-vue/es';

const base = 'undo_';
let timeout = new Map();
let time = 3000;
/**
 * 添加到 undo 队列中
 * @param {*} id 数据id 必填
 * @param {*} url  请求地址 必填
 * @param {*} oldData  原始数据
 * @param {*} newData  新数据 必填
 * @param {*} content  提示文字
 * @param {*} method  请求类型 get post
 * @param {*} errorMessage  是否显示错误信息
 * @param {*} loading  是否显示加载信息
 * @param {*} loadingLock 加载是否锁屏
 */
const undoSave = (
  id,
  url,
  oldData,
  newData,
  content,
  method = 'get',
  errorMessage = true,
  loading = true,
  loadingLock = true,
  contentType = ''
) => {
  if (!id) {
    throw 'id 是必填字段';
  } else if (!newData) {
    throw 'newData 是必填字段';
  } else if (!url) {
    throw 'url 是必填字段';
  } else if (id instanceof String) {
    throw 'id 只能是字符串';
  }
  let p = new Promise(function (resolve, reject) {
    // 添加浏览器关闭事件监听
    window.addEventListener('beforeunload', closeWindow);
    // bind(window,'beforeunload',closeWindow)
    let data = { oldData, newData: oldData, url };
    Vue.ls.set(base + id, data);
    undoMessage(id, content, reject);
    queue(
      id,
      url,
      newData,
      method,
      resolve,
      reject,
      errorMessage,
      loading,
      loadingLock,
      contentType
    );
  });
  return p;
};
const closeWindow = e => {
  let ev = window.event || e;
  ev.returnValue = '数据正在保存中,您确定要离开当前页面吗？';
};
/**
 * 消息提醒上的撤回按钮
 * @param {*} id
 */
const undoMessage = (id, content = '操作成功', reject) => {
  message.info(
    h =>
      h('span', null, [
        content,
        [
          h(
            Button,
            {
              style: {
                marginLeft: '20px'
              },
              attrs: {
                id,
                size: 'small',
                type: 'primary'
              },
              on: {
                click: e => {
                  e.target.disabled = true;
                  reject(undo(id));
                  // message.destroy()
                }
              }
            },
            '撤回'
          )
        ]
      ]),
    time / 1000
  );
};
// 关闭消息提醒
const close = id => {
  let e = document.getElementById(id);
  if (e) {
    e.parentElement.parentElement.parentElement.parentElement.parentElement.remove();
  }
};
/**
 * 没有撤销发送http请求
 * @param {*} id  数据id
 * @param {*} data  数据
 * @param {*} url  地址
 * @param {*} method  http请求类型
 * @param {*} resolve  成功回掉
 * @param {*} reject 失败回掉
 * @param {*} errorMessage  是否显示错误信息
 * @param {*} loading  是否显示加载框
 * @param {*} loadingLock  加载是否锁屏
 */
const queue = (
  id,
  url,
  data,
  method = 'get',
  resolve,
  reject,
  errorMessage = true,
  loading = true,
  loadingLock = true,
  contentType = ''
) => {
  let out = setTimeout(() => {
    Vue.ls.remove(base + id);
    httpAction(url, data, method, errorMessage, loading, loadingLock, contentType)
      .then(res => {
        if (timeout.size == 0) {
          // 移除浏览器关闭事件监听
          window.removeEventListener('beforeunload', closeWindow);
        }
        resolve(res);
      })
      .catch(response => {
        if (timeout.size == 0) {
          // 移除浏览器关闭事件监听
          window.removeEventListener('beforeunload', closeWindow);
        }
        reject(response);
      });
    timeout.delete(id);
  }, time);
  timeout.set(id, out);
};
/**
 * 撤销方法
 * @param {*} id
 */
const undo = id => {
  if (!id) {
    throw 'id 是必填字段';
  }
  try {
    const original = Vue.ls.get(base + id);
    if (original) {
      let out = timeout.get(id);
      if (out) {
        clearTimeout(out);
      }
      timeout.delete(id);
      Vue.ls.remove(base + id);
      // message.destroy()
      return original.oldData;
    } else {
      console.warn('[undo] 您的请求已经执行,无法在撤销');
    }
  } finally {
    close(id);
    if (timeout.size == 0) {
      // 移除浏览器关闭事件监听
      window.removeEventListener('beforeunload', closeWindow);
    }
  }
};
export { undoSave, undo };
