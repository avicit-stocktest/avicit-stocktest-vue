import { httpAction } from '@api/manage';
/**
 * 通用代码数据统一加载
 * @param params 请求参数
 *        params数据结构: [{"fileName": "fileName1","lookUpType": "PLATFORM_DEPT_TYPE"}, {"fileName": "fileName2","lookUpType": "PLATFORM_SKIN_STATE"}]
 * @param successCallback 获取通用代码成功的回调函数
 *        successCallback(data)中的data数据结构: [{"fileName": "fileName1","value": "通用代码集合1"}, {"fileName": "fileName2","value": "通用代码集合2"}]
 * @param errorCallback 获取通用代码失败的回调函数
 */
let queryUrl = '/api/appsys/lookup/LookupRest/getAllLookUpData/v1'; // 通用代码请求地址
let quertMethod = 'post'; // 通用代码请求方式
const loadLookUp = (params, successCallback, errorCallback) => {
  if (params) {
    httpAction(queryUrl, { params }, quertMethod)
      .then(res => {
        if (res.success && res.result && successCallback) {
          successCallback(res.result.allLookUpData);
        }
      })
      .catch(error => {
        if (errorCallback) {
          errorCallback(error);
        }
      });
  }
};

export { loadLookUp };
