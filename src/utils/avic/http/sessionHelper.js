import { httpAction } from '@api/manage';
/**
 * 获取当前登录用户相关信息
 */
const sessionProcessParam = {
  url: '/api/mms/iqs/iqsdeflection/IqsDeflectionRest/getCurrentInfo/v1',
  method: 'post'
};
/**
 * 异步获取当前登录人
 * @param {*} successCallback
 * @param {*} errorCallback
 */
const getUserAjax = (successCallback, errorCallback) => {
  const user = sessionStorage.getItem('sessionUser');
  if (user == null && typeof user != 'undefined') {
    httpAction(sessionProcessParam.url, null, sessionProcessParam.method)
      .then(res => {
        if (res.success && res.result && successCallback) {
          sessionStorage.setItem('sessionUser', JSON.stringify(res.result.result));
          successCallback(res.result);
        } else {
          if (errorCallback) {
            errorCallback(null);
          }
        }
      })
      .catch(error => {
        if (errorCallback) {
          errorCallback(error);
        }
      });
  } else {
    try {
      successCallback(JSON.parse(user));
    } catch (e) {
      successCallback(null);
    }
  }
};
/**
 * 同步获取当前登录人
 */
const getUser = async () => {
  const user = sessionStorage.getItem('sessionUser');
  if (user == null && typeof user != 'undefined') {
    let result = await httpAction(sessionProcessParam.url, null, sessionProcessParam.method);
    return new Promise(resolve => {
      if (result.success) {
        sessionStorage.setItem('sessionUser', JSON.stringify(result.result));
        resolve(result.result);
      } else {
        resolve(null);
      }
    });
  } else {
    return new Promise(resolve => {
      try {
        resolve(JSON.parse(user));
      } catch (e) {
        resolve(null);
      }
    });
  }
};
export { getUserAjax, getUser };
