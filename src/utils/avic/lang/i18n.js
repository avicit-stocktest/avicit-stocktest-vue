import Vue from 'vue';
import VueI18n from 'vue-i18n';
import msgZh from '@/lang/zh_CN';
// import axios from 'axios'

Vue.use(VueI18n);

const message = {};
message['zh_CN'] = msgZh;

export const i18n = new VueI18n({
  locale: 'zh_CN', // 设置语言环境
  fallbackLocale: 'zh_CN',
  //   silentTranslationWarn: true,
  messages: message // 设置语言环境信息
});

const loadedLanguages = ['zh_CN']; // 我们的预装默认语言

function setI18nLanguage(lang) {
  i18n.locale = lang;
  //设置请求语言格式
  //   axios.defaults.headers.common['Accept-Language'] = lang
  document.querySelector('html').setAttribute('lang', lang);
  return lang;
}

export function loadLanguageAsync(lang) {
  if (i18n.locale !== lang) {
    if (!loadedLanguages.includes(lang)) {
      return import(/* webpackChunkName: "lang-[request]" */ `@/lang/${lang}`).then(msgs => {
        i18n.setLocaleMessage(lang, msgs.default);
        loadedLanguages.push(lang);
        setI18nLanguage(lang);
        return;
      });
    }
    return Promise.resolve(setI18nLanguage(lang));
  }
  return Promise.resolve(lang);
}
