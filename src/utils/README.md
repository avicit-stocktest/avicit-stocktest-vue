## 目录规范

```
utils
  -avic
    -common
      format.js(格式化)
      eventBus.js(总线)
      formDataPretreatment.js(表单数据预处理)
      treeUtils.js(树数据处理)
      drag.js(弹窗拖拽)
      filter.js(过滤)
      validate.js(校验)
      util.js
    -authority
      authFilter.js(表单字段编辑权限)
      hasPermission.js(按钮、表格字段显隐权限)
    -bpm
      bpmUtils.js(流程工具类)
    -http
      loadLookUp.js (通用代码)
      request.js (http 请求)
      sessionHelper.js (当前登录人信息)
      undo.js (撤销)
      axios.js(封装axios)
    -layout
      baseMouldStyles.js(表单布局及样式控制)
      device.js
      mixin.js
    -lang
     i18n.js(多语言)
```
