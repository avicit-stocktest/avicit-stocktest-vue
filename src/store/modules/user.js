import Vue from 'vue';
import { logout } from '@/api/manage';
import {
  ACCESS_TOKEN,
  ACCESS_KEY_ID,
  ACCESS_KEY_SECRET,
  USER_INFO,
  USER_NAME,
  USER_PERMISSIONLIST,
  AUTO_LOGIN
} from '@/store/mutationTypes';
// import { welcome } from '@/utils/avic/common/util'
import config from '@config/defaultSettings';
import { checkPermissionStatue, login, sessionIdLogin, queryPermissionsByUser } from '@/api/api';

const user = {
  state: {
    token: '',
    username: '',
    realname: '',
    // welcome: '',
    avatar: '',
    permissionList: [],
    info: {},
    unreadCount: 0
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_NAME: (
      state,
      {
        username,
        realname
        // welcome
      }
    ) => {
      state.username = username;
      state.realname = realname;
      // state.welcome = welcome
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar;
    },
    SET_PERMISSIONLIST: (state, permissionList) => {
      state.permissionList = permissionList;
    },
    SET_INFO: (state, info) => {
      state.info = info;
    },
    SET_COUNT: (state, info) => {
      state.unreadCount = info;
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo)
          .then(response => {
            if (response.code === '200') {
              const result = response.result;
              let userInfo = result.userInfo;
              userInfo[ACCESS_KEY_ID] = '';
              userInfo[ACCESS_KEY_SECRET] = '';
              if (result.accessKey !== undefined) {
                userInfo[ACCESS_KEY_ID] = result.accessKey[ACCESS_KEY_ID];
                userInfo[ACCESS_KEY_SECRET] = result.accessKey[ACCESS_KEY_SECRET];
              }
              Vue.ls.set(ACCESS_TOKEN, result.token, config.tokenTime);
              Vue.ls.set(USER_NAME, userInfo.username, 7 * 24 * 60 * 60 * 1000);
              Vue.ls.set(USER_INFO, userInfo, 7 * 24 * 60 * 60 * 1000);
              commit('SET_TOKEN', result.token);
              commit('SET_INFO', userInfo);
              commit('SET_NAME', {
                username: userInfo.username,
                realname: userInfo.realname
                // welcome: welcome()
              });
              commit('SET_AVATAR', userInfo.avatar);
              resolve();
            } else {
              reject(response);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },

    // 免密登录
    AuthLogin({ commit }, query) {
      return new Promise((resolve, reject) => {
        sessionIdLogin(query)
          .then(response => {
            if (response.code === '200') {
              const result = response.result;
              const userInfo = result.userInfo;
              userInfo[ACCESS_KEY_ID] = '';
              userInfo[ACCESS_KEY_SECRET] = '';
              if (result.accessKey !== undefined) {
                userInfo[ACCESS_KEY_ID] = result.accessKey[ACCESS_KEY_ID];
                userInfo[ACCESS_KEY_SECRET] = result.accessKey[ACCESS_KEY_SECRET];
              }
              Vue.ls.set(ACCESS_TOKEN, result.token, config.tokenTime);
              Vue.ls.set(USER_NAME, userInfo.username, 7 * 24 * 60 * 60 * 1000);
              Vue.ls.set(USER_INFO, userInfo, 7 * 24 * 60 * 60 * 1000);
              commit('SET_TOKEN', result.token);
              commit('SET_INFO', userInfo);
              commit('SET_NAME', {
                username: userInfo.username,
                realname: userInfo.realname
                // welcome: welcome()
              });
              commit('SET_AVATAR', userInfo.avatar);
              resolve();
            } else {
              reject(response);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },

    // 设置本地权限字典
    SetLocalPermission({ commit }, menuData) {
      commit('SET_PERMISSIONLIST', menuData);
    },

    // 检查权限变化
    CheckPermissionStatue(state) {
      return new Promise((resolve, reject) => {
        let username = Vue.ls.get(USER_NAME);
        let params = {
          username
        };

        checkPermissionStatue(params)
          .then(response => {
            if (response.success == false) {
              Vue.ls.remove(USER_PERMISSIONLIST);
              state.permissionList = [];

              resolve();
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },

    // 获取用户信息
    GetPermissionList({ commit }) {
      return new Promise((resolve, reject) => {
        let username = Vue.ls.get(USER_NAME);
        let params = {
          username
        };

        queryPermissionsByUser(params)
          .then(response => {
            const menuData = response.result;
            if (!menuData.length) {
              // 保证无权限用户也能看到首页
              menuData.push({
                children: [],
                component: config.homePageConfig.component,
                meta: {
                  title: config.homePageConfig.title,
                  keepAlive: null,
                  icon: config.homePageConfig.icon,
                  url: null,
                  menuButtonCount: 0,
                  permissionList: []
                },
                name: config.homePageConfig.name,
                path: config.homePageConfig.path,
                redirect: null,
                route: null,
                sort: 1
              });
              // reject('getPermissionList: permissions must be a non-null array !')
            }
            // 如果用户是超级管理员 就给丫玩一下菜单
            // if(config.superAdmin.indexOf(username) >= 0){
            //   menuData.push({
            //     children: [],
            //     component: 'avic/pt/system/menu/MenuTreeTable',
            //     meta: {title: "菜单管理", keepAlive: null, icon: null, url: null,menuButtonCount: 0, permissionList: []},
            //     name: "menuManageSuper",
            //     path: "/menu/manageSuper",
            //     redirect: null,
            //     route: null,
            //     sort: 1
            //   })
            //   menuData.push({
            //     children: [],
            //     component: 'avic/pt/system/portLet/portalConfig/PortalConfigDesignManage',
            //     meta: {title: "多栏目门户", keepAlive: null, icon: null, url: null,menuButtonCount: 0, permissionList: []},
            //     name: "PortalConfigDesignManage",
            //     path: "/PortalConfigDesignManage",
            //     redirect: null,
            //     route: null,
            //     sort: 1
            //   })
            // }
            const userObj = {};
            userObj[username] = menuData;
            Vue.ls.set(USER_PERMISSIONLIST, userObj);
            commit('SET_PERMISSIONLIST', menuData);
            resolve(response);
          })
          .catch(error => {
            reject(error);
          });
      });
    },

    // 登出
    Logout({ commit, state }, justlogout) {
      // 主动登出时移除自动登录逻辑
      if (justlogout) {
        Vue.ls.remove(AUTO_LOGIN);
      }
      return new Promise(resolve => {
        let logoutToken = state.token;
        commit('SET_TOKEN', '');
        commit('SET_PERMISSIONLIST', []);
        Vue.ls.remove(ACCESS_TOKEN);
        Vue.ls.remove(USER_INFO);
        // console.log('logoutToken: '+ logoutToken)
        logout(logoutToken)
          .then(() => {
            resolve();
          })
          .catch(() => {
            resolve();
          });
      });
    },
    setUnreadCount: ({ commit, state }, count) => {
      commit('SET_COUNT', count);
    }
  },
  getters: {
    /**
     * 获取未读
     */
    getUserUnreadCount: state => {
      return state.unreadCount;
    }
  }
};

export default user;
