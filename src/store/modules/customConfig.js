import Vue from 'vue';
import config from '@config/defaultSettings';
import { DEFAULT_COLOR, DEFAULT_LAYOUT_MODE, USER_INFO } from '@/store/mutationTypes';
import { getCustomCfg, getProfileValueByCode, getBasicComponents } from '@api/api';

/**
 * @description 个性化配置数据公共存储方法
 */
const customConfig = {
  state: {
    //平台系统参数配置项
    messageRequestInternal: null, //平台V6消息请求时间间隔配置
    manageSystemUrl: '',

    platformConfigCode: {
      messageRequestInternal: 'PLATFORM_V6_MESSAGE_REQUEST_INTERVAL',
      manageSystemUrl: 'PLATFORM_V6_BS_REDIRECT_URL'
    },
    customCfg: null, // 当前登录用户的所有个性化配置数据
    // 当前登录用户的门户权限数据
    customPortlet: {
      PORTAL_DESIGN: [],
      PORTAL_DESIGN_CONTENT: ''
      // PORTALLET_SMALLPAGES:[]
    },
    /**
     * KEY值及常量定义,包含：
     * 1、全局配置项Key G_XXX
     * 2、模块命名  MN_模块名 (包含子模块则追加 "_子模块名")
     * 3、模块配置项key MK_XXX
     * 4、模块配置项中常量(最值、默认值等) MC_XXX
     */
    customCfgKeys: {
      G_USER_CFG: 'G_USER_CFG', // Storage存储配置数据的KEY
      G_GLOBAL_FMT: 'G_', // 全局配置项命名格式, 以该字符串开头
      G_MODULE_NAME_FMT: 'MN_', // 局部配置项命名格式, 以该字符串开头，MN_模块名_子模块名
      G_CFG: 'G_CFG', // 定义全局配置key

      /**
       * 模块配置项key MK_XXX
       */
      MK_COLUMNS_SORT: 'MK_COLUMNS_SORT', // 定义模块中表格显示列及排序规则 配置项的key
      MK_SEARCH_HISTORY: 'MK_SEARCH_HISTORY', // 定义模块中搜索历史配置项的key
      MK_PAGER_RULE: 'MK_PAGER_RULE', // 定义模块中表格分页器&排序配置项的key

      /**
       * 模块配置项中常量(最值、默认值等) MC_XXX
       */
      MC_SEARCH_HISTORY_MAX: 10, // 定义模块中搜索历史最大存储量

      /**
       * 本条为模块命名定义 MN_模块名_子模块名
       */
      MN_EXCEPTION_404: 'MN_EXCEPTION_404',
      MN_EXCEPTION_403: 'MN_EXCEPTION_403',
      MN_EXCEPTION_500: 'MN_EXCEPTION_500',
      MN_HOME: 'MN_HOME',
      MN_TABREFRESH_DEMO: 'MN_TABREFRESH_DEMO',
      MN_CUSTOMCONFIG_DEMO: 'MN_CUSTOMCONFIG_DEMO'
    }
  },
  mutations: {
    /**
     * @description 存储指定模块表格中 显示列及排序配置
     * @param payload
     * {
     *   key:模块命名,
     *   val:表格中显示列及排序配置规则
     * }
     */
    SAVE_MODULE_COLUMNS: (state, payload) => {
      if (payload.key && payload.key.indexOf(state.customCfgKeys.G_MODULE_NAME_FMT) === 0) {
        if (state.customCfg[payload.key]) {
          state.customCfg[payload.key][state.customCfgKeys.MK_COLUMNS_SORT] = payload.val;
        } else {
          state.customCfg[payload.key] = {};
          state.customCfg[payload.key][state.customCfgKeys.MK_COLUMNS_SORT] = payload.val;
        }
      }
    },
    /**
     * @description 逐条存储指定模块搜索历史记录(总数不超过最大存储量)
     * @param payload
     * {
     *   key:模块命名,
     *   val:需要存储的搜索历史条目
     * }
     */
    SAVE_MODULE_SEARCH_HISTORY: (state, payload) => {
      if (payload.key && payload.key.indexOf(state.customCfgKeys.G_MODULE_NAME_FMT) === 0) {
        if (!state.customCfg[payload.key]) {
          state.customCfg[payload.key] = {};
        }

        if (state.customCfg[payload.key][state.customCfgKeys.MK_SEARCH_HISTORY]) {
          if (
            state.customCfg[payload.key][state.customCfgKeys.MK_SEARCH_HISTORY].length <
            state.customCfgKeys.MC_SEARCH_HISTORY_MAX
          ) {
            state.customCfg[payload.key][state.customCfgKeys.MK_SEARCH_HISTORY].unshift(
              payload.val
            );
          } else {
            state.customCfg[payload.key][state.customCfgKeys.MK_SEARCH_HISTORY].splice(
              state.customCfg[payload.key][state.customCfgKeys.MK_SEARCH_HISTORY].length - 1,
              1
            );
            state.customCfg[payload.key][state.customCfgKeys.MK_SEARCH_HISTORY].unshift(
              payload.val
            );
          }
        } else {
          state.customCfg[payload.key][state.customCfgKeys.MK_SEARCH_HISTORY] = [];
          state.customCfg[payload.key][state.customCfgKeys.MK_SEARCH_HISTORY].unshift(payload.val);
        }
      }
    },
    /**
     * @description 更新指定模块表格中分页器配置规则
     * @param payload
     * {
     *   key:模块命名,
     *   val:分页器配置规则
     * }
     */
    SAVE_MODULE_PAGER_RULE: (state, payload) => {
      if (payload.key && payload.key.indexOf(state.customCfgKeys.G_MODULE_NAME_FMT) === 0) {
        if (!state.customCfg[payload.key]) {
          state.customCfg[payload.key] = {};
        }
        state.customCfg[payload.key][state.customCfgKeys.MK_PAGER_RULE] = payload.val;
      }
    },
    /**
     * @description 全量覆盖加载用户个性化配置数据，只在以下两个时机调用
     * 1、初始化构建默认配置完成
     * 2、通过向服务器请求获取到最新的个性化配置
     */
    UPDATE_CUSTOM_CFG: (state, payload) => {
      state.customCfg = payload;
    },
    /**
     * @description 全量覆盖加载用户个性化配置数据，只在以下两个时机调用
     * 1、初始化构建默认配置完成
     * 2、通过向服务器请求获取到最新的个性化配置
     */
    UPDATE_CUSTOM_PORTLET: (state, payload) => {
      // console.log('payload.val',payload.val)
      if (payload.key) {
        // Vue.set(state.customPortlet[payload.key], payload.val)
        state.customPortlet[payload.key] = payload.val;
      }
    },
    /**
     * @description 指定模块中扩展自定义配置项，若已有该配置项则覆盖内容
     * @param payload
     * {
     *   key:模块命名,
     *   itemKey: 自定义配置项Key
     *   val: 自定义配置项内容
     * }
     */
    SAVE_MODULE_ITEM_CFG: (state, payload) => {
      if (
        payload.key &&
        payload.itemKey &&
        payload.key.indexOf(state.customCfgKeys.G_MODULE_NAME_FMT) === 0
      ) {
        if (!state.customCfg[payload.key]) {
          // state.customCfg[payload.key] = {};
          Vue.set(state.customCfg, payload.key, {});
        }
        // state.customCfg[payload.key][payload.itemKey] = payload.val;
        Vue.set(state.customCfg[payload.key], payload.itemKey, payload.val);
      }
    },
    /**
     * @description 全局自定义配置项，若已有该配置项则覆盖内容
     * @param payload
     * {
     *   key:自定义全局配置项Key,
     *   val: 自定义全局配置项内容
     * }
     */
    SAVE_GLOBAL_ITEM_CFG: (state, payload) => {
      if (payload.key) {
        if (
          !state.customCfg[state.customCfgKeys.G_CFG][
            state.customCfgKeys.G_GLOBAL_FMT + payload.key
          ]
        ) {
          state.customCfg[state.customCfgKeys.G_CFG][
            state.customCfgKeys.G_GLOBAL_FMT + payload.key
          ] = {};
        }
        state.customCfg[state.customCfgKeys.G_CFG][state.customCfgKeys.G_GLOBAL_FMT + payload.key] =
          payload.val;
      }
    },
    SAVE_MODULE_CFG: (state, payload) => {
      Vue.set(state.customCfg, payload.key, payload.val);
    }
  },
  getters: {
    getMessageRequestInternal: state => {
      return state.messageRequestInternal ? parseInt(state.messageRequestInternal) : 30000;
    },
    getManageSystemUrl: state => {
      return state.manageSystemUrl;
    },
    /**
     * @description 读取某模块中已存储配置（模块内全量）
     *   以模块MN_GOODSMANAGE为例，结构如下
     *   {
     *     "MK_COLUMNS_SORT": [
     *       {
     *         "key": "goodsName",
     *         "name": "商品名称",
     *         "sort": ""
     *       },{
     *         "key": "goodsCnt",
     *         "name": "商品库存",
     *         "sort": "DESC"
     *       }
     *     ],
     *     "M_SEARCH_HISTORY": [
     *       [{
     *           "key": "startTime",
     *           "name": "起始时间",
     *           "val": "2019-05-26 11:13:50"
     *         },{
     *           "key": "secretLvl",
     *           "name": "密级",
     *           "val": "secret"
     *         }],[{
     *           "name": "组织机构",
     *           "key": "orgName",
     *           "val": "系统工程部"
     *         }
     *       ]
     *     ],
     *     "M_PAGER_RULE": {
     *       "pageSize": 20
     *     }
     *   }
     */
    getModuleCfg: state => moduleKey => {
      return state.customCfg[moduleKey];
    },
    /**
     * @description 读取某模块中已存储配置（模块内单项）
     */
    getModuleCfgItem: state => (moduleKey, itemKey) => {
      return state.customCfg[moduleKey] ? state.customCfg[moduleKey][itemKey] : {};
    },
    /**
     * @description 读取全局配置项中的已存配置
     */
    getGlobalCfgItem: state => globalItemKey => {
      return state.customCfg[state.customCfgKeys.G_CFG][
        state.customCfgKeys.G_GLOBAL_FMT + globalItemKey
      ]
        ? state.customCfg[state.customCfgKeys.G_CFG][
            state.customCfgKeys.G_GLOBAL_FMT + globalItemKey
          ]
        : null;
    },
    /**
     * @description 读取全局配置项中的门户配置信息
     */
    getPortLet: state => Key => {
      return state.customPortlet[Key] ? state.customPortlet[Key] : null;
    }
  },
  actions: {
    loadPlatformConfig({ commmit, state }) {
      //请求平台系统参数配置---消息请求时间间隔
      if (!state.messageRequestInternal) {
        let params = { profileCode: state.platformConfigCode.messageRequestInternal };
        getProfileValueByCode(params).then(res => {
          if (res.success && res.code === '200' && res.result) {
            state.messageRequestInternal = res.result;
          } else {
            state.messageRequestInternal = config.messageRequestInternal;
          }
        });
      }
      if (!state.manageSystemUrl) {
        let params = { profileCode: state.platformConfigCode.manageSystemUrl };
        getProfileValueByCode(params).then(res => {
          if (res.success && res.code === '200' && res.result) {
            state.manageSystemUrl = res.result;
          } else {
            state.manageSystemUrl = config.manageSystemDomain + config.manageSystemUrl;
          }
        });
      }
    },
    /* 加载指定模块的个性化配置 */
    loadModuleCustomCfg({ commit, state }, moduleKey) {
      if (!state.customCfg[moduleKey]) {
        // state.customCfg[moduleKey] = {};
        commit('SAVE_MODULE_CFG', { key: moduleKey, val: {} });
      }
      if (!Object.keys(state.customCfg[moduleKey]).length) {
        let params = {
          userId: Vue.ls.get(USER_INFO).id,
          customedCode: moduleKey
        };
        getCustomCfg(params).then(res => {
          if (res.success && res.code === '200' && res.result && res.result.length > 0) {
            // state.customCfg[moduleKey] = JSON.parse(res.result);
            commit('SAVE_MODULE_CFG', { key: moduleKey, val: JSON.parse(res.result) });
          } else {
            // state.customCfg[moduleKey] = {};
            commit('SAVE_MODULE_CFG', { key: moduleKey, val: {} });
          }
          Vue.ls.set(state.customCfgKeys.G_USER_CFG, {
            userId: Vue.ls.get(USER_INFO).id,
            data: state.customCfg
          });
        });
      }
    },
    /* 加载用户的全局个性化配置 */
    loadCustomCfg({ commit, state }) {
      // 读取本地并做用户验证，无有效数据则发送网络请求获取用户配置
      if (!state.customCfg) {
        let localCfg = Vue.ls.get(state.customCfgKeys.G_USER_CFG);
        if (localCfg && localCfg.userId === Vue.ls.get(USER_INFO).id) {
          commit('UPDATE_CUSTOM_CFG', localCfg.data);
          /* 根据个人设置加载主题 */
          if (
            localCfg.data[state.customCfgKeys.G_CFG][
              state.customCfgKeys.G_GLOBAL_FMT + 'LAYOUT_MODE'
            ]
          ) {
            commit(
              'TOGGLE_LAYOUT_MODE',
              localCfg.data[state.customCfgKeys.G_CFG][
                state.customCfgKeys.G_GLOBAL_FMT + 'LAYOUT_MODE'
              ]
            );
          } else {
            commit('TOGGLE_LAYOUT_MODE', Vue.ls.get(DEFAULT_LAYOUT_MODE, config.layout));
          }
          /* 根据个人设置加载皮肤 */
          if (
            localCfg.data[state.customCfgKeys.G_CFG][
              state.customCfgKeys.G_GLOBAL_FMT + 'SKIN_COLOR'
            ]
          ) {
            commit(
              'TOGGLE_COLOR',
              localCfg.data[state.customCfgKeys.G_CFG][
                state.customCfgKeys.G_GLOBAL_FMT + 'SKIN_COLOR'
              ]
            );
          } else {
            commit('TOGGLE_COLOR', Vue.ls.get(DEFAULT_COLOR, config.primaryColor));
          }
        } else {
          return new Promise((resolve, reject) => {
            let params = {
              userId: Vue.ls.get(USER_INFO).id,
              customedCode: state.customCfgKeys.G_USER_CFG
            };
            let cfgObj = {};
            getCustomCfg(params)
              .then(res => {
                // 有则使用，无则设置默认
                if (res.success && res.code === '200' && res.result && res.result.length > 0) {
                  cfgObj[state.customCfgKeys.G_CFG] = JSON.parse(res.result);

                  Vue.ls.set(state.customCfgKeys.G_USER_CFG, {
                    userId: Vue.ls.get(USER_INFO).id,
                    data: cfgObj
                  });
                  /* 获取到个人配置，使用默认设置 */
                  if (
                    cfgObj[state.customCfgKeys.G_CFG][
                      state.customCfgKeys.G_GLOBAL_FMT + 'LAYOUT_MODE'
                    ]
                  ) {
                    commit(
                      'TOGGLE_LAYOUT_MODE',
                      cfgObj[state.customCfgKeys.G_CFG][
                        state.customCfgKeys.G_GLOBAL_FMT + 'LAYOUT_MODE'
                      ]
                    );
                  }
                  if (
                    cfgObj[state.customCfgKeys.G_CFG][
                      state.customCfgKeys.G_GLOBAL_FMT + 'SKIN_COLOR'
                    ]
                  ) {
                    commit(
                      'TOGGLE_COLOR',
                      cfgObj[state.customCfgKeys.G_CFG][
                        state.customCfgKeys.G_GLOBAL_FMT + 'SKIN_COLOR'
                      ]
                    );
                  }
                } else {
                  // 构建默认, 默认配置仅包含全局配置中的部分配置项
                  cfgObj[state.customCfgKeys.G_CFG] = {
                    defaultKey: 'defaultVal'
                    // ...
                  };
                  /* 未获取到个人配置，使用默认设置 */
                  commit('TOGGLE_LAYOUT_MODE', config.layout);
                  commit('TOGGLE_COLOR', config.primaryColor);
                  Vue.ls.remove(state.customCfgKeys.G_USER_CFG);
                }
                commit('UPDATE_CUSTOM_CFG', cfgObj);
                resolve();
              })
              .catch(e => {
                // 构建默认, 默认配置仅包含全局配置中的部分配置项
                cfgObj[state.customCfgKeys.G_CFG] = {
                  defaultKey: 'defaultVal'
                  // ...
                };
                commit('UPDATE_CUSTOM_CFG', cfgObj);
                /* 未获取到个人配置，使用默认设置 */
                commit('TOGGLE_LAYOUT_MODE', config.layout);
                commit('TOGGLE_COLOR', config.primaryColor);
                Vue.ls.remove(state.customCfgKeys.G_USER_CFG);
                reject(e);
              });
          });
        }
      }
    },
    /*
       获取门户配置小页权限字典
   */
    loadPortlet({ commit, state }) {
      if (!state.customPortlet['PORTALLET_SMALLPAGES']) {
        return new Promise((resolve, reject) => {
          getBasicComponents(null)
            .then(res => {
              let data = [];
              if (res.success && res.code === '200' && res.result) {
                res.result.map(tim => {
                  if (tim != null) {
                    data.push(tim);
                  }
                });
              }
              commit('UPDATE_CUSTOM_PORTLET', {
                key: 'PORTALLET_SMALLPAGES',
                val: data
              });
              resolve(res);
            })
            .catch(error => {
              reject(error);
            });
        });
      }
    }
  }
};

export default customConfig;
