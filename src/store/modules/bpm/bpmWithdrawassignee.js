import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 减签
 */
const bpmWithdrawassignee = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmWithdrawassigneeInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmWithdrawassigneeExecute: ({ dispatch, commit, state }) => {
      dispatch('openBpmCommonSelect', {
        buttonData: state.data,
        submitFuncName: 'BpmWithdrawassigneeSubmit'
      });
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    BpmWithdrawassigneeSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        bpmUtils.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dowithdrawactassignee/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          executionId: params.buttonData.executionId,
          userJson: params.users
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success('减签成功');
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
          dispatch('createButtons');
        }
      });
    }
  },
  getters: {}
};
export default bpmWithdrawassignee;
