import Store from '@/store/index';
import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 流程退回,包括退回上一步、退回拟稿人、任意退回、跨节点退回
 */
const bpmRetreat = {
  state: {
    data: {},
    enable: false,
    doretreattodraft: '',
    doretreattoprev: '',
    doretreattowant: '',
    doretreattoactivity: ''
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmRetreatInitFunc: ({ dispatch, commit, state }, params) => {
      if (params.data.event === 'doretreattodraft') {
        // 退回拟稿人
        state.doretreattodraft = params.data;
      } else if (params.data.event === 'doretreattoprev') {
        // 退回上一步
        state.doretreattoprev = params.data;
      } else if (params.data.event === 'doretreattowant') {
        // 任意退回
        state.doretreattowant = params.data;
      } else if (params.data.event === 'doretreattoactivity') {
        // 跨节点退回
        state.doretreattoactivity = params.data;
      }
      if (
        bpmUtils.notNull(state.doretreattodraft) ||
        bpmUtils.notNull(state.doretreattoprev) ||
        bpmUtils.notNull(state.doretreattowant) ||
        bpmUtils.notNull(state.doretreattoactivity)
      ) {
        state.enable = true;
      }
    },
    /**
     * 执行 退回拟稿人
     * @param params
     * {
     *    data
     *    start
     *    save
     * }
     */
    executeTodraft: ({ dispatch, commit, state }, params) => {
      // 自动保存
      // if (this.bpmEditor.defaultForm.isAutoSave && this.bpmEditor.bpmSave.isEnable()) {
      if (Store.state.bpmSave.enable) {
        params.save({
          callback(startResult) {
            dispatch('executeTodraftBack');
            dispatch('createButtons');
          }
        });
      } else {
        dispatch('executeTodraftBack');
      }
    },
    /**
     * 执行
     */
    executeTodraftBack: ({ dispatch, commit, state }) => {
      dispatch('openBpmCommonSelect', {
        buttonData: state.doretreattodraft,
        submitFuncName: 'executeTodraftSubmit'
      });
    },
    /**
     * 选人回调 退回拟稿人
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    executeTodraftSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        message.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dobacktofirst/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          executionId: params.buttonData.executionId,
          userJson: params.users
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success(
            '退回成功！表单将自动关闭',
            2,
            function () {
              bpmUtils.closeWindow();
              setTimeout(function () {
                dispatch('createButtons');
              }, 500);
            },
            true
          );
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
        }
      });
    },
    /**
     * 执行 退回上一步
     * @param params
     * {
     *    data
     *    start
     *    save
     * }
     */
    executeToprev: ({ dispatch, commit, state }, params) => {
      // 自动保存
      // if (this.bpmEditor.defaultForm.isAutoSave && this.bpmEditor.bpmSave.isEnable()) {
      if (Store.state.bpmSave.enable) {
        params.save({
          callback(startResult) {
            dispatch('executeToprevBack');
            dispatch('createButtons');
          }
        });
      } else {
        dispatch('executeToprevBack');
      }
    },
    /**
     * 执行
     */
    executeToprevBack: ({ dispatch, commit, state }) => {
      dispatch('openBpmCommonSelect', {
        buttonData: state.doretreattoprev,
        submitFuncName: 'executeToprevSubmit'
      });
    },
    /**
     * 选人回调 退回上一步
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    executeToprevSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        message.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dobacktoprev/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          executionId: params.buttonData.executionId,
          userJson: params.users
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success(
            '退回成功！表单将自动关闭',
            2,
            function () {
              bpmUtils.closeWindow();
              setTimeout(function () {
                dispatch('createButtons');
              }, 500);
            },
            true
          );
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
        }
      });
    },
    /**
     * 执行 任意退回
     * @param params
     * {
     *    data
     *    start
     *    save
     * }
     */
    executeTowant: ({ dispatch, commit, state }, params) => {
      // 自动保存
      // if (this.bpmEditor.defaultForm.isAutoSave && this.bpmEditor.bpmSave.isEnable()) {
      if (Store.state.bpmSave.enable) {
        params.save({
          callback(startResult) {
            dispatch('executeTowantSelectTask');
            dispatch('createButtons');
          }
        });
      } else {
        dispatch('executeTowantSelectTask');
      }
    },
    /**
     * 执行
     */
    executeTowantSelectTask: ({ dispatch, commit, state }) => {
      dispatch('OpenFlowNodeSelectModal', {
        type: state.doretreattowant.event,
        title: '任意退回节点选择'
      });
    },
    /**
     * 选择流程节点回调
     * @param params
     * {
     *  executionId
     *  activityName
     * }
     */
    executeTowantCallback: ({ dispatch, commit, state }, params) => {
      dispatch('closeFlowNodeSelectModal');
      let data = JSON.parse(JSON.stringify(state.doretreattowant));
      data.targetActivityName = params.activityName;
      dispatch('openBpmCommonSelect', { buttonData: data, submitFuncName: 'executeTowantSubmit' });
    },
    /**
     * 选人回调 任意退回
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    executeTowantSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        message.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dobacktowant/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          executionId: params.buttonData.executionId,
          userJson: params.users,
          activityName: params.buttonData.targetActivityName
        },
        'post'
      ).then(res => {
        dispatch('closeBpmCommonSelect');
        message.success(
          '退回成功！表单将自动关闭',
          2,
          function () {
            bpmUtils.closeWindow();
            setTimeout(function () {
              dispatch('createButtons');
            }, 500);
          },
          true
        );
      });
      try {
        //按钮点击后置事件
        dispatch('afterButtons', { event: params.buttonData.event });
      } catch (e) {}
    },
    /**
     * 执行 跨节点退回
     * @param params
     * {
     *    data
     *    start
     *    save
     * }
     */
    executeToactivity: ({ dispatch, commit, state }, params) => {
      // 自动保存
      // if (this.bpmEditor.defaultForm.isAutoSave && this.bpmEditor.bpmSave.isEnable()) {
      if (Store.state.bpmSave.enable) {
        params.save({
          callback(startResult) {
            dispatch('executeToactivityBack', { targetActivityName: params.data.targetName });
            dispatch('createButtons');
          }
        });
      } else {
        dispatch('executeToactivityBack', { targetActivityName: params.data.targetName });
      }
    },
    /**
     * 执行
     */
    executeToactivityBack: ({ dispatch, commit, state }, params) => {
      let data = JSON.parse(JSON.stringify(state.doretreattoactivity));
      data.targetActivityName = params.targetActivityName;
      dispatch('openBpmCommonSelect', {
        buttonData: data,
        submitFuncName: 'executeToactivitySubmit'
      });
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    executeToactivitySubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        message.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dobacktoactivity/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          executionId: params.buttonData.executionId,
          userJson: params.users,
          activityName: params.buttonData.targetActivityName
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success(
            '退回成功！表单将自动关闭',
            2,
            function () {
              bpmUtils.closeWindow();
              setTimeout(function () {
                dispatch('createButtons');
              }, 500);
            },
            true
          );
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
        }
      });
    }
  },
  getters: {}
};
export default bpmRetreat;
