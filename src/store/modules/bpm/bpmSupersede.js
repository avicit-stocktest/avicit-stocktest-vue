import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 流程转办
 */
const bpmSupersede = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmSupersedeInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmSupersedeExecute: ({ dispatch, commit, state }) => {
      dispatch('openBpmCommonSelect', {
        buttonData: state.data,
        submitFuncName: 'BpmSupersedeSubmit'
      });
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    BpmSupersedeSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        message.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dosupersede/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          executionId: params.buttonData.executionId,
          userJson: params.users,
          selectUserId: null,
          formJson: null,
          activityName: params.buttonData.targetActivityName
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success(
            '操作成功！表单将自动关闭',
            2,
            function () {
              bpmUtils.closeWindow();
              setTimeout(function () {
                dispatch('createButtons');
              }, 500);
            },
            true
          );
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
        }
      });
    }
  },
  getters: {}
};
export default bpmSupersede;
