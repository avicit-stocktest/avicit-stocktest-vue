import { Modal, message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 流程恢复
 */
const bpmGlobalresume = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmGlobalresumeInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmGlobalresumeExecute: ({ dispatch, commit, state }) => {
      Modal.confirm({
        title: '您确定恢复该流程吗？',
        onOk: () => {
          httpAction(
            bpmUtils.baseurl + '/business/doresume/v1',
            {
              processInstanceId: state.data.procinstDbid
            },
            'post'
          ).then(res => {
            if (res.success) {
              message.success('操作成功');
              try {
                //按钮点击后置事件
                dispatch('afterButtons', { event: state.data.event });
              } catch (e) {}
              dispatch('createButtons');
              bpmUtils.refreshBack();
            }
          });
        }
      });
    }
  },
  getters: {}
};
export default bpmGlobalresume;
