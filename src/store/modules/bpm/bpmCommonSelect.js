import Store from '@/store/index';
/**
 * 选人
 */
const bpmCommonSelect = {
  state: {
    selectVisible: false,
    buttonData: null, //按钮表单数据
    submitFuncName: '', //掉用点击事件
    beforeOpenSelect: false, //大选人前置事件
    defaultSelectUsers: null, // 默认选中用户
    isShowOpinion: true //是否显示流程意见框
  },
  mutations: {
    SELECT_VISIBLE(state, selectVisible) {
      state.selectVisible = selectVisible;
    },
    BEFORE_OPEN_SELECT(state, beforeOpenSelect) {
      state.beforeOpenSelect = beforeOpenSelect;
    },
    SET_SELECTUSER(state, defaultSelectUsers) {
      state.defaultSelectUsers = defaultSelectUsers;
    },
    IS_SHOW_OPINION(state, isShowOpinion) {
      state.isShowOpinion = isShowOpinion;
    }
    // OPEN
  },
  actions: {
    //打开流程选人
    openBpmCommonSelect: ({ commit, state }, data) => {
      // debugger
      console.log(state.selectVisible, '---selectVisible---');
      console.log(data.buttonData.lable, '---data---');
      if (state.selectVisible != true) {
        state.buttonData = data.buttonData;
        if (!state.buttonData.defineId) {
          state.buttonData.defineId = Store.state.bpmModel.defineId;
        }
        if (!state.buttonData.executionId) {
          state.buttonData.executionId = Store.state.bpmModel.executionId;
        }
        if (!state.buttonData.executionId) {
          state.buttonData.processInstanceId = Store.state.bpmModel.processInstanceId;
        }
        state.submitFuncName = data.submitFuncName;
        if (data.defaultSelectUsers) {
          commit('SET_SELECTUSER', data.defaultSelectUsers);
          // state.defaultSelectUsers =data.defaultSelectUsers;
        }
        if (typeof data.isShowOpinion != 'undefined') {
          // state.isShowOpinion =data.isShowOpinion;
          commit('IS_SHOW_OPINION', data.isShowOpinion);
        }
        commit('BEFORE_OPEN_SELECT', true);
        commit('SELECT_VISIBLE', true);
        // commit('IS_SHOW_OPINION',true);
      } else {
        state.buttonData = null;
        state.submitFuncName = null;
        commit('SELECT_VISIBLE', false);
        commit('IS_SHOW_OPINION', true);
      }
    },
    //设置默认选中人
    setDefaultSelectUsers: ({ commit, state }, defaultSelectUsers) => {
      commit('SET_SELECTUSER', defaultSelectUsers);
    },
    //关闭流程选人
    closeBpmCommonSelect: ({ commit, state }) => {
      console.log(state.selectVisible, '---beforeClose---');
      if (state.selectVisible == true) {
        state.buttonData = null;
        state.submitFuncName = null;
        commit('SELECT_VISIBLE', false);
      }
      console.log(state.selectVisible, '---afterClose---');
    },
    //设置流程意见框是否可见
    setShowOpinion: ({ commit, state }, isShowOpinion) => {
      console.log(isShowOpinion, '==isShowOpinion');
      commit('IS_SHOW_OPINION', isShowOpinion);
    }
  },
  getters: {
    getSelectVisible: state => {
      return state.selectVisible;
    },
    /**
     * 打开选人前置事件
     */
    getBeforeOpenSelect: state => {
      return state.beforeOpenSelect;
    },
    /**
     * 设置默认选中人
     */
    getDefaultSelectUsers: state => {
      return state.defaultSelectUsers;
    }
  }
};
export default bpmCommonSelect;
