import Store from '@/store/index';
import { message } from 'ant-design-vue';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 保存
 */
const bpmSave = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmSaveInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (Store.state.bpmEditor.isStart || bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     * @param params
     * {
     *    data
     *    start
     *    save
     * }
     */
    BpmSaveExecute: ({ dispatch, commit, state }, params) => {
      {
        if (Store.state.bpmEditor.isStart) {
          params.start({
            defineId: Store.state.bpmModel.defineId,
            callback(startResult) {
              message.success('暂存成功');
              dispatch('afterStart', startResult);
              dispatch('createButtons');
              bpmUtils.refreshBack();
            }
          });
        } else {
          params.save({
            callback() {
              message.success('暂存成功');
              dispatch('createButtons');
              bpmUtils.refreshBack();
            }
          });
        }
      }
    }
  },
  getters: {}
};
export default bpmSave;
