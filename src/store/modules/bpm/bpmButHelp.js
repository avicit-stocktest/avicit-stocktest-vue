import Store from '@/store/index';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 帮助
 */
const bpmButHelp = {
  state: {
    data: null,
    enable: true,
    helpinfo: ''
  },
  mutations: {},
  actions: {
    /**
     * 执行
     */
    BpmButHelpExecute: ({ dispatch, commit, state }) => {
      httpAction(
        bpmUtils.baseurl + '/business/getTaskRemark/v1',
        {
          defineId: Store.state.bpmModel.defineId,
          activityname: Store.state.bpmModel.activityname
        },
        'post'
      ).then(res => {
        if (res.success) {
          state.helpinfo = res.result || '当前节点无描述';
          // let msg = res.result || '当前节点无描述';
          // Modal.info({
          //   // title: res.result || '当前节点无描述'
          //   title: h => {
          //     // 获取注册的组件 (始终返回构造器)
          //     var MyComponent = Vue.component('help-msg', {
          //       template: '<div>' + msg + '</div>'
          //     })
          //     return h(MyComponent);
          //   }
          // });
        }
      });
    }
  },
  getters: {
    getHelpInfo: state => {
      return state.helpinfo;
    }
  }
};
export default bpmButHelp;
