import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 流程结束
 */
const bpmGlobalend = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmGlobalendInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmGlobalendExecute: ({ dispatch, commit, state }) => {
      dispatch('openBpmCommonSelect', {
        buttonData: state.data,
        submitFuncName: 'BpmGlobalendSubmit'
      });
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     *    idea
     * }
     */
    BpmGlobalendSubmit: ({ dispatch, commit, state }, params) => {
      httpAction(
        bpmUtils.baseurl + '/business/doend/v1',
        {
          executionId: params.buttonData.executionId,
          message: JSON.parse(params.users).idea
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success(
            '操作成功！表单将自动关闭',
            2,
            function () {
              bpmUtils.closeWindow();
              setTimeout(function () {
                dispatch('createButtons');
              }, 500);
            },
            true
          );
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
        }
      });
    }
  },
  getters: {}
};
export default bpmGlobalend;
