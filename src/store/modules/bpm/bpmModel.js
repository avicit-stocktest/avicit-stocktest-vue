/**
 * 数据模板
 */
const bpmModel = {
  state: {
    formId: null,
    defineId: null,
    deploymentId: null,
    entryId: null,
    executionId: null,
    taskId: null,
    activityname: null,
    activitylabel: null,
    userIdentityKey: null,
    userIdentity: null,
    flowRefresh: false, // 刷新页面
    afterButtonsEvent: null //按钮点击完成事件
  },
  mutations: {
    /**
     * 初始化
     * @param params
     * {
     *    defineId
     *    deploymentId
     *    entryId
     *    executionId
     *    taskId
     * }
     */
    flowModelInitFunc: (state, params) => {
      state.defineId = params.defineId;
      state.deploymentId = params.deploymentId;
      // _fileupload_entryId = entryId;
      state.entryId = params.entryId;
      state.executionId = params.executionId;
      state.taskId = params.taskId;
      state.formId = params.formId;
    },
    /**
     * 表单id
     * @param defineId
     */
    setFormId: (state, formId) => {
      state.formId = formId;
    },
    /**
     * 流程定义文件主键
     * @param defineId
     */
    setDefineId: (state, defineId) => {
      state.defineId = defineId;
    },
    /**
     * 流程部署文件主键
     * @param deploymentId
     */
    setDeploymentId: (state, deploymentId) => {
      state.deploymentId = deploymentId;
    },
    /**
     * 流程实例主键
     * @param entryId
     */
    setEntryId: (state, entryId) => {
      state.entryId = entryId;
    },
    /**
     * 流程指针ID
     * @param executionId
     */
    setExecutionId: (state, executionId) => {
      state.executionId = executionId;
    },
    /**
     * 流程任务ID
     * @param taskId
     */
    setTaskId: (state, taskId) => {
      state.taskId = taskId;
    },
    /**
     * 当前节点名称
     * @param activityname
     */
    setActivityname: (state, activityname) => {
      state.activityname = activityname;
    },
    /**
     * 当前节点显示名
     * @param activitylabel
     */
    setActivitylabel: (state, activitylabel) => {
      state.activitylabel = activitylabel;
    },
    /**
     * 当前身份
     * 用户身份 1待办人2 已办人 3待阅人 4已阅人 5读者，6拟稿人，7管理员，0未知
     * @param userIdentityKey
     */
    setUserIdentityKey: (state, userIdentityKey) => {
      state.userIdentityKey = userIdentityKey;
    },
    /**
     * 当前身份
     * 用户身份 待办人 已办人 待阅人 已阅人 读者，拟稿人，管理员
     * @param userIdentity
     */
    setUserIdentity: (state, userIdentity) => {
      state.userIdentity = userIdentity;
    },
    /**
     * 刷新页面数据
     */
    setFlowRefresh: (state, flowRefresh) => {
      state.flowRefresh = flowRefresh;
    },
    /**
     * 刷新页面数据
     */
    setAfterButtins: (state, afterButtonsEvent) => {
      state.afterButtonsEvent = afterButtonsEvent;
    }
  },
  actions: {
    //刷新页面方法
    flowRefresh: ({ dispatch, commit, state }) => {
      commit('setFlowRefresh', !state.flowRefresh);
    },
    //创建流程对象
    createFlowModel(
      { dispatch, commit, state },
      entryId = '',
      executionId,
      taskId,
      defineId,
      deploymentId
    ) {
      if (!entryId) {
        new Error('流程实例ID不能空');
      }
      commit('flowModelInitFunc', {
        entryId,
        executionId,
        taskId,
        defineId,
        deploymentId
      });
    },
    //按钮点击后置事件
    afterButtons({ dispatch, commit, state }, afterButtonsEvent = null) {
      commit('setAfterButtins', afterButtonsEvent);
    }
  },
  getters: {
    /**
     * 表单id
     * @param defineId
     */
    getFormId: state => {
      return state.formId;
    },
    /**
     * 刷新页面数据
     */
    getFlowRefresh: state => {
      return state.flowRefresh;
    },
    /**
     * 按钮点击后置事件
     */
    getAfterButtons: state => {
      return state.afterButtonsEvent;
    },
    /**
     * 流程定义文件主键
     * @param defineId
     */
    getDefineId: state => {
      return state.defineId;
    },
    /**
     * 流程部署文件主键
     * @param deploymentId
     */
    getDeploymentId: state => {
      return state.deploymentId;
    },
    /**
     * 流程实例主键
     * @param entryId
     */
    getEntryId: state => {
      return state.entryId;
    },
    /**
     * 流程指针ID
     * @param executionId
     */
    getExecutionId: state => {
      return state.executionId;
    },
    /**
     * 流程任务ID
     * @param taskId
     */
    getTaskId: state => {
      return state.taskId;
    },
    /**
     * 当前节点名称
     * @param activityname
     */
    getActivityname: state => {
      return state.activityname;
    },
    /**
     * 当前节点显示名
     * @param activitylabel
     */
    getActivitylabel: state => {
      return state.activitylabel;
    },
    /**
     * 当前身份
     * 用户身份 1待办人 2已办人 3待阅人 4已阅人 5读者 6拟稿人 7管理员 0未知
     * @param userIdentityKey
     */
    getUserIdentityKey: state => {
      return state.userIdentityKey;
    },
    /**
     * 当前身份
     * 用户身份 待办人 已办人 待阅人 已阅人 读者 拟稿人 管理员
     * @param userIdentity
     */
    getUserIdentity: state => {
      return state.userIdentity;
    }
  }
  //暴漏的全局变量，下载附件是像后台传递
  // var _fileupload_entryId = "";
  // var _fileupload_taskName = "";
};
export default bpmModel;
