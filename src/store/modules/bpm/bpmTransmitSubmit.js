import Store from '@/store/index';
import { Modal, message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 已阅
 */
const bpmTransmitSubmit = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmTransmitSubmitInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (Store.state.bpmEditor.isStart || bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 完成传阅
     */
    BpmTransmitSubmitExecute: ({ dispatch, commit, state }) => {
      Modal.confirm({
        title: '确定提交吗？',
        onOk: () => {
          dispatch('transmitSubmit');
        }
      });
    },
    /**
     * 执行
     */
    transmitSubmit: ({ dispatch, commit, state }) => {
      httpAction(
        bpmUtils.baseurl + '/business/dosubmitTransmit/v1',
        {
          instanceId: state.data.procinstDbid,
          message: '已阅',
          taskId: state.data.taskId
        },
        'post'
      ).then(res => {
        if (res.success) {
          message.success('提交成功！表单将自动关闭', 2, function () {
            bpmUtils.closeWindow();
            setTimeout(function () {
              dispatch('createButtons');
            }, 500);
          });
        }
      });
    }
  },
  getters: {}
};
export default bpmTransmitSubmit;
