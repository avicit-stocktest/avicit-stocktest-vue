/**
 * 选择流程节点
 */
const bpmNodeSelect = {
  state: {
    flowNodeSelectVisible: false,
    type: '',
    title: ''
  },
  mutations: {
    /**
     * 控制选择流程节点弹窗显隐
     */
    flowNodeSelectIsVisible(state, flowNodeSelectVisible) {
      state.flowNodeSelectVisible = flowNodeSelectVisible;
    }
  },
  actions: {
    /**
     * 打开选择流程节点弹窗
     */
    OpenFlowNodeSelectModal: ({ dispatch, commit, state }, params) => {
      state.type = params.type;
      state.title = params.title;
      commit('flowNodeSelectIsVisible', true);
    },
    /**
     * 关闭选择流程节点弹窗
     */
    closeFlowNodeSelectModal: ({ dispatch, commit, state }) => {
      state.type = '';
      state.title = '';
      commit('flowNodeSelectIsVisible', false);
    }
  },
  getters: {
    /**
     * 获取选择流程节点弹窗显隐
     */
    getFlowNodeSelectVisible: state => {
      return state.flowNodeSelectVisible;
    },
    /**
     * 获取打开选择流程节点弹窗的按钮类型
     */
    getFlowNodeSelectType: state => {
      return state.type;
    },
    /**
     * 获取选择流程节点弹窗的标题
     */
    getFlowNodeSelectTitle: state => {
      return state.title;
    }
  }
};
export default bpmNodeSelect;
