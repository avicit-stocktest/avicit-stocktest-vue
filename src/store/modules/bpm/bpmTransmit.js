import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 发送阅知 即流程转发
 */
const bpmTransmit = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmTransmitInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmTransmitExecute: ({ dispatch, commit, state }) => {
      dispatch('openBpmCommonSelect', {
        buttonData: state.data,
        submitFuncName: 'BpmTransmitSubmit'
      });
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    BpmTransmitSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        bpmUtils.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dotransmit/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          taskId: params.buttonData.taskId,
          userJson: params.users
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success('操作成功');
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
          dispatch('createButtons');
        }
      });
    }
  },
  getters: {}
};
export default bpmTransmit;
