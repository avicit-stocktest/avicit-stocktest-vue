import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
import { message } from 'ant-design-vue';
/**
 * 流程tabs页签
 */
const bpmTabs = {
  state: {
    tabsVisible: false,
    tabs: [],
    showTabs: [],
    originalTabs: []
  },
  mutations: {
    TABS_VISIBLE(state, tabsVisible) {
      state.tabsVisible = tabsVisible;
    },
    setTabs(state, tabs) {
      state.showTabs = tabs;
    },
    setTabsArr(state, tabs) {
      state.tabs = tabs;
    },
    setOriginalTabs(state, tabs) {
      state.originalTabs = JSON.parse(JSON.stringify(tabs));
    }
  },
  actions: {
    setBpmOriginalTabs: ({ commit, state }, list) => {
      commit('setOriginalTabs', list);
    },
    //获取流程标签
    getBpmTabs: async ({ commit, state }) => {
      let res = await bpmUtils.getBpmTabs();
      if (res.success) {
        // 字符串数据，暂时无用
        commit('setOriginalTabs', res.result);
        commit('setTabsArr', res.result);
        let arr = [];
        arr.push(res.result.slice(0)[0]);
        commit('setTabs', res.result);
      } else {
        message.error('获取流程页签失败!');
      }
    },
    showAllTabs: ({ commit, state }) => {
      commit('setTabs', state.tabs);
    },
    //获取流程导航类型
    getBpmNavbarType: async ({ commit, state }) => {
      let res = await bpmUtils.getBpmNavbarType();
      if (res.success) {
        if (res.result == '1') {
          commit('TABS_VISIBLE', true);
        } else {
          commit('TABS_VISIBLE', false);
        }
      } else {
        message.error('获取流程页签失败!');
      }
    }
  },
  getters: {
    getTabs: state => {
      return state.showTabs;
    },
    getTabsArr: state => {
      return state.tabs;
    },
    getTabsVisible: state => {
      return state.tabsVisible;
    }
  }
};
export default bpmTabs;
