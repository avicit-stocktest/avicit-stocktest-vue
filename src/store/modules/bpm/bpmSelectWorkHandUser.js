/**
 * 选择流程委托处理人
 */
const bpmSelectWorkHandUser = {
  state: {
    selectWorkHandUserVisible: false
  },
  mutations: {
    /**
     * 选择流程委托处理人弹窗显隐
     */
    selectWorkHandUserIsVisible(state, selectWorkHandUserVisible) {
      state.selectWorkHandUserVisible = selectWorkHandUserVisible;
    }
  },
  actions: {
    /**
     * 打开选择流程委托处理人弹窗
     */
    openSelectWorkHandUserModal: ({ dispatch, commit, state }) => {
      commit('selectWorkHandUserIsVisible', true);
    },
    /**
     * 关闭选择流程委托处理人弹窗
     */
    closeSelectWorkHandUserModal: ({ dispatch, commit, state }) => {
      commit('selectWorkHandUserIsVisible', false);
    }
  },
  getters: {
    /**
     * 获取选择流程委托处理人弹窗显隐
     */
    getSelectWorkHandUserVisible: state => {
      return state.selectWorkHandUserVisible;
    }
  }
};
export default bpmSelectWorkHandUser;
