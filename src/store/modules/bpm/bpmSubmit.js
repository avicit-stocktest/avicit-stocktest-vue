import Store from '@/store/index';
import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 提交
 */
const bpmSubmit = {
  state: {
    data: {},
    enable: false,
    isAutoClickAfterStart: false // 是否是启动流程后的自动点击事件
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmSubmitInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (Store.state.bpmEditor.isStart || bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     * @param params
     * {
     *    data
     *    start
     *    save
     * }
     */
    BpmSubmitExecute: ({ dispatch, commit, state }, params) => {
      if (Store.state.bpmEditor.isStart) {
        params.start({
          defineId: Store.state.bpmModel.defineId,
          callback(startResult) {
            dispatch('afterStart', startResult);
            dispatch('createButtons', { flg: true });
            bpmUtils.refreshBack();
          }
        });
      } else {
        // 自动保存
        if (!state.isAutoClickAfterStart && state.enable) {
          params.save({
            callback() {
              dispatch('createButtons', { flg: true, outcome: params.data.name });
            }
          });
        } else {
          dispatch('openBpmCommonSelect', {
            buttonData: params.data,
            submitFuncName: 'BpmSubmitSubmit'
          });
        }
      }
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    BpmSubmitSubmit: ({ dispatch, commit, state }, params) => {
      httpAction(
        bpmUtils.baseurl + '/business/dosubmit/v1',
        {
          userJson: params.users,
          instanceId: params.buttonData.procinstDbid,
          taskId: params.buttonData.taskId,
          outcome: params.buttonData.name,
          formJson: '',
          uflowDealType: '',
          isUflow: false
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success('提交成功！表单将自动关闭', 2, function () {
            bpmUtils.closeWindow();
            setTimeout(function () {
              dispatch('createButtons');
            }, 500);
          });
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
        }
      });
    },
    /**
     * 流程启动之后自动点击一次提交按钮,或者触发保存之后再来调用提交
     */
    clickBut: ({ dispatch, commit, state }, outcome) => {
      if (state.enable) {
        let buttonsArray = Store.state.bpmEditor.flowButtons;
        let submitButtonsArray = [];
        let submitButtonNum = 0;
        for (let i = 0; i < buttonsArray.length; i++) {
          if (buttonsArray[i].event === 'dosubmit') {
            submitButtonsArray.push(buttonsArray[i]);
            submitButtonNum += 1;
          }
        }
        if (bpmUtils.notNull(outcome)) {
          if (submitButtonNum > 0) {
            for (let i = 0; i < submitButtonsArray.length; i++) {
              if (submitButtonsArray[i].name === outcome) {
                dispatch('openBpmCommonSelect', {
                  buttonData: submitButtonsArray[i],
                  submitFuncName: 'BpmSubmitSubmit'
                });
                return;
              }
            }
          }
          message.warning('路由条件可能已经发生变化，请重新进行提交');
        } else {
          if (submitButtonNum > 1) {
            message.success('流程已创建，请选择一个分支进行提交');
          } else if (submitButtonNum > 0) {
            state.isAutoClickAfterStart = true;
            try {
              dispatch('BpmSubmitExecute', { data: submitButtonsArray[0] });
            } catch (e) {
              state.isAutoClickAfterStart = false;
            }
            state.isAutoClickAfterStart = false;
          }
        }
      }
    }
  },
  getters: {}
};
export default bpmSubmit;
