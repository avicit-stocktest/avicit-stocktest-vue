import Vue from 'vue';
import { Modal, message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 发送催办
 */
const bpmPresstodo = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmPresstodoInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmPresstodoExecute: ({ dispatch, commit, state }) => {
      httpAction(
        bpmUtils.baseurl + '/business/beforepresstodo/v1',
        {
          executionId: state.data.executionId
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('presstodo', res.result);
        }
      });
    },
    /**
     * 执行
     */
    presstodo: ({ dispatch, commit, state }, result) => {
      let msg = '';
      if (result.flg == 'true') {
        msg = '确定对下一节点进行催办吗？' + result.msg;
      } else {
        msg = '下一节点在' + result.time + '被催办过，确定继续进行催办吗？' + result.msg;
      }
      Modal.confirm({
        title: '提示',
        content: h => {
          // 获取注册的组件 (始终返回构造器)
          let MyComponent = Vue.component('my-msg', {
            template: '<div style="max-height: 150px;overflow: auto">' + msg + '</div>'
          });
          return h(MyComponent);
        },
        onOk: () => {
          httpAction(
            bpmUtils.baseurl + '/business/dopresstodo/v1',
            {
              executionId: state.data.executionId
            },
            'post'
          ).then(res => {
            if (res.success) {
              message.success('催办成功');
              try {
                //按钮点击后置事件
                dispatch('afterButtons', { event: state.data.event });
              } catch (e) {}
            }
          });
        }
      });
    }
  },
  getters: {}
};
export default bpmPresstodo;
