import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 拿回
 */
const bpmWithdraw = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmWithdrawInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmWithdrawExecute: ({ dispatch, commit, state }) => {
      httpAction(
        bpmUtils.baseurl + '/clientbpmdisplayaction/getcoordinate/v1',
        {
          processInstanceId: state.data.procinstDbid
        },
        'post'
      ).then(res => {
        if (res.success) {
          if (bpmUtils.notNull(res.result)) {
            for (let key in res.result) {
              let activity = res.result[key];
              let activityName = activity.activityName;
              let isCurrent = activity.isCurrent;
              let executionId = activity.executionId;
              let isAlone = activity.executionAlone;
              // 只有一个当前节点时候补发操作和拿回操作自动处理
              if (isAlone && isCurrent == 'true') {
                dispatch('BpmWithdrawCallback', {
                  executionId,
                  activityName
                });
                return;
              }
            }
            dispatch('OpenFlowNodeSelectModal', { type: state.data.event, title: '拿回节点选择' });
          }
        }
      });
    },
    /**
     * 选择流程节点回调
     * @param params
     * {
     *    executionId
     *    activityName
     * }
     */
    BpmWithdrawCallback: ({ dispatch, commit, state }, params) => {
      dispatch('closeFlowNodeSelectModal');
      let data = JSON.parse(JSON.stringify(state.data));
      //循环节点中的拿回传入的executionId不准确
      // 这里注释掉,正常的拿回会有问题
      data.executionId = params.executionId;
      data.targetActivityName = params.activityName;
      dispatch('openBpmCommonSelect', { buttonData: data, submitFuncName: 'BpmWithdrawSubmit' });
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    BpmWithdrawSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        message.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dowithdrawcurract/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          executionId: params.buttonData.executionId,
          userJson: params.users
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success('拿回成功！表单将自动关闭', 2, function () {
            bpmUtils.closeWindow();
            setTimeout(function () {
              commit('createButtons');
            }, 500);
          });
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
        }
      });
    }
  },
  getters: {}
};
export default bpmWithdraw;
