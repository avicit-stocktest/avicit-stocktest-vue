import { Modal, message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 流程暂停
 */
const bpmGlobalsuspend = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmGlobalsuspendInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmGlobalsuspendExecute: ({ dispatch, commit, state }) => {
      Modal.confirm({
        title: '您确定暂停该流程吗？',
        onOk: () => {
          httpAction(
            bpmUtils.baseurl + '/business/dosuspend/v1',
            {
              processInstanceId: state.data.procinstDbid
            },
            'post'
          ).then(res => {
            if (res.success) {
              message.success('操作成功！');
              try {
                //按钮点击后置事件
                dispatch('afterButtons', { event: state.data.event });
              } catch (e) {}
              dispatch('createButtons');
              bpmUtils.refreshBack();
            }
          });
        }
      });
    }
  },
  getters: {}
};
export default bpmGlobalsuspend;
