import Vue from 'vue';
import Vuex from 'vuex';

import app from './modules/app';
import user from './modules/user';
import permission from './modules/permission';
import tabMenu from './modules/tabMenu';
import axiosLog from './modules/axiosLog';
import customConfig from './modules/customConfig';
import getters from './getters';
import commitCustomCfgPlugin from './modules/customConfigPlugin';

import fourTable from './modules/fourTable';
import bpmEngine from './modules/bpm/bpmEngine';
import bpmModel from './modules/bpm/bpmModel';
import bpmCommonSelect from './modules/bpm/bpmCommonSelect';
import bpmSelectWorkHandUser from './modules/bpm/bpmSelectWorkHandUser';
import bpmDetailSelect from './modules/bpm/bpmDetailSelect';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    app,
    user,
    permission,
    tabMenu,
    customConfig,
    fourTable,
    bpmEngine,
    bpmModel,
    bpmDetailSelect,
    bpmCommonSelect,
    bpmSelectWorkHandUser,

    //FOR DEBUG By CJ
    axiosLog
  },
  state: {},
  mutations: {},
  actions: {},
  getters,
  plugins: [commitCustomCfgPlugin]
});
