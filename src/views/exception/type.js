const types = {
  404: {
    img: 'images/common/404.svg',
    title: '404',
    desc: '抱歉，你无权访问该页面'
  },
  500: {
    img: 'images/common/500.svg',
    title: '500',
    desc: '抱歉，服务器出错了'
  }
};

export default types;
