1、文件夹目录说明

          -theme
            -layouts
              -common
                    BasicLayout.vue
                    BlankLayout.vue
                    IframePageView.vue
                    RouteView.vue,
                    PageView.vue,
                    TabLayout.vue
                    MainLayout.vue
                    index.js
              -ConsoleLayout    成飞控制台 主题
                    - style
                    index.vue
              -HPlusLayout        H+ 主题
                   -style
                     base.less    H+主题不涉及换肤的定制化样式
                     skin.less   H+主题与换肤相关的样式
                   index.vue
              -SimpleLayout      Simple 主题
              -TraditionLayout   Tradition 主题
              -WinLayout         Win 主题
              -UserLayout        User 主题
              LayoutIndex.vue    主题注册应用文件

              README.MD  描述每个文件夹用途，以及新主题添加的方式

2、新增主题方法
1） 第一步：在 portal/theme 文件夹下新建主题文件夹，
如下：
-NewThemeLayout 新主题主题
-style
base.less 此文件存放新主题不涉及换肤的定制化样式
skin.less 此文件存放新主题与换肤相关的样式
index.vue 主题模板文件代码和 JS 代码、引入 base.less 2) 第二步： 注册皮肤文件
在 styles/skin/skin-register.less 文件中注册皮肤文件
代码如下：

           /* 覆盖定制化New主题相关样式，换肤用，方法名.avic_newthemelayout_theme() */
           @import '~@views/avic/pt/portal/theme/NewThemeLayout/style/skin';

           /*换肤函数 .theme(...） 中添加如下函数引用换肤才能生效/
           .avic_newthemelayout_theme( @primary-color );


3） 第三步：LayoutIndex.vue 文件中注册主题文件

            注册参考示例 WinLayout注册方式
            定义新主题对应的编码，例如 newthememenu ，并添加如下代码
           <NewThemeLayout v-else-if="layoutMode === 'newthememenu'" />



4） 第四步：SettingDrawer（路径：src/views/avic/pt/theme/setting/SettingDrawer.vue）文件夹中添加新选项

          如下：
           <!-- 新主题风格 -->
          <a-tooltip>
            <template slot="title">新主题</template>
            <div class="setting-drawer-index-item" @click="handleLayout('newthememenu')">
              <img src="~@assets/images/theme/newthememenu.png" alt="新主题" />
              <div v-if="layoutMode === 'newthememenu'" class="setting-drawer-index-selectIcon">
                <a-icon type="check" />
              </div>
            </div>
          </a-tooltip>
