import Icons from '../icon/Icons';
export const parentTableMixin = {
  components: {
    Icons
  },
  data() {
    return {};
  },
  methods: {
    selectIcons() {
      this.iconChooseVisible = true;
    },
    handleIconCancel() {
      this.iconChooseVisible = false;
    },
    handleIconChoose(value) {
      this.form.setFieldsValue({ icon: value });
      this.iconChooseVisible = false;
    }
  },
  computed: {}
};
