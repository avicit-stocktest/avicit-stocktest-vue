文件目录说明

      -avic
         -pt
           -demo 代码生成器模板
           -examples    所有示例页
              -bigDataTableDemo    大数据表格示例 
              -bigTreeDemo         大数据树列表示例
              -customConfigDemo      个性化配置示例
              -FormLayoutAndRightsDemo    表单布局示例
              -InputNumberCheckDemo      数字框示例
              -layoutDemo      布局示例
              -tabRefreshDemo  标签刷新示例
              -componentApiDemo  组件API文档示例
              -meetingDemo  会议管理
              -noticeDemo   通知公告管理
              -stateTableDemo
           - system 
              -dashboard    首页
              -menu         菜单管理（包括添加编辑管理页面、权限按钮添加编辑页面）
              -menuGroup    菜单分组管理（包括添加编辑管理页面）
              -news         消息通知管理 （包括添加编辑管理页面）
              -flowCenter   流程中心相关页面
              -portLet      门户小页管理 
                  -portalConfig       多栏目门户管理
                  -portLetCards       小页示例
              -personalSetting        个人设置相关页面    
              -flowPublicDetail       打开新页面流程详情页的组件
              -routerContentLoader     打开新页面的框架
                
           - portal      门户页面相关文件
              -theme    主题文件
                   -ConsoleLayout      成飞控制台 主题        
                   -HPlusLayout        H+ 主题          
                   -SimpleLayout       Simple 主题
                   -TraditionLayout    Tradition 主题
                   -WinLayout          Win 主题
                   LayoutIndex.vue     主题注册应用文件
         
              -pageModules  页面小模块功能
                  -menu     页签刷新组件 和 侧边栏菜单组件
                      Contextmenu.vue  页签菜单刷新组件 
                      SideMenu.vue     侧边栏菜单组件
                      UserMenu.vue     在 GlobalHeader中引用  
                  -drawer  右边栏设置抽屉内容
                      setting.js       皮肤数据设置 更新主题和皮肤的方法
                  HeaderNotice.vue     消息通知浮窗  
                  Logo.vue   
                  TaskPreviewList.vue   预览页签功能 在MainLayout 中引用  
             
              -pageLayouts  jecc 原始工程的页面布局文件
                  -common  
                        ContentHover.vue      在 GlobalMainLayout 中引用
                        GlobalFooter.vue      **未引用** 
                        GlobalHeader.vue      在 GlobalLayout中引用
                        GlobalLayout.vue      在 basicLayout TabLayout  MainLayout 中引用
                        GlobalMainLayout.vue  在 MainLayout 中引用
                        PageHeader.vue        在 PageLayout 中引用 
                        PageLayout.vue        在 PageView 中引用 
                  BasicLayout.vue  
                  BlankLayout.vue    
                  IframePageView.vue   
                  RouteView.vue  
                  PageView.vue  
                  TabLayout.vue  
                  MainLayout.vue  
                  index.js  
                  
文件扩展说明建议：
     业务模块功能扩展建议放在views/avic 文件下,例如
            
         -avic
           -pt  平台内容：包括系统默认功能、主题切换和门户功能）
           -newbusiness  新业务模块功能
