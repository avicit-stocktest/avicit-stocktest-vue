import { httpAction } from '@/api/manage';

//用户登录
const login = params => httpAction('/api/appsys/user/login', params, 'post');
const authLogin = params => httpAction('/api/appsys/user/loginByAccessKey', params, 'post');
// 控制台session登录
const sessionIdLogin = params => httpAction('/api/appsys/user/loginBySid', params, 'post');
//用户登录
const changePassword = params => httpAction('/api/appsys/user/changePassword', params, 'post');
//获取菜单权限字典数据
const queryPermissionsByUser = params => httpAction('/api/appsys/user/auth/menu', params);
//检查用户状态、菜单权限
const checkPermissionStatue = params => httpAction('/api/appsys/user/auth/check', params);

//获取首页访问数据
const getLoginfo = params => httpAction('/api/appsys/user/loginfo', params);

// 获取系统配置参数的接口
const getProfileValueByCode = params =>
  httpAction('/api/appsys/profile/ProfileRest/getProfileValueByCode/v1', params, 'post');
// 获取个性化配置
const getCustomCfg = params => httpAction('/api/appsys/user/customed/get/v1', params, 'post');
// 更新个性化配置
const putCustomCfg = params => httpAction('/api/appsys/user/customed/updateAll/v1', params, 'post');
// 添加个性化配置
const postCustomCfg = params => httpAction('/api/appsys/user/customed/save/v1', params, 'post');
// 保存访问历史
const saveVisitMenu = params =>
  httpAction('/api/appsys/user/menu/visitMenu/saveMenu/v1', params, 'post');

// 获取门户小页权限字典数据
const getBasicComponents = params =>
  httpAction('/api/appsys/portlet/portalConfig/togGetPortalAuth/v1', params, 'post');

export {
  login,
  authLogin,
  sessionIdLogin,
  changePassword,
  getLoginfo,
  checkPermissionStatue,
  queryPermissionsByUser,
  getProfileValueByCode,
  getCustomCfg,
  putCustomCfg,
  postCustomCfg,
  saveVisitMenu,
  getBasicComponents
};
