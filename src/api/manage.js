import { axios } from '@/utils/avic/http/request';
import Vue from 'vue';
import { ACCESS_TOKEN, MICRO_SERVICE_TOKEN } from '@/store/mutationTypes';

const api = {
  user: '/api/user',
  role: '/api/role',
  service: '/api/service',
  permission: '/api/permission',
  permissionNoPager: '/api/permission/no-pager'
};
import { notification } from 'ant-design-vue';
import { message } from 'ant-design-vue';
import { formateObjToParamStr } from '@/utils/avic/common/util';
export default api;
/**
 * 文件下载
 * @param {*} url
 * @param {*} parameter 请求参数
 * @param {*} method  http请求类型
 * @param {*} listenerProgress 下载进度监听事件
 * @param {*} flieName  文件名，如果不传会到Content-disposition中filename取
 * @param {*} isdownload  是否下载
 * @param {*} messageTime  提示消息时间
 */
export function download(
  url,
  parameter = null,
  method = 'get',
  listenerProgress,
  flieName = '',
  isdownload = true,
  messageTime = 4,
  callBack
) {
  let p = new Promise(function (resolve, reject) {
    let xhr = new XMLHttpRequest();
    //get 平接url参数
    if (method == 'get' && parameter != null) {
      let params = formateObjToParamStr(parameter);
      if (params) {
        if (url.indexOf('?') != -1) {
          url += '&' + params;
        } else {
          url += '?' + params;
        }
      }
    }
    xhr.open(method, url, true);
    xhr.responseType = 'blob';
    xhr.onload = function () {
      if (this.status == 200) {
        let blob = this.response;
        if (callBack) {
          callBack(true);
        }

        //获取文件名
        if (!flieName) {
          if (xhr.getResponseHeader('Content-disposition')) {
            let reg = new RegExp('(^|)filename=([^&]*)(&|$)');
            let r = xhr.getResponseHeader('Content-disposition').match(reg);
            // console.log(decodeURIComponent(r[2]).replace(/%20/g, ' '), '-------flieName-----');
            flieName = decodeURIComponent(r[2]).replace(/%20/g, ' ');
          }
        }
        if (isdownload) {
          let link = document.createElement('a');
          link.style.display = 'none';
          link.href = window.URL.createObjectURL(blob);
          link.setAttribute('download', flieName);
          document.body.appendChild(link);
          link.click();
          window.URL.revokeObjectURL(link.href);
        }
        /**
         *  else {
          resultUrl = window.URL.createObjectURL(blob)
        }
         */
        resolve(blob);
      } else {
        //下载失败处理提示
        let json = null;
        try {
          let res = this.response;
          let bal = new Blob([res]);
          let reader = new FileReader();
          reader.addEventListener('loadend', function () {
            try {
              json = JSON.parse(reader.result);
            } catch (e) {
              json = { message: reader.result };
            }
            if (callBack) {
              callBack(false);
            }
            if (decodeURI(json.message).indexOf('不允许预览') > -1) {
              message.error('不允许预览当前附件！');
            } else {
              notification.error({
                message: '系统提示',
                description: json.message ? decodeURI(json.message) : '请求资源失败',
                duration: messageTime
              });
            }
          });
          reader.readAsBinaryString(bal);
        } catch (e) {
          notification.error({
            message: '系统提示',
            description: '请求资源失败',
            duration: messageTime
          });
        }
        reject();
      }
    };
    xhr.addEventListener('progress', function (evt) {
      // console.log('download progress', evt);
      if (evt.lengthComputable && listenerProgress) {
        let percentComplete = evt.loaded / evt.total;
        listenerProgress((percentComplete * 100).toFixed(2));
      }
    });
    let formData = new FormData();
    xhr.setRequestHeader('X-Access-Token', Vue.ls.get(ACCESS_TOKEN));
    if (Vue.ls.get(MICRO_SERVICE_TOKEN)) {
      xhr.setRequestHeader('x-kong-api-key', 'bearer ' + Vue.ls.get(MICRO_SERVICE_TOKEN));
    }
    //post 转换数据
    if (method == 'post') {
      Object.keys(parameter).forEach(key => {
        formData.append(key, parameter[key]);
      });
      xhr.send(formData);
    } else {
      xhr.send();
    }
  });
  return p;
}
/**
 *
 * @param {*} url URL地址
 * @param {*} parameter  请求参数
 * @param {*} method  //请类型 get post
 * @param {*} errorMessage  请求错误
 * @param {*} loading  //是否显示加载框
 * @param {*} loadingLock //加载框是否锁屏
 * @param {*} errorSystemMessage  是否显示系统错误
 * @param {*} contentType // http Headers contentType 类型
 * @param {*} messageTime  提示消息时间
 * @param {*} specialStatusCode  需要特殊处理的status，不走平台统一处理
 * @param {*} notUpdateSession 是否刷新用户token的有效期(默认不传，定时任务类请求传入true)
 *
 */
export function httpAction(
  url,
  parameter,
  method = 'get',
  errorMessage = true,
  loading = true,
  loadingLock = true,
  errorSystemMessage = true,
  contentType = '',
  messageTime = 4,
  specialStatusCode = [],
  notUpdateSession = false
) {
  return axios({
    url,
    method,
    data: parameter,
    errorMessage,
    loading,
    loadingLock,
    contentType,
    errorSystemMessage,
    messageTime,
    specialStatusCode,
    notUpdateSession
  });
}

export function logout(logoutToken) {
  // 清除消息定时器
  clearInterval(window._CONFIG['unreadmsg']);
  return axios({
    url: '/api/appsys/user/logout',
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      'X-Access-Token': logoutToken
    }
  });
}
