import { Modal, message } from 'ant-design-vue';
import AvicEditTable from '@comp/avic/pt/common/AvicEditTable';
import { httpAction } from '@/api/manage';
import { mapGetters } from 'vuex';
import { loadLookUp } from '@utils/avic/http/loadLookUp';
import differenceWith from 'lodash.differencewith';
import isEqual from 'lodash.isequal';
import CryptoJS from 'crypto-js';
export const baseTableListMixin = {
  components: {
    AvicEditTable
  },
  data() {
    return {
      hasTableDataChanged: false,
      dataRecord: [], // 记录每次刷新得到的表格的数据
      newRowName: 'supposeId', // 新增行主键前缀
      count: 0, // 新增行主键后半部分,递增
      saveLoading: false // 保存按钮的loading状态
    };
  },
  methods: {
    ...mapGetters(['userInfo']),
    /**
     * 获取通用代码,并写入columns
     */
    getEditTableLookups(lookupParams) {
      loadLookUp(lookupParams, data => {
        for (let i = 0; i < data.length; i++) {
          for (let j = 0; j < this.columns.length; j++) {
            if (this.columns[j].dataIndex === data[i].fileName) {
              if (data[i].fileName !== 'secretLevel') {
                this.columns[j].list = data[i].value; // 拿到通用代码,赋给相应的通用代码数组
              } else {
                httpAction(
                  '/api/appsys/lookup/LookupRest/getUserSecretWordList/v1/' + this.userInfo().id,
                  '',
                  'get'
                ) // 提交
                  .then(res => {
                    if (res.success) {
                      this.columns[j].list = res.result;
                    } else {
                      this.$message.error('读取文档密级选项失败！');
                    }
                  })
                  .catch(e => {
                    console.error(e);
                  });
              }
            }
          }
        }
      });
    },
    /**
     * 利用表格组件暴露出的方法getData, 接收表格组件请求回的数据并处理
     * @param obj 形式: {data: data}
     */
    getData(obj) {
      if (obj.data && obj.data.result && obj.data.result.length > 0) {
        this.dataRecord = JSON.parse(JSON.stringify(obj.data.result)); // 深拷贝data,记录刷新得到的数据,用于识别是否有编辑项
        this.data = JSON.parse(JSON.stringify(obj.data.result)); // 渲染表格
      } else {
        this.dataRecord = [];
        this.data = [];
      }
      this.hasTableDataChanged = false;
    },
    /**
     * 删除处理逻辑
     * @param {Object} params 形式: {refName: refName(表格refName), ids: ids(删除项id集合数组)}
     */
    handleDelete(params) {
      if (!params) return;
      if (params.param && params.param.ids.length == 0) {
        this.$message.warning('请选择要删除的数据！');
        return;
      }
      let _this = this;
      let newIdsArray = []; // 定义新添加项的id集合
      let ids = params.param.ids; // 选中项的id集合
      for (let i = 0; i < ids.length; i++) {
        // 获取新添加项的id集合
        if (ids[i].indexOf(_this.newRowName) != -1) {
          newIdsArray.push(ids[i]);
        }
      }
      if (newIdsArray.length === 0) {
        // 选中的均为编辑过的,直接请求后台删除
        // 询问是否删除,确定删除则删除
        Modal.confirm({
          title: '确认要删除选中的数据吗？',
          onOk: () => {
            _this.delLoading = true;
            httpAction(_this.delParam.url, ids.join(','), _this.delParam.method)
              .then(res => {
                if (res.success) {
                  // 刷新表格
                  _this.reloadData(false, params.refName, () => {
                    // 提示成功
                    _this.$message.success('删除成功！');
                    _this.delLoading = false;
                  });
                } else {
                  _this.delLoading = false;
                }
              })
              .catch(() => {
                _this.delLoading = false;
              });
          }
        });
      } else if (newIdsArray.length === ids.length) {
        // 选中的均为未保存项,前台直接删除
        // 询问是否删除,确定删除则删除
        Modal.confirm({
          title: '确定删除吗？',
          onOk: () => {
            this.delLoading = true;
            let dataSource = [...this.data];
            for (let i = 0; i < newIdsArray.length; i++) {
              dataSource = dataSource.filter(item => item[this.rowKey] != newIdsArray[i]);
            }
            // 清空表格选中项
            this.selectedRowKeys = [];
            this.selectedRows = [];
            this.$refs[this.tableRefName].changeSelectedRowKeys([]);
            // 前台刷新表格
            this.data = dataSource;
            // 提示成功
            this.$message.success('删除成功！');
            this.delLoading = false;
          }
        });
      } else if (newIdsArray.length < ids.length) {
        // 选中的既有未保存的,又有编辑过的,给出提示
        Modal.warning({
          title: '请确保上一条数据修改完毕！'
        });
      }
    },
    /**
     * 获取字符串字符长度
     * @param {String} str 需要获取字符长度的字符串
     */
    getboeAbstractLength(str) {
      return str.length;
    },
    /**
     * 添加或者保存之前进行数据检验
     * @param {Array} checkData 被校验的数据
     * @param {Array} checkArray 校验规则
     * @param {Function} callbackSuccess 校验成功的回调函数
     * @param {Function} callbackError 校验失败的回调函数
     */
    handleAddOrSaveCheck(checkData, checkArray, callbackSuccess, callbackError) {
      for (let i = 0; i < checkData.length; i++) {
        if (checkArray) {
          for (let j = 0; j < checkArray.length; j++) {
            let target = checkData[i][checkArray[j].id];
            if (checkArray[j].isRequired) {
              // 必填校验
              if (
                target === '' ||
                target === null ||
                typeof target === 'undefined' ||
                (typeof target === 'string' && target.trim() === '')
              ) {
                message.warning('“' + checkArray[j].name + '”为必填字段！');
                if (callbackError) {
                  callbackError(false);
                }
                return false;
              }
            }
            if (checkArray[j].isCheckScript) {
              // 非法字符校验
              if (new RegExp('<script[^>]*?>.*?<\\/script>', 'ig').test(target)) {
                message.warning('“' + checkArray[j].name + '”非法含有JS脚本完整标签');
                if (callbackError) {
                  callbackError(false);
                }
                return false;
              }
            }
            if (checkArray[j].isCheckAllSpace) {
              // 不能输入空格校验
              if (new RegExp('[ ]+', 'ig').test(target)) {
                message.warning('“' + checkArray[j].name + '”不允许输入空格');
                if (callbackError) {
                  callbackError(false);
                }
                return false;
              }
            }
            if (checkArray[j].isCheckMaxLength) {
              // 长度校验
              if (typeof target === 'string' || typeof target === 'number') {
                if (this.getboeAbstractLength(target + '') > checkArray[j].maxLength) {
                  message.warning(
                    '“' + checkArray[j].name + '”不能超过' + checkArray[j].maxLength + '个字符'
                  );
                  if (callbackError) {
                    callbackError(false);
                  }
                  return false;
                }
              } else {
                if (
                  target != null &&
                  this.getboeAbstractLength(target.join(',')) > checkArray[j].maxLength
                ) {
                  message.warning(
                    '“' + checkArray[j].name + '”不能超过' + checkArray[j].maxLength + '个字符'
                  );
                  if (callbackError) {
                    callbackError(false);
                  }
                  return false;
                }
              }
            }
          }
        }
      }
      // 回调函数
      if (callbackSuccess) {
        callbackSuccess();
      }
    },
    /**
     * 安全Base64编码
     * @param value
     * @returns {*|string}
     */
    encodeMyBase64(value) {
      let wordArray = CryptoJS.enc.Utf8.parse(value);
      let base64 = CryptoJS.enc.Base64.stringify(wordArray);

      //将标准Base64编码中字符+和/分别替换为-和_
      base64 = base64.replace(/\+/g, '-');
      base64 = base64.replace(/\//g, '_');

      return base64;
    },
    /**
     * 接收单元格编辑组件--模态选择框中已选数据
     * 将数据代入表格数据中当前列指定的dataIndexName字段，值为当前列配置中指定的showField
     * @param {Object} obj 行编辑组件传回控件的值 形式: {record: record(行数据), col: col(行设置), value: value(控件值)}
     */
    handleCellModalSelect(obj) {
      // 获取传回的参数
      let record = obj.record;
      let selectedObj = obj.obj;
      // 更新表格数据
      let dataSource = [...this.data];
      let target = dataSource.find(item => item[this.rowKey] === record[this.rowKey]);
      if (target) {
        target._vue_editing = record._vue_editing; // 行编辑标志要传回去
        for (let key of selectedObj) {
          target[key] = selectedObj[key];
        }
        this.data = dataSource;
      }
      this.hasTableDataChanged = true;
    },
    /**
     * 接收单元格编辑组件数据,将数据代入表格
     * @param {Object} obj 行编辑组件传回控件的值 形式: {record: record(行数据), col: col(行设置), value: value(控件值)}
     */
    handleCellChange(obj) {
      // 获取传回的参数
      let record = obj.record;
      let col = obj.col;
      let value = obj.value;
      // 更新表格数据
      let dataSource = [...this.data];
      let target = dataSource.find(item => item[this.rowKey] === record[this.rowKey]);
      if (target) {
        target._vue_editing = record._vue_editing; // 行编辑标志要传回去
        if (col.type === 'commonselect') {
          target[col.dataIndex] = value.ids;
          if (col.dataIndexName) {
            target[col.dataIndexName] = value.names;
          } else {
            target[col.dataIndex + 'Alias'] = value.names;
          }
        } else if (col.type === 'select' || col.type === 'radio' || col.type === 'checkbox') {
          target[col.dataIndex] = value.key;
          if (col.dataIndexName) {
            target[col.dataIndexName] = value.label.trim();
          } else {
            target[col.dataIndex + 'Name'] = value.label.trim();
          }
        } else {
          target[col.dataIndex] = value;
        }
        this.data = dataSource;
      }
      this.hasTableDataChanged = true;
    },

    /**
     * 保存，包括整体保存与独行保存
     */
    handleSave(params) {
      let _this = this;
      // 规避正在保存时连续点击
      if (this.saveLoading) return;
      // 保存按钮打开loading状态
      if (!params.saveItem) {
        this.saveLoading = true;
      }
      // 排除掉组件中自己加的字段，以免影响判断
      let copyDatas = [];
      let recordOldData = [];
      if (params && params.saveItem) {
        copyDatas = JSON.parse(
          JSON.stringify(this.data.filter(item => item[this.rowKey] == params.id))
        );
        recordOldData = JSON.parse(
          JSON.stringify(this.dataRecord.filter(item => item[this.rowKey] == params.id))
        );
      } else {
        copyDatas = JSON.parse(JSON.stringify(this.data));
      }
      for (let i = 0; i < copyDatas.length; i++) {
        let obj = copyDatas[i];
        for (let key in obj) {
          if (key.indexOf('_vue_') != -1) {
            delete obj[key];
          }
        }
      }
      // 表格数据分为两部分: 新加的行newData;旧有的行oldData
      let newData = [];
      let oldData = [];
      for (let i = 0; i < copyDatas.length; i++) {
        if (copyDatas[i][this.rowKey].indexOf(this.newRowName) != -1) {
          newData.push(copyDatas[i]);
        } else {
          oldData.push(copyDatas[i]);
        }
      }
      // 新加的行数据是否编辑了,默认未编辑
      let newDataFlag = false;
      for (let i = 0; i < newData.length; i++) {
        let obj = newData[i];
        for (let key in obj) {
          if (key != this.rowKey && obj[key] != '' && typeof obj[key] != 'undefined') {
            newDataFlag = true; // 编辑过了
          }
        }
      }
      // 旧有的行数据是否有改动, 默认未改
      let oldDataFlag = false;
      if (params && params.saveItem) {
        if (!isEqual(oldData, recordOldData)) {
          oldDataFlag = true; // 改过了
        }
      } else {
        if (!isEqual(oldData, this.dataRecord)) {
          oldDataFlag = true; // 改过了
        }
      }
      // 判断表格的值有无变化,没有变化,提示修改数据
      if (!newDataFlag && !oldDataFlag) {
        if (!params.saveItem) {
          Modal.warning({
            title: '请先修改数据！'
          });
          this.saveLoading = false;
        } else {
          this.$message.warning('请先修改数据！');
        }
        return;
      }
      // 有改动的数据
      let changedData = [];
      // 获取旧有的行中编辑过的数据
      let difData = [];
      if (params && params.saveItem) {
        difData = differenceWith(oldData, recordOldData, isEqual);
      } else {
        difData = differenceWith(oldData, this.dataRecord, isEqual);
      }
      // 新加的行与编辑过的行,整体为有改动的数据
      changedData = [...newData, ...difData];
      // 校验字段设置: 必填, 非法含有脚本标签, 最大长度
      let checkArray = this.setCheckArray();
      // 传设置参数进行校验，检验通过后请求接口保存
      this.handleAddOrSaveCheck(
        changedData,
        checkArray,
        () => {
          // 保存提交数据整理
          let paramData = [];
          // 新加的行数据将主键删掉
          let newDataDeal = JSON.parse(JSON.stringify(newData));
          for (let i = 0; i < newDataDeal.length; i++) {
            delete newDataDeal[i][_this.rowKey];
          }
          // 新加的行与编辑过的行一起提交
          paramData = [...newDataDeal, ...difData];
          // 请求后台进行保存
          httpAction(_this.saveParam.url, JSON.stringify(paramData), _this.saveParam.method)
            .then(res => {
              if (res.success) {
                // 刷新表格
                _this.reloadData(false, _this.tableRefName, () => {
                  // 提示成功
                  _this.$message.success('保存成功！');
                  _this.saveLoading = false;
                });
              } else {
                _this.saveLoading = false;
              }
            })
            .catch(() => {
              _this.saveLoading = false;
            });
        },
        () => {
          this.saveLoading = false;
        }
      );
    },
    /**
     * 操作列编辑
     */
    handleEdit(params) {
      let datas = [...this.data];
      for (let i = 0; i < datas.length; i++) {
        if (params.param.id == datas[i][this.rowKey]) {
          datas[i]._vue_editing = true;
        }
      }
      this.data = datas;
    }
  }
};
