import '@assets/styles/common/base-surface-panel.less';
import '@/assets/styles/common/base-surface-toolbar.less';
import {
  Modal,
  Button,
  Icon,
  Input,
  Form,
  Row,
  Col,
  Tag,
  Divider,
  Radio,
  Checkbox,
  Select,
  InputNumber,
  DatePicker
} from 'ant-design-vue';
import AvicResizeLayout from '@comp/avic/pt/common/AvicResizeLayout';
import AvicResizeLayoutPanel from '@comp/avic/pt/common/AvicResizeLayout/AvicResizeLayoutPanel';
import AvicAsyncComponent from '@comp/avic/pt/common/AvicAsyncComponent/index';
import AvicBigTable from '@comp/avic/pt/common/AvicBigTable';
import AvicChangeTableSize from '@comp/avic/pt/common/AvicChangeTableSize';
import AvicTableColumnSettings from '@comp/avic/pt/common/AvicTableColumnSettings';
import moment from 'moment';
import { mapGetters } from 'vuex';
import { httpAction } from '@api/manage';
import config from '@config/defaultSettings';
export const bigTableMixin = {
  components: {
    AButton: Button,
    AIcon: Icon,
    AInput: Input,
    AInputSearch: Input.Search,
    AForm: Form,
    ARow: Row,
    ACol: Col,
    ATag: Tag,
    ADivider: Divider,
    ARadio: Radio,
    ACheckbox: Checkbox,
    ASelect: Select,
    AInputNumber: InputNumber,
    ADatePicker: DatePicker,
    AvicResizeLayout,
    AvicResizeLayoutPanel,
    AvicAsyncComponent,
    AvicImport: () => import('@comp/avic/pt/common/AvicButtons/AvicImport'),
    AvicExport: () => import('@comp/avic/pt/common/AvicButtons/AvicExport'),
    AvicDelete: () => import('@comp/avic/pt/common/AvicButtons/AvicDelete'),
    AvicBigTable,
    AvicChangeTableSize,
    AvicTableColumnSettings,
    AvicCommonSelect: () => import('@comp/avic/pt/org/AvicCommonSelect')
  },
  data() {
    return {
      isBigDataTable: false, // 是否大数据表格
      data: [], // 表格dataSource
      tableSize: '', // 表格尺寸
      pagination: {
        layouts: [
          'PrevJump',
          'PrevPage',
          'JumpNumber',
          'NextPage',
          'NextJump',
          'Sizes',
          'FullJump',
          'Total'
        ],
        pageSizes: config.tablePageSize,
        border: true,
        currentPage: 1,
        pageSize: config.defaultPageSize
      },
      page: '1', // 初始请求第几页
      rows: config.defaultPageSize, // 初始每页请求几条数据
      sidx: '', // 表格排序字段
      sord: '', // 排序方式: desc降序 asc升序
      selectedRowKeys: [], // 表格选中行主键集合
      selectedRows: [], // 表格选中行数据集合
      searchText: '', // 普通搜索input双向绑定值
      advancedSearchParam: {}, // 高级查询表单查询时传的请求参数
      isSearchByKeyWord: '', // 是否是基本查询
      isAdvancedSearch: '', // 是否是高级查询
      delLoading: false, // 删除按钮loading状态
      exportFileName: '', // 导出文件名称
      initSettingColumns: [], // 记录初始表格列配置，用于列显隐重置
      initSetData: [], // 记录初始个性化配置数据，用于列显隐重置
      setData: [], // 个性化配置数据
      showAddModal: false, // 是否展示添加弹窗
      showDetailModal: false, // 是否展示详情弹窗
      showEditModal: false, // 是否展示编辑弹窗
      paramId: '', // 初始传递参数,编辑数据Id，详情数据Id
      searchLoad: false // 高级查询加载控制
    };
  },
  created() {
    this.initSetData = this.getColSet(this.moduleName); // 初始个性化设置数据
    this.initSettingColumns = this.setSelectColumns(this.moduleName, this.initSetData); // 初始表格列配置
    this.setData = this.getColSet(this.moduleName); // 个性化设置数据
    this.tableSize = this.getTableSizeSet(this.moduleName); // 初始个性化表格尺寸数据
  },
  mounted() {
    if (this.moduleName) {
      // 导出文件名称
      this.exportFileName =
        this.moduleName.substring(3) + '_' + moment(new Date()).format('YYYYMMDD');
    }
  },
  methods: {
    /**
     * 页签右键刷新时,基础数据重置
     */
    menuRefreshReset() {
      // 重置分页
      this.page = '1';
      this.rows = config.defaultPageSize;
      // 重置排序
      this.sidx = '';
      this.sord = '';
      // 重置普通搜索
      this.searchText = '';
      // 重置高级查询表单
      if (this.$refs[this.advancedSearchRefName]) {
        this.$refs[this.advancedSearchRefName].searchForm.resetFields();
      }
      this.advancedSearchParam = {};
      // 重置选中
      this.selectedRowKeys = [];
      // 重新请求表格数据
      this.$refs[this.tableRefName].loadData(this.queryParam);
    },
    /**
     * 表格组件传递出的getData属性绑定的方法,传递出的值为obj,形式是{data: data} 此方法接收obj并做处理
     * @param {Object} obj 形式: {data: data(表格数据)}
     */
    getData(obj) {
      if (obj.data && obj.data.result && obj.data.result.length > 0) {
        this.data = obj.data.result; // 接收表格数据,渲染表格
      } else {
        this.data = [];
      }
    },
    /**
     * 刷新表格
     * @param {Boolean} isCurrent 是否定位到第一页
     * @param {String} refName 绑定表格的refName
     * @param {Function} callback 回调函数
     */
    reloadData(isCurrent, refName, callback) {
      let that = this;
      if (isCurrent) {
        that.page = 1;
      }
      that.$refs[refName].loadData(
        {
          url: that.queryParam.url,
          parameter: that.queryParam.parameter,
          method: that.queryParam.method
        },
        () => {
          // 表格组件loadData的回调函数: 刷新后,去掉选中项
          that.selectedRowKeys = [];
          that.$refs[refName].changeSelectedRowKeys([]);
          // 其他传入的回调函数
          if (callback) {
            callback();
          }
        }
      );
    },
    /**
     * 根据页面数据配置获取表头配置数据,判断点: isTableHidden是否为true
     */
    getColumns() {
      let pageSettingData = [...this.pageSettingData];
      let columns = [];
      for (let i = 0; i < pageSettingData.length; i++) {
        if (!pageSettingData[i].isTableHidden) {
          columns.push(pageSettingData[i]);
        }
      }
      return columns;
    },
    /**
     * 根据表格表头设置及个性化配置获取要显示的列及其顺序的表格表头设置
     * @param {String} key 模块名称
     */
    setSelectColumns(key, setData) {
      const columns = [...this.columns]; // 表格所有字段
      let realColumns = []; // 排序后的真实columns
      let unrealSelectColumns = []; // 个性化表格设置与表格所有字段的公共部分
      if (setData.length === 0) {
        // 个性化表格设置为空, 则表格展示所有字段，即：所有字段的visible设为true
        for (let i = 0; i < columns.length; i++) {
          columns[i].visible = true;
          realColumns.push(columns[i]);
        }
      } else {
        // 个性化表格设置不为空,则获取个性化表格设置与表格所有字段的公共部分
        for (let i = 0; i < columns.length; i++) {
          for (let j = 0; j < setData.length; j++) {
            if (setData[j].key === columns[i].dataIndex) {
              unrealSelectColumns.push(columns[i]);
            }
          }
        }
        if (unrealSelectColumns.length > 0) {
          // 个性化表格设置与表格所有字段有公共部分，则展示个性化设置字段
          for (let i = 0; i < setData.length; i++) {
            for (let j = 0; j < columns.length; j++) {
              if (setData[i].key == columns[j].dataIndex) {
                columns[j].visible = setData[i].visible;
                realColumns.push(columns[j]);
              }
            }
          }
        } else {
          // 个性化表格设置与表格所有字段没有公共部分,则表格展示所有字段，即：所有字段的visible设为true
          for (let i = 0; i < columns.length; i++) {
            columns[i].visible = true;
            realColumns.push(columns[i]);
          }
          // 个性化表格设置清空
          this.saveColSet(key, []);
        }
      }
      // 深拷贝
      let returnDealColumns = [];
      realColumns.map(item => {
        let newObj = Object.assign({}, item);
        returnDealColumns.push(newObj);
      });
      return returnDealColumns;
    },
    /**
     * 接收表格选中信息,并处理
     * @param {Object} obj 形式: {selectedRowKeys: selectedRowKeys(选中行主键集合)}
     */
    handleRowSelection(obj) {
      if (obj.selectedRowKeys) {
        this.selectedRowKeys = obj.selectedRowKeys;
      }
      if (obj.selectedRows) {
        this.selectedRows = obj.selectedRows;
      }
    },
    /**
     * 接收分页、排序、筛选信息,并处理
     * @param {String} refName 表格refName
     * @param {String} obj 形式: {pagination: pagination(分页信息), sorter: sorter(排序信息)}
     */
    handleTableChange(refName, obj) {
      // 清空表格选中项
      this.selectedRowKeys = [];
      // 接收分页、排序、筛选信息
      let pagination = obj.pagination; // 分页信息
      let sorter = obj.sorter; // 排序信息
      // 处理分页信息
      this.page = pagination.currentPage; // 页数
      this.rows = pagination.pageSize; // 每页条数
      // 处理排序信息
      if (sorter) {
        this.sidx = sorter.sidx || ''; // 排序字段
        this.sord = sorter.sord || ''; // 排序方式: desc降序 asc升序
      }
      // 请求表格数据
      this.$refs[refName].loadData({
        url: this.queryParam.url,
        parameter: this.queryParam.parameter,
        method: this.queryParam.method
      });
    },
    /**
     * 点击添加按钮,调用添加弹窗方法
     */
    handleAdd() {
      this.showAddModal = true;
    },
    /**
     * 点击跳详情 或者 点击编辑按钮,调用弹窗方法
     * @param {Object} params 形式: {refName: refName(弹窗refName), param: {id: id(行数据主键), ...(其他,待定)}}
     */
    handleDetailOrEdit(params) {
      let that = this;
      if (params && that.$refs[params.refName]) {
        // 采用直接注册加载组件和一般异步组件加载方式
        that.$refs[params.refName].modalHandleDetailOrEdit(params.param.id);
      }
    },
    /**
     * 点击跳详情按钮,调用弹窗方法（异步）
     * @param {Object} params 形式: {refName: refName(弹窗refName), param: {id: id(行数据主键), ...(其他,待定)}}
     */
    handleDetail(params) {
      this.paramId = params.param.id;
      this.showDetailModal = true;
    },
    /**
     * 点击编辑按钮,调用弹窗方法（异步）
     * @param {Object} params 形式: {refName: refName(弹窗refName), param: {id: id(行数据主键), ...(其他,待定)}}
     */
    handleEdit(params) {
      this.paramId = params.param.id;
      this.showEditModal = true;
    },
    /**
     * 删除处理
     * @param {Object} params 形式: {refName: refName(表格refName),ids: ids(删除项id集合数组)}
     */
    handleDelete(params) {
      if (!params) return;
      if (params.param && params.param.ids.length == 0) {
        this.$message.warning('请选择要删除的数据！');
        return;
      }
      if (params.isNeedAsk) {
        Modal.confirm({
          // 使用普通的按钮需要提示
          title: '确定删除吗？',
          onOk: () => {
            this.delLoading = true;
            httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
              .then(res => {
                if (res.success) {
                  this.$message.success('删除成功！'); // 提示成功
                  this.reloadData(false, params.refName); // 刷新表格
                }
                this.delLoading = false;
              })
              .catch(() => {
                this.delLoading = false;
              });
          }
        });
      } else {
        // 使用封装的删除按钮不必提示
        this.delLoading = true;
        httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
          .then(res => {
            if (res.success) {
              this.$message.success('删除成功！'); // 提示成功
              this.reloadData(false, params.refName); // 刷新表格
            }
            this.delLoading = false;
          })
          .catch(() => {
            this.delLoading = false;
          });
      }
    },
    /**
     * 导入
     * @param {Object} res 导入请求返回值
     */
    backImport(res) {
      if (res && res.code === '0') {
        this.menuRefreshReset(); // 刷新表格
      }
    },
    /**
     * 导出
     */
    backExport() {},
    /**
     * 根据页面数据配置获取高级查询表单配置数据,判断点: isFormShow是否为true
     */
    getAdvancedSearchFormData() {
      let pageSettingData = [...this.pageSettingData];
      let advancedSearchFormData = [];
      for (let i = 0; i < pageSettingData.length; i++) {
        if (!pageSettingData[i].isFormHidden) {
          advancedSearchFormData.push(pageSettingData[i]);
        }
      }
      return advancedSearchFormData;
    },
    /**
     * 获取普通搜索的汉字及英文字段
     */
    getSearchData() {
      let searchTips = new Array();
      let searchNames = new Array();
      for (let i = 0; i < this.columns.length; i++) {
        if (this.columns[i].isSearch) {
          searchTips.push(this.columns[i].title || this.columns[i].slots.content);
          searchNames.push(this.columns[i].dataIndex);
        }
      }
      let searchPlaceholder = '';
      for (let i = 0; i < searchTips.length; i++) {
        if (i == 0) {
          searchPlaceholder = searchPlaceholder + '请输入' + searchTips[i];
        } else {
          searchPlaceholder = searchPlaceholder + '或' + searchTips[i];
        }
      }
      // 返回拼接好的placeholder及字段数组
      return {
        searchPlaceholder,
        searchNames
      };
    },
    /**
     * 普通搜索,根据搜索内容刷新表格,从第一页开始
     * @param {String} refName 普通搜索绑定的表格refName
     */
    searchByKeyWord(refName) {
      this.isSearchByKeyWord = true;
      this.isAdvancedSearch = false;
      if (this.hideAdvanceSearch) {
        this.hideAdvanceSearch();
      }
      this.reloadData(true, refName);
    },
    /**
     * 接收高级查询数据,并处理
     * @param {Object} params 形式: {isDeal:isDeal(是否刷新表格), advancedSearchParam: advancedSearchParam(高级查询数据)}
     */
    advancedSearch(params) {
      // 接收高级查询数据,isDeal为true,则刷新表格,且从第一页开始
      if (!params) return;
      this.advancedSearchParam = params.param.advancedSearchParam;
      if (params.param.type === 'search' || params.param.type === 'reset') {
        this.isSearchByKeyWord = false;
        this.isAdvancedSearch = true;
      }
      if (params.param.isDeal) {
        this.reloadData(true, params.refName);
      }
    },
    /**
     * 获取key模块的个性化表格设置
     * @param {String} key 模块名
     */
    getColSet(key) {
      let cfgContent = this.getModuleCfg(key);
      return cfgContent ? (cfgContent.MK_COLUMNS_SORT ? cfgContent.MK_COLUMNS_SORT : []) : [];
    },
    /**
     * 保存key模块的个性化表格设置
     * @param {String} key 模块名
     * @param {Array} data 表格个性化设置项数组
     */
    saveColSet(key, data, callback) {
      this.$store.commit('SAVE_MODULE_COLUMNS', {
        key,
        val: data
      });
      if (callback) {
        callback();
      }
    },
    /**
     * 获取key模块的个性化表格尺寸设置
     * @param {String} key 模块名
     */
    getTableSizeSet(key) {
      let cfgContent = this.getModuleCfg(key);
      return cfgContent
        ? cfgContent.MK_TABLE_SIZE
          ? cfgContent.MK_TABLE_SIZE
          : 'middle'
        : 'middle';
    },
    /**
     * 保存moduleName模块的个性化表格尺寸设置
     * @param {String} moduleName 模块名
     * @param {String} key 自定义配置项
     * @param {String} val 自定义配置项内容
     */
    saveTableSizeSet(moduleName, key, val) {
      this.$store.commit('SAVE_MODULE_ITEM_CFG', {
        key: moduleName,
        itemKey: key,
        val
      });
    },
    /**
     * 列显隐发生改变或者列顺序发生改变时,表格展示项跟着变化,并且存入个性化接口
     * @param {Array} result 表格组件返回的表格显示项及其顺序数组
     * @param {String} key 模块名
     */
    changeColumnsDeal(result, key) {
      let setData = [];
      const columns = [...this.columns];
      for (let j = 0; j < result.length; j++) {
        for (let i = 0; i < columns.length; i++) {
          if (result[j].dataIndex === columns[i].dataIndex) {
            setData.push({
              key: columns[i].dataIndex,
              name: columns[i].title,
              visible: result[j].visible
            });
          }
        }
      }
      this.setData = [...setData];
      this.saveColSet(key, setData);
    },
    /**
     * 表格组件colMenuSelect属性绑定的方法
     * @param {Array} result 表格组件传递回的表格显示项及其顺序数组
     */
    changeColumns(result) {
      this.changeColumnsDeal(result, this.moduleName);
    },
    /**
     * 表格刷新
     */
    handleRefreshTable(refName) {
      this.$refs[refName].loadData(this.queryParam);
    },
    /**
     * 表格尺寸变化
     * @param {String} size 表格尺寸值
     */
    changeTableSize(size) {
      this.tableSize = size;
      this.saveTableSizeSet(this.moduleName, 'MK_TABLE_SIZE', size);
    }
  },
  computed: {
    /**
     * 引入个性化设置vuex的getter
     */
    ...mapGetters(['getModuleCfg']),
    /**
     * 表头columns全配置
     */
    columns() {
      return this.getColumns();
    },
    /**
     * 表格显示列的columns设置
     */
    selectColumns() {
      return this.setSelectColumns(this.moduleName, this.setData);
    },
    /**
     * 普通搜索的字段数组
     */
    searchNames() {
      return this.getSearchData().searchNames;
    },
    /**
     * 普通搜索的placeholder
     */
    searchPlaceholder() {
      return this.getSearchData().searchPlaceholder;
    },
    /**
     * 普通查询参数
     */
    searchParam() {
      if (this.searchText) {
        let obj = {};
        this.searchNames.map(item => {
          obj[item] = this.searchText;
        });
        return obj;
      } else {
        return {};
      }
    },
    /**
     * 获取高级查询配置数据
     */
    advancedSearchFormData() {
      return this.getAdvancedSearchFormData();
    },
    /**
     * 列表接口传的基本查询参数
     */
    postSearchParam() {
      let postSearchParam;
      if (this.isSearchByKeyWord) {
        postSearchParam = this.searchParam;
      } else {
        postSearchParam = null;
      }
      return postSearchParam;
    },
    /**
     * 列表接口传的高级查询参数
     */
    postAdvancedSearchParam() {
      let postAdvancedSearchParam;
      if (this.isAdvancedSearch) {
        postAdvancedSearchParam = this.advancedSearchParam;
      } else {
        postAdvancedSearchParam = null;
      }
      return postAdvancedSearchParam;
    },
    /**
     * 选中一条或多条数据才显示删除按钮
     */
    isShowDelete() {
      return this.selectedRowKeys.length > 0;
    },
    /**
     * 选中一条数据才显示编辑按钮
     */
    isShowEdit() {
      return this.selectedRowKeys.length === 1;
    },
    /**
     * 有数据才显示导出按钮
     */
    isShowExport() {
      return this.data.length > 0;
    },
    /**
     * 导出时间数据处理
     */
    exportData() {
      let exportData = JSON.parse(JSON.stringify(this.data));
      for (let i = 0; i < exportData.length; i++) {
        for (let j = 0; j < this.pageSettingData.length; j++) {
          if (this.pageSettingData[j].type === 'date') {
            if (exportData[i][this.pageSettingData[j].dataIndex]) {
              exportData[i][this.pageSettingData[j].dataIndex] = moment(
                exportData[i][this.pageSettingData[j].dataIndex]
              ).format(this.pageSettingData[j].format || 'YYYY-MM-DD');
            }
          }
          if (this.pageSettingData[j].type === 'datetime') {
            if (exportData[i][this.pageSettingData[j].dataIndex]) {
              exportData[i][this.pageSettingData[j].dataIndex] = moment(
                exportData[i][this.pageSettingData[j].dataIndex]
              ).format(this.pageSettingData[j].format || 'YYYY-MM-DD HH:mm:ss');
            }
          }
        }
      }
      return exportData;
    }
  }
};
