import TabsDropdown from '@views/avic/pt/system/portLet/portletDesign/TabsDropdown';
import PortalDesignModal from '@views/avic/pt/system/portLet/portletDesign/PortalDesignModal';
// import AvicAsyncComponent from '@comp/avic/pt/common/AvicAsyncComponent/index';
import { mapGetters } from 'vuex';

export const portletSettingMixin = {
  components: {
    TabsDropdown,
    PortalDesignModal
  },
  data() {
    return {
      /* 门户配置设计数据 */
      portalDesignData: null,
      /* 门户配置默认配置数据 */
      defaultWidgetForm: {
        list: [],
        config: { labelWidth: 100, labelPosition: 'right', size: 'small' }
      },
      /* 门户配置列表弹窗ref名称 */
      tabsDropRefName: 'tabsDropRefName',
      /* 门户配置列表显隐控制 */
      showDrop: false,
      /* 门户配置修改content接口 */
      updateForPortletUrl: '/api/appsys/portlet/portalConfig/updateForPortlet/v1',
      /* 门户配置信息 */
      portLet: {},
      /* 门户配置操作类型*/
      handleType: 'init',
      /* 保存门户配置JSon  */
      content: '',
      /* 设计器弹窗显隐控制  */
      showEditIndexModal: false,
      /* 设计器弹窗ref名称 */
      portalDesignRefName: 'portalDesignRefName',
      /* 设计器路径  */
      portalDesignPath: 'views/avic/pt/system/portLet/portletDesign/PortalDesignModal'
    };
  },
  watch: {},
  computed: {
    ...mapGetters(['getPortLet']),
    widgetForm() {
      let widgetForm = {};
      widgetForm = this.content;
      widgetForm.config['id'] = this.portLet.id; //保存门户配置id
      // 门户小页权限过滤
      let basicComponents = this.getPortLet('PORTALLET_SMALLPAGES');
      if (widgetForm && widgetForm.list.length > 0) {
        //权限判断
        let wlen = widgetForm.list.length;
        for (let i = 0; i < wlen; i++) {
          let isSwith = false;
          widgetForm.list[i].key = Date.parse(new Date()) + '_' + Math.ceil(Math.random() * 99999);
          if (basicComponents && basicComponents.length > 0) {
            let bcLen = basicComponents.length;
            for (let j = 0; j < bcLen; j++) {
              if (widgetForm.list[i].id == basicComponents[j].id) {
                isSwith = true;
                widgetForm.list[i].url = basicComponents[j].menuUrl;
              }
            }
          }
          if (!isSwith) {
            widgetForm.list[i].url = 'avic/pt/system/portLet/portLetCards/401';
          }
        }
      }
      return widgetForm;
    }
  },
  methods: {
    /*更新门户配置设计数据 */
    updateDesign(data) {
      this.portalDesignData = data;
    },
    /*打开门户桌面设计器*/
    openPortalDesign(res) {
      this.portLet = res;
      if (res.content) {
        this.handleType = 'edit';
        this.content = res.content;
      } else {
        this.handleType = 'add';
        this.content = this.defaultWidgetForm;
      }
      this.showEditIndexModal = true;
    },
    // 设计器关闭事件
    handeleCloseDesignMd() {
      this.showEditIndexModal = false;
    },
    /*删除操作更新tabsDropRefName列表*/
    updatePortLetList(withoutCloseMd) {
      // this.closeModal()
      if (!withoutCloseMd) {
        this.handeleCloseDesignMd(); //关闭设计器弹窗
      }
      this.$refs[this.tabsDropRefName][0].refreshData();
    }
  }
};
