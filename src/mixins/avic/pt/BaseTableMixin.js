import '@assets/styles/common/base-surface-panel.less';
import '@/assets/styles/common/base-surface-toolbar.less';
import {
  Modal,
  Button,
  Icon,
  Input,
  Form,
  Row,
  Col,
  Tag,
  Divider,
  Radio,
  Checkbox,
  Select,
  InputNumber,
  DatePicker
} from 'ant-design-vue';
import AvicResizeLayout from '@comp/avic/pt/common/AvicResizeLayout';
import AvicResizeLayoutPanel from '@comp/avic/pt/common/AvicResizeLayout/AvicResizeLayoutPanel';
import AvicAsyncComponent from '@comp/avic/pt/common/AvicAsyncComponent/index';
import AvicBaseTable from '@comp/avic/pt/common/AvicBaseTable';
import AvicChangeTableSize from '@comp/avic/pt/common/AvicChangeTableSize';
import AvicTableColumnSettings from '@comp/avic/pt/common/AvicTableColumnSettings';
import moment from 'moment';
import { mapGetters, mapActions } from 'vuex';
import { httpAction } from '@api/manage';
import config from '@config/defaultSettings';
export const baseTableMixin = {
  components: {
    AButton: Button,
    AIcon: Icon,
    AInput: Input,
    AInputSearch: Input.Search,
    AForm: Form,
    ARow: Row,
    ACol: Col,
    ATag: Tag,
    ADivider: Divider,
    ARadio: Radio,
    ACheckbox: Checkbox,
    ASelect: Select,
    AInputNumber: InputNumber,
    ADatePicker: DatePicker,
    AvicResizeLayout,
    AvicResizeLayoutPanel,
    AvicAsyncComponent,
    AvicImport: () => import('@comp/avic/pt/common/AvicButtons/AvicImport'),
    AvicExport: () => import('@comp/avic/pt/common/AvicButtons/AvicExport'),
    AvicDelete: () => import('@comp/avic/pt/common/AvicButtons/AvicDelete'),
    AvicBaseTable,
    AvicChangeTableSize,
    AvicTableColumnSettings,
    AvicCommonSelect: () => import('@comp/avic/pt/org/AvicCommonSelect')
  },
  data() {
    return {
      data: [], // 表格dataSource
      pagination: {
        // 分页
        showQuickJumper: true,
        showSizeChanger: true,
        defaultCurrent: 1,
        defaultPageSize: config.defaultPageSize,
        pageSizeOptions: config.tablePageSize,
        showTotal: (total, range) => {
          return '第' + range[0] + '到第' + range[1] + '条' + '  ' + '共' + total + '条';
        }
      },
      page: '1', // 初始请求第几页
      rows: config.defaultPageSize, // 初始每页请求几条数据
      sidx: '', // 表格排序字段
      sord: '', // 排序方式: desc降序 asc升序
      selectedRowKeys: [], // 表格选中行主键集合
      selectedRows: [], // 表格选中行数据集合
      searchText: '', // 普通搜索input双向绑定值
      advancedSearchParam: {}, // 高级查询表单查询时传的请求参数,搭配混入文件BaseSearchMixin.js 使用
      isAdvancedSearch: '', // 是否是高级查询
      isSearchByKeyWord: '', // 是否是基本查询
      delLoading: false, // 删除按钮loading状态
      exportFileName: '', // 导出文件名称
      setData: [], // 个性化配置数据
      showAddModal: false, // 是否展示添加弹窗
      showDetailModal: false, // 是否展示详情弹窗
      showEditModal: false, // 是否展示编辑弹窗
      paramId: '' // 初始传递参数,编辑数据Id，详情数据Id
    };
  },
  created() {
    this.loadModuleCustomCfg(this.moduleName);
  },
  mounted() {
    if (this.moduleName) {
      // 导出文件名称
      this.exportFileName =
        this.moduleName.substring(3) + '_' + moment(new Date()).format('YYYYMMDD');
    }
  },
  watch: {
    initSetData: {
      immediate: true,
      handler(val) {
        this.setData = val;
      }
    },
    cfgContent: {
      immediate: true,
      handler(val) {
        if (val && val.MK_PAGER_RULE) {
          this.pagination.defaultPageSize = val.MK_PAGER_RULE.rows || config.defaultPageSize;
          this.rows = val.MK_PAGER_RULE.rows || config.defaultPageSize;
          this.sord = val.MK_PAGER_RULE.sord || '';
          this.sidx = val.MK_PAGER_RULE.sidx || '';
          // // 重新请求表格数据
          // this.$refs[this.tableRefName].loadData(this.queryParam);
        }
      }
    }
  },
  methods: {
    ...mapActions(['loadModuleCustomCfg']),
    /**
     * 页签右键刷新时,基础数据重置
     */
    menuRefreshReset() {
      // 重置分页
      this.page = '1';
      this.rows = this.cfgContent.MK_PAGE_SIZE || config.defaultPageSize;
      // 重置排序
      this.sidx = this.cfgContent.MK_SIDX || '';
      this.sord = this.cfgContent.MK_SORD || '';
      // 重置普通搜索
      this.searchText = '';
      // 重置高级查询表单
      if (this.$refs[this.advancedSearchRefName]) {
        this.$refs[this.advancedSearchRefName].clearSearchParam();
      }
      this.advancedSearchParam = {};
      // 重置选中
      this.selectedRowKeys = [];
      this.selectedRows = [];
      // 重新请求表格数据
      this.$refs[this.tableRefName].loadData(this.queryParam);
    },
    /**
     * 表格组件传递出的getData属性绑定的方法,传递出的值为obj,形式是{data: data} 此方法接收obj并做处理
     * @param {Object} obj 形式: {data: data(表格数据)}
     */
    getData(obj) {
      if (obj.data && obj.data.result && obj.data.result.length > 0) {
        this.data = obj.data.result; // 接收表格数据,渲染表格
      } else {
        this.data = [];
      }
    },
    /**
     * 刷新表格
     * @param {Boolean} isCurrent 是否定位到第一页
     * @param {String} refName 绑定表格的refName
     * @param {Function} callback 回调函数
     */
    reloadData(isCurrent, refName, callback) {
      let that = this;
      if (isCurrent) {
        that.page = 1;
      }
      that.$refs[refName].loadData(
        {
          url: that.queryParam.url,
          parameter: that.queryParam.parameter,
          method: that.queryParam.method
        },
        () => {
          // 表格组件loadData的回调函数: 刷新后,去掉选中项
          that.selectedRowKeys = [];
          this.selectedRows = [];
          that.$refs[refName].changeSelectedRowKeys([]);
          // 其他传入的回调函数
          if (callback) {
            callback();
          }
        }
      );
    },
    /**
     * 根据页面数据配置获取表头配置数据,判断点: isTableHidden是否为true
     */
    getColumns() {
      let pageSettingData = [...this.pageSettingData];
      let columns = [];
      for (let i = 0; i < pageSettingData.length; i++) {
        if (!pageSettingData[i].isTableHidden) {
          columns.push(pageSettingData[i]);
        }
      }
      return columns;
    },
    /**
     * 根据表格表头设置及个性化配置获取要显示的列及其顺序的表格表头设置
     * @param {String} moduleName 模块名称
     */
    setSelectColumns(moduleName, setData) {
      const columns = [...this.columns]; // 表格所有字段
      let realColumns = []; // 排序后的真实columns
      let unrealSelectColumns = []; // 个性化表格设置与表格所有字段的公共部分
      if (setData.length === 0) {
        // 个性化表格设置为空, 则表格展示所有字段，即：所有字段的visible设为true
        for (let i = 0; i < columns.length; i++) {
          columns[i].visible = true;
          realColumns.push(columns[i]);
        }
      } else {
        // 个性化表格设置不为空,则获取个性化表格设置与表格所有字段的公共部分
        for (let i = 0; i < columns.length; i++) {
          for (let j = 0; j < setData.length; j++) {
            if (setData[j].key === columns[i].dataIndex) {
              unrealSelectColumns.push(columns[i]);
            }
          }
        }
        if (unrealSelectColumns.length > 0) {
          // 个性化表格设置与表格所有字段有公共部分，则展示个性化设置字段
          for (let i = 0; i < setData.length; i++) {
            for (let j = 0; j < columns.length; j++) {
              if (setData[i].key == columns[j].dataIndex) {
                columns[j].visible = setData[i].visible;
                realColumns.push(columns[j]);
              }
            }
          }
        } else {
          // 个性化表格设置与表格所有字段没有公共部分,则表格展示所有字段，即：所有字段的visible设为true
          for (let i = 0; i < columns.length; i++) {
            columns[i].visible = true;
            realColumns.push(columns[i]);
          }
          // 个性化表格设置清空
          this.saveColSet(moduleName, []);
        }
      }
      // 深拷贝
      let returnDealColumns = [];
      realColumns.map(item => {
        let newObj = Object.assign({}, item);
        if (this.sidx && item.dataIndex === this.sidx) {
          let sortOrder = !this.sord ? false : this.sord;
          sortOrder = sortOrder === 'asc' ? 'ascend' : sortOrder;
          sortOrder = sortOrder === 'desc' ? 'descend' : sortOrder;

          newObj.sortOrder = sortOrder;
        }
        returnDealColumns.push(newObj);
      });
      return returnDealColumns;
    },
    /**
     * 接收表格选中信息,并处理
     * @param {Object} obj 形式: {selectedRowKeys: selectedRowKeys(选中行主键集合),selectedRows: selectedRows(选中行数据集合)}
     */
    handleRowSelection(obj) {
      if (obj.selectedRowKeys) {
        this.selectedRowKeys = obj.selectedRowKeys;
      }
      if (obj.selectedRows) {
        this.selectedRows = obj.selectedRows;
      }
    },
    /**
     * 接收分页、排序、筛选信息,并处理
     * @param {String} refName 表格refName
     * @param {String} obj 形式: {pagination: pagination(分页信息), sorter: sorter(排序信息), filters: filters(筛选信息)}
     */
    handleTableChange(refName, obj) {
      // 清空表格选中项
      this.selectedRowKeys = [];
      this.selectedRows = [];
      // 接收分页、排序、筛选信息
      let pagination = obj.pagination; // 分页信息
      let sorter = obj.sorter; // 排序信息
      // 处理分页信息
      this.page = pagination.current; // 页数
      this.rows = pagination.pageSize; // 每页条数
      let customCfg = { rows: this.rows };
      // 处理排序信息
      if (sorter) {
        this.sidx = sorter.field || ''; // 排序字段
        if (sorter.order) {
          this.sord = sorter.order === 'ascend' ? 'asc' : 'desc'; // 排序方式: desc降序 asc升序
        } else {
          this.sord = '';
        }
        customCfg.sidx = this.sidx;
        customCfg.sord = this.sord;
      }

      this.$store.commit('SAVE_MODULE_PAGER_RULE', {
        key: this.moduleName,
        val: customCfg
      });
      // 请求表格数据
      this.$refs[refName].loadData({
        url: this.queryParam.url,
        parameter: this.queryParam.parameter,
        method: this.queryParam.method
      });
    },
    /**
     * 点击添加按钮,调用添加弹窗方法
     */
    handleAdd() {
      this.showAddModal = true;
    },
    /**
     * 点击跳详情 或者 点击编辑按钮,调用弹窗方法
     * @param {Object} params 形式: {refName: refName(弹窗refName), param: {id: id(行数据主键), ...(其他,待定)}}
     */
    handleDetailOrEdit(params) {
      let that = this;
      if (params && that.$refs[params.refName]) {
        // 采用直接注册加载组件和一般异步组件加载方式
        that.$refs[params.refName].modalHandleDetailOrEdit(params.param.id);
      }
    },
    /**
     * 点击跳详情按钮,调用弹窗方法（异步）
     * @param {Object} params 形式: {refName: refName(弹窗refName), param: {id: id(行数据主键), ...(其他,待定)}}
     */
    handleDetail(params) {
      this.paramId = params.param.id;
      this.showDetailModal = true;
    },
    /**
     * 点击编辑按钮,调用弹窗方法（异步）
     * @param {Object} params 形式: {refName: refName(弹窗refName), param: {id: id(行数据主键), ...(其他,待定)}}
     */
    handleEdit(params) {
      this.paramId = params.param.id;
      this.showEditModal = true;
    },
    /**
     * 删除处理
     * @param {Object} params 形式: {refName: refName(表格refName),ids: ids(删除项id集合数组),isNeedAsk: isNeedAsk(是否需要提示)}
     */
    handleDelete(params) {
      if (!params) return;
      if (params.param && params.param.ids.length == 0) {
        this.$message.warning('请选择要删除的数据！');
        return;
      }
      if (params.isNeedAsk) {
        Modal.confirm({
          // 使用普通的按钮需要提示
          title: '确定删除吗？',
          onOk: () => {
            this.delLoading = true;
            httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
              .then(res => {
                if (res.success) {
                  this.$message.success('删除成功！'); // 提示成功
                  this.reloadData(false, params.refName); // 刷新表格
                }
                this.delLoading = false;
              })
              .catch(() => {
                this.delLoading = false;
              });
          }
        });
      } else {
        // 使用封装的删除按钮不必提示
        this.delLoading = true;
        httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
          .then(res => {
            if (res.success) {
              this.$message.success('删除成功！'); // 提示成功
              this.reloadData(false, params.refName); // 刷新表格
            }
            this.delLoading = false;
          })
          .catch(() => {
            this.delLoading = false;
          });
      }
    },
    /**
     * 导入
     * @param {Object} res 导入请求返回值
     */
    backImport(res) {
      if (res && res.code === '0') {
        this.menuRefreshReset(); // 刷新表格
      }
    },
    /**
     * 导出
     */
    backExport() {},
    /**
     * 获取普通搜索的汉字及英文字段
     */
    getSearchData() {
      let searchTips = new Array();
      let searchNames = new Array();
      for (let i = 0; i < this.columns.length; i++) {
        if (this.columns[i].isSearch) {
          searchTips.push(this.columns[i].title || this.columns[i].slots.content);
          searchNames.push(this.columns[i].dataIndex);
        }
      }
      let searchPlaceholder = '';
      for (let i = 0; i < searchTips.length; i++) {
        if (i == 0) {
          searchPlaceholder = searchPlaceholder + '请输入' + searchTips[i];
        } else {
          searchPlaceholder = searchPlaceholder + '或' + searchTips[i];
        }
      }
      // 返回拼接好的placeholder及字段数组
      return {
        searchPlaceholder,
        searchNames
      };
    },
    /**
     * 普通搜索,根据搜索内容刷新表格,从第一页开始
     * @param {String} refName 普通搜索绑定的表格refName
     */
    searchByKeyWord(refName) {
      this.isSearchByKeyWord = true;
      this.isAdvancedSearch = false;
      if (this.hideAdvanceSearch) {
        this.hideAdvanceSearch();
      }
      this.reloadData(true, refName);
    },
    /**
     * 保存key模块的个性化表格设置
     * @param {String} key 模块名
     * @param {Array} data 表格个性化设置项数组
     */
    saveColSet(key, data, callback) {
      this.$store.commit('SAVE_MODULE_COLUMNS', {
        key,
        val: data
      });
      if (callback) {
        callback();
      }
    },
    /**
     * 保存moduleName模块的个性化表格尺寸设置
     * @param {String} moduleName 模块名
     * @param {String} key 自定义配置项
     * @param {String} val 自定义配置项内容
     */
    saveTableSizeSet(moduleName, key, val) {
      this.$store.commit('SAVE_MODULE_ITEM_CFG', {
        key: moduleName,
        itemKey: key,
        val
      });
    },
    /**
     * 列显隐发生改变或者列顺序发生改变时,表格展示项跟着变化,并且存入个性化接口
     * @param {Array} result 表格组件返回的表格显示项及其顺序数组
     * @param {String} key 模块名
     */
    changeColumnsDeal(result, key) {
      let setData = [];
      const columns = [...this.columns];
      for (let j = 0; j < result.length; j++) {
        for (let i = 0; i < columns.length; i++) {
          if (result[j].dataIndex === columns[i].dataIndex) {
            setData.push({
              key: columns[i].dataIndex,
              name: columns[i].title,
              visible: result[j].visible
            });
          }
        }
      }
      this.setData = [...setData];
      this.saveColSet(key, setData);
    },
    /**
     * 表格组件colMenuSelect属性绑定的方法
     * @param {Array} result 表格组件传递回的表格显示项及其顺序数组
     */
    changeColumns(result) {
      this.changeColumnsDeal(result, this.moduleName);
    },
    /**
     * 表格刷新
     */
    handleRefreshTable(refName) {
      this.$refs[refName].loadData(this.queryParam);
    },
    /**
     * 表格尺寸变化
     * @param {String} size 表格尺寸值
     */
    changeTableSize(size) {
      this.saveTableSizeSet(this.moduleName, 'MK_TABLE_SIZE', size);
    }
  },
  computed: {
    /**
     * 引入个性化设置vuex的getter
     */
    ...mapGetters(['getModuleCfg']),
    /**
     * 模块个性化配置数据
     */
    cfgContent() {
      return this.getModuleCfg(this.moduleName);
    },
    // 可响应个性化配置数据变化的初始化设置数据
    initSetData() {
      return this.cfgContent
        ? this.cfgContent.MK_COLUMNS_SORT
          ? this.cfgContent.MK_COLUMNS_SORT
          : []
        : [];
    },
    // 可响应个性化配置数据变化的模块列配置
    initSettingColumns() {
      return this.setSelectColumns(this.moduleName, this.initSetData);
    },
    // 可响应个性化配置数据变化的表格行间距设置
    tableSize() {
      return this.cfgContent
        ? this.cfgContent.MK_TABLE_SIZE
          ? this.cfgContent.MK_TABLE_SIZE
          : 'middle'
        : 'middle';
    },
    /**
     * 表头columns全配置
     */
    columns() {
      return this.getColumns();
    },
    /**
     * 表格显示列的columns设置
     */
    selectColumns() {
      return this.setSelectColumns(this.moduleName, this.setData);
    },
    /**
     * 普通搜索的字段数组
     */
    searchNames() {
      return this.getSearchData().searchNames;
    },
    /**
     * 普通搜索的placeholder
     */
    searchPlaceholder() {
      return this.getSearchData().searchPlaceholder;
    },
    /**
     * 普通查询参数
     */
    searchParam() {
      if (this.searchText) {
        let obj = {};
        this.searchNames.map(item => {
          obj[item] = this.searchText;
        });
        return obj;
      } else {
        return {};
      }
    },

    /**
     * 列表接口传的基本查询参数
     */
    postSearchParam() {
      let postSearchParam;
      if (this.isSearchByKeyWord) {
        postSearchParam = this.searchParam;
      } else {
        postSearchParam = null;
      }
      return postSearchParam;
    },
    /**
     * 列表接口传的高级查询参数
     */
    postAdvancedSearchParam() {
      let postAdvancedSearchParam;
      if (this.isAdvancedSearch) {
        postAdvancedSearchParam = this.advancedSearchParam;
      } else {
        postAdvancedSearchParam = null;
      }
      return postAdvancedSearchParam;
    },
    /**
     * 选中一条或多条数据才显示删除按钮
     */
    isShowDelete() {
      return this.selectedRowKeys.length > 0;
    },
    /**
     * 选中一条数据才显示编辑按钮
     */
    isShowEdit() {
      return this.selectedRowKeys.length === 1;
    },
    /**
     * 有数据才显示导出按钮
     */
    isShowExport() {
      return this.data.length > 0;
    },
    /**
     * 导出时间数据处理
     */
    exportData() {
      let exportData = JSON.parse(JSON.stringify(this.data));
      for (let i = 0; i < exportData.length; i++) {
        for (let j = 0; j < this.pageSettingData.length; j++) {
          if (this.pageSettingData[j].type === 'date') {
            if (exportData[i][this.pageSettingData[j].dataIndex]) {
              exportData[i][this.pageSettingData[j].dataIndex] = moment(
                exportData[i][this.pageSettingData[j].dataIndex]
              ).format(this.pageSettingData[j].format || 'YYYY-MM-DD');
            }
          }
          if (this.pageSettingData[j].type === 'datetime') {
            if (exportData[i][this.pageSettingData[j].dataIndex]) {
              exportData[i][this.pageSettingData[j].dataIndex] = moment(
                exportData[i][this.pageSettingData[j].dataIndex]
              ).format(this.pageSettingData[j].format || 'YYYY-MM-DD HH:mm:ss');
            }
          }
        }
      }
      return exportData;
    }
  }
};
