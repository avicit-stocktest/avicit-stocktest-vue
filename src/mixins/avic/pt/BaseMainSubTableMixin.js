import { Modal } from 'ant-design-vue';
import { httpAction } from '@/api/manage';
// import { formDataPretreatment } from '@utils/avic/common/formDataPretreatment';
// import '@assets/styles/css/main-sub-table.css';
import moment from 'moment';
export const baseMainSubTableMixin = {
  data() {
    return {
      mainSubAdvancedSearchParam: {},
      mainSubSearchParam: {},
      mainTableSelectRowKeys: []
    };
  },
  methods: {
    /**
     * 接收分页、排序、筛选信息,并处理
     * @param {String} refName 表格refName
     * @param {String} obj 形式: {pagination: pagination(分页信息), sorter: sorter(排序信息)}
     */
    handleMainTableChange(refName, obj) {
      // 接收分页、排序、筛选信息
      let pagination = obj.pagination; // 分页信息
      let sorter = obj.sorter; // 排序信息
      // 处理分页信息
      this.page = pagination.current; // 页数
      this.rows = pagination.pageSize; // 每页条数
      // 处理排序信息
      if (sorter) {
        this.sidx = sorter.field || ''; // 排序字段
        if (sorter.order) {
          this.sord = sorter.order === 'ascend' ? 'asc' : 'desc'; // 排序方式: desc降序 asc升序
        } else {
          this.sord = '';
        }
      }
      // 请求表格数据
      this.$refs[refName].loadData(
        {
          url: this.queryParam.url,
          parameter: this.queryParam.parameter,
          method: this.queryParam.method
        },
        () => {
          // 选中首行
          this.selectedRowKeys = this.data.length ? [this.data[0][this.rowKey]] : [];
          this.selectedRows = this.data.length ? this.data[0] : [];
          this.setMainId(this.selectedRowKeys);
          this.$refs[refName].changeSelectedRowKeys(this.selectedRowKeys);
        }
      );
    },
    setMainId(val) {
      this.mainTableSelectRowKeys = val;
    },
    //主子表页面右键刷新
    mainMenuRefreshReset() {
      let that = this;
      // 右键刷新时,基础数据重置
      // 重置分页
      this.page = '1';
      this.rows = '15';
      // 重置排序
      this.sidx = '';
      this.sord = '';
      // 重置普通搜索
      this.searchText = '';
      // 重置高级查询表单
      if (this.$refs[this.advancedSearchRefName]) {
        this.$refs[this.advancedSearchRefName].searchForm.resetFields();
      }
      this.advancedSearchParam = {};
      // 重置选中
      this.selectedRowKeys = [];
      this.selectedRows = [];
      // 重新请求表格数据
      that.$refs[that.mainTableRefName].loadData(that.queryParam, function () {
        if (that.data && that.data[0]) {
          that.selectedRowKeys = [that.data[0].id];
          that.selectedRows = [that.data[0]];
        }
        that.$refs[that.mainTableRefName].changeSelectedRowKeys(that.selectedRowKeys);
        that.setMainId(that.selectedRowKeys);
      });
    },
    /**
     * 接收表格选中信息,并处理
     * @param {Object} obj 形式: {selectedRowKeys: selectedRowKeys(选中行主键集合),selectedRows: selectedRows(选中行数据集合)}
     */
    handleRowSelectionMain(obj) {
      if (obj.selectedRowKeys) {
        this.selectedRowKeys = obj.selectedRowKeys;
        this.setMainId(obj.selectedRowKeys);
      }
      if (obj.selectedRows) {
        this.selectedRows = obj.selectedRows;
      }
    },
    //主表删除
    mainHandleDelete(params) {
      if (!params) return;
      if (!params.param.ids.length) {
        this.$message.warning('请选择要删除的数据！');
        return;
      }
      // 点击删除按钮
      Modal.confirm({
        title: '确定删除吗？',
        onOk: () => {
          this.delLoading = true;
          httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
            .then(res => {
              if (res.success) {
                // 提示成功
                this.$message.success('删除成功！');
                this.reloadMainData(false, params.refName, params.param.type);
                this.delLoading = false;
              } else {
                this.delLoading = false;
              }
            })
            .catch(() => {
              this.delLoading = false;
            });
        }
      });
    },
    //子表删除
    subHandleDelete(params) {
      if (!params) return;
      if (!params.param.ids.length) {
        this.$message.warning('请选择要删除的数据！');
        return;
      }
      // 点击删除按钮
      Modal.confirm({
        title: '确定删除吗？',
        onOk: () => {
          this.delLoading = true;
          httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
            .then(res => {
              if (res.success) {
                // 提示成功
                this.$message.success('删除成功！');
                this.reloadSubData(false, params.refName, params.param.type);
                this.delLoading = false;
              } else {
                this.delLoading = false;
              }
            })
            .catch(() => {
              this.delLoading = false;
            });
        }
      });
    },
    //主表高级查询
    advancedSearch(params) {
      // 接收高级查询数据,isDeal为true,则刷新表格,且从第一页开始
      if (!params) return;
      this.advancedSearchParam = params.param.advancedSearchParam;
      this.mainSubAdvancedSearchParam = { ...this.advancedSearchParam }; // 高级查询参数
      if (params.param.type === 'search' || params.param.type === 'reset') {
        this.mainSubSearchParam = null; // 基本查询参数设为null
      }
      if (params.param.isDeal) {
        this.reloadMainData(true, params.refName, '');
      }
    },
    //子表高级查询
    subAdvancedSearch(params) {
      // 接收高级查询数据,isDeal为true,则刷新表格,且从第一页开始
      if (!params) return;
      this.advancedSearchParam = params.param.advancedSearchParam;
      this.mainSubAdvancedSearchParam = { ...this.advancedSearchParam }; // 高级查询参数
      if (params.param.type === 'search' || params.param.type === 'reset') {
        this.mainSubSearchParam = null; // 基本查询参数设为null
      }
      if (params.param.isDeal) {
        this.reloadSubData(true, params.refName, '');
      }
    },
    //基本查询
    searchByKeyWord(params) {
      // 普通搜索,根据搜索内容刷新表格,从第一页开始
      this.mainSubAdvancedSearchParam = null; // 高级查询参数 设为null
      this.mainSubSearchParam = { ...this.searchParam }; // 基本查询参数
      if (this.hideAdvanceSearch) {
        this.hideAdvanceSearch();
      }
      let that = this;
      if (params && params.param.type == 'main') {
        this.$refs[params.refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.selectedRows = [that.data[0]];
              that.setMainId(that.selectedRowKeys);
            } else {
              that.selectedRowKeys = [];
              that.selectedRows = [];
              that.setMainId(['-1']);
            }
            that.$refs[params.refName].changeSelectedRowKeys(that.selectedRowKeys);
          }
        );
      } else {
        this.$refs[params.refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.selectedRows = [that.data[0]];
            } else {
              that.selectedRowKeys = [];
              that.selectedRows = [];
            }
            that.$refs[params.refName].changeSelectedRowKeys(that.selectedRowKeys);
          }
        );
      }
    },
    //重载主表数据
    reloadMainData(current, refName, type) {
      let that = this;
      if (current) {
        this.page = 1;
      }
      if (type && (type == 'delete' || type == 'add')) {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.selectedRows = [that.data[0]];
              that.setMainId(that.selectedRowKeys);
            } else {
              that.selectedRowKeys = [];
              that.selectedRows = [];
              that.setMainId(['-1']);
            }
            that.$refs[refName].changeSelectedRowKeys(that.selectedRowKeys);
          }
        );
      } else {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.selectedRows = [that.data[0]];
              that.setMainId(that.selectedRowKeys);
            } else {
              that.selectedRowKeys = [];
              that.selectedRows = [];
              that.setMainId(['-1']);
            }
            that.$refs[refName].changeSelectedRowKeys(that.selectedRowKeys);
          }
        );
      }
    },
    //重载子表数据
    reloadSubData(current, refName, type) {
      let that = this;
      if (current) {
        this.page = 1;
      }
      if (type && (type == 'delete' || type == 'add')) {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.selectedRows = [that.data[0]];
            } else {
              that.selectedRowKeys = [];
              that.selectedRows = [];
            }
            that.$refs[refName].changeSelectedRowKeys(that.selectedRowKeys);
          }
        );
      } else {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.selectedRows = [that.data[0]];
            } else {
              that.selectedRowKeys = [];
              that.selectedRows = [];
            }
            that.$refs[refName].changeSelectedRowKeys(that.selectedRowKeys);
          }
        );
      }
    },
    //添加子表数据
    subHandleAdd(params) {
      // 点击添加按钮,调用添加弹窗方法
      if (this.mainId.length != 1 || this.mainId[0] == '-1') {
        this.$message.warning('请选择一条主表数据！');
        return;
      } else {
        this.showAddModal = true;
      }
    },
    modalHandleDetailOrEdit(id) {
      // 点击编辑按钮或者查看详情时,回显数据
      this.form.resetFields(); // 清空表单
      if (this.formId) {
        this.formId = '';
      }
      this.visible = true; // 打开弹窗
      this.loadLookUp(this.modalFormLookupParams); // 获取通用代码
      let that = this;
      httpAction(this.detailMainParam.url + id, '', this.detailMainParam.method) // 请求数据
        .then(res => {
          if (res.success) {
            this.formId = res.result.id; // 设置附件formId
            this.loadSubData(id);
            // 从返回结果中抽取本页面表单字段,原因: 多余表单字段的字段不能set到表单里
            let data = new Object();
            for (let i = 0; i < this.modalFormData.length; i++) {
              data[this.modalFormData[i].dataIndex] = res.result[this.modalFormData[i].dataIndex];
            }
            // 从本页面表单字段抽取值不为null的字段,原因: 字段值为null,set不到表单里
            let newData = new Object();
            for (let key in data) {
              if (data[key] !== null && data[key] !== '') {
                newData[key] = data[key];
              }
            }
            // 对时间、多选框等前后台数据格式不一致的字段进行数据处理
            for (let key in newData) {
              for (let i = 0; i < that.modalFormData.length; i++) {
                if (
                  (that.modalFormData[i].dataIndex === key &&
                    that.modalFormData[i].type === 'date') ||
                  (that.modalFormData[i].dataIndex === key &&
                    that.modalFormData[i].type === 'datetime')
                ) {
                  newData[key] = moment(data[key]);
                } else if (
                  that.modalFormData[i].dataIndex === key &&
                  that.modalFormData[i].type === 'checkbox'
                ) {
                  newData[key] = data[key].split(';');
                } else if (
                  that.modalFormData[i].dataIndex === key &&
                  that.modalFormData[i].type === 'commonselect'
                ) {
                  newData[key] = { ids: data[key], names: res.result[key + 'Alias'] };
                }
              }
            }
            this.form.setFieldsValue(newData); // 将处理好的数据塞入本页面表单
          }
        });
    },
    loadSubData(id) {
      if (id) {
        let postData = {
          url: this.detailSubParam.url + id,
          parameter: '',
          method: this.detailSubParam.method
        };
        this.$refs[this.tableRefName].loadData(postData);
      }
    },
    //主子表流程方法
    //流程状态筛选,联动子表数据
    mainChangeFlowState(e, params) {
      this.bpmState = e;
      // 进入页面,加载表格数据
      this.reloadMainData(false, params.refName, '');
    }
  }
};
