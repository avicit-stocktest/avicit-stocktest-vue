import { httpAction } from '@/api/manage';
import AvicTree from '@/components/avic/pt/common/AvicTree';
import AvicInitTreeRootNode from '@/components/avic/pt/common/AvicTree/AvicInitTreeRootNode';
import AvicResizeLayout from '@comp/avic/pt/common/AvicResizeLayout';
import AvicResizeLayoutPanel from '@comp/avic/pt/common/AvicResizeLayout/AvicResizeLayoutPanel';
import { getContentHeight } from '@utils/avic/layout/baseMouldStyles';
import { getTreeById } from '@/utils/avic/common/treeUtils';
export const baseTreeListMixin = {
  components: {
    AvicTree,
    AvicInitTreeRootNode,
    AvicResizeLayout,
    AvicResizeLayoutPanel
  },
  data() {
    return {
      parentNodeId: '', // 添加节点的初始父节点
      nodeId: '', // 编辑节点Id
      treeWidth: 240, //树区域默认宽度
      treeStyle: {}, //树列表样式
      loadTreeParentId: '', //当前加载的树节点id
      selectedKeys: [],
      buttonPermissions: true, //是否显示操作按钮
      menuVisible: false, // 右键树菜单
      selectTree: null, // 右键点击的树
      treeLoading: false, // 树加载状态
      initTreeDataVisible: false,
      menuList: [
        { key: '1', text: '添加平级节点', disabled: false },
        { key: '2', text: '添加子节点' },
        { key: '3', text: '编辑节点' },
        { key: '4', text: '删除节点' }
      ]
    };
  },
  created() {
    // 当前加载的树节点id
    this.loadTreeParentId = this.defaultParentId;
    // 进入页面,加载树列表数据
    this.treeParam.url = this.requestTreeUrl + this.defaultLevel + '/' + this.defaultParentId;
  },
  methods: {
    // 显示右键菜单
    showRMenu(e) {
      if (e.node.$options.propsData.dataRef) {
        this.menuVisible = true;
        this.selectTree = e.node.$options.propsData.dataRef;
        if (this.selectTree != null) {
          if (this.defaultParentId == this.selectTree.parentId) {
            this.menuList[0].disabled = true;
            this.menuList[3].disabled = true;
          } else {
            this.menuList[0].disabled = false;
            this.menuList[3].disabled = false;
          }
        }
      }
    },
    // 点击树右键菜单的操作项
    onMenuSelect(obj) {
      let e = obj.key;
      if (obj.selectTreeNode) {
        this.selectTree = obj.selectTreeNode;
        this.onSelected = this.selectTree.id;
        this.selectedKeys = [this.selectTree.id]; //选中的节点编号
        // 设置树的选中节点为右键操作的节点
        this.selectNodeTree([this.selectTree.id], this.tableRefName);
        this.parentTitle = '';
        if (e == '1') {
          // 添加平级节点
          this.parentNodeId = this.selectTree.parentId;
          this.showAddNodeModal = true;
        } else if (e == '2') {
          // 添加子节点
          this.parentNodeId = this.selectTree.id;
          this.parentTitle = this.selectTree.title;
          this.showAddNodeModal = true;
        } else if (e == '3') {
          // 编辑节点
          this.parentNodeId = this.selectTree.parentId;
          this.nodeId = this.selectTree.id;
          this.showEditNodeModal = true;
        } else if (e == '4') {
          // 删除节点
          if (
            this.selectTree.isLeaf &&
            (this.selectTree.children == null || this.selectTree.children.length == 0)
          ) {
            this.handleTreeDelete(this.selectTree.id);
          } else {
            this.$message.warning('当前选中节点含有子节点，请先删除子节点！');
          }
        }
      }
    },
    // 删除树节点
    handleTreeDelete() {
      let _this = this;
      this.$confirm({
        title: '您确定要删除选中的节点吗?',
        content: '',
        okText: '确定',
        cancelText: '取消',
        onOk() {
          if (_this.selectTree.id) {
            _this.treeLoading = true;
            httpAction(
              _this.treeDeleteParam.url,
              _this.selectTree.id,
              _this.treeDeleteParam.method,
              false,
              true,
              false
            )
              .then(res => {
                if (res.success) {
                  if (_this.selectTree.id) {
                    let deleteNode = getTreeById(
                      _this.$refs[_this.treeRefName].treeNodeData,
                      _this.selectTree.id
                    );
                    if (deleteNode && deleteNode.parentId) {
                      _this.selectedKeys = [deleteNode.parentId];
                    } else {
                      _this.selectedKeys = [_this.defaultParentId];
                    }
                    _this.reloadTreeDataByNode(_this.selectedKeys[0], _this.treeRefName);
                    _this.$refs[_this.tableRefName].loadData(_this.queryParam);
                  }
                  _this.$message.success('删除节点成功！');
                }
                _this.treeLoading = false;
              })
              .catch(response => {
                _this.treeLoading = false;
              });
          }
        },
        onCancel() {}
      });
    },
    // 树列表加载数据事件
    afterLoadData(treeData) {
      if (treeData) {
        // 判断是否显示初始化节点
        if (
          this.loadTreeParentId == this.defaultParentId &&
          treeData.success &&
          (!treeData.result || treeData.result.length == 0) &&
          this.$refs[this.treeRefName].treeNodeData.length == 0
        ) {
          this.initTreeDataVisible = true;
        }
        //设置默认选中
        if (this.loadTreeParentId == this.defaultParentId) {
          if (treeData.result && treeData.result.length > 0) {
            this.selectedKeys = [treeData.result[0].id];
            this.$refs[this.tableRefName].loadData(this.queryParam);
          }
        }
        this.treeLoading = false;
      } else {
        this.treeParam.url = this.requestTreeUrl + this.defaultLevel + '/' + this.defaultParentId;
        this.$refs[this.treeRefName].refresh();
      }
    },
    // 加载初始化节点完成
    confirmInitTreeData() {
      this.initTreeDataVisible = false;
      this.buttonPermissions = true;
      this.$refs[this.treeRefName].refresh();
    },
    // 关闭初始化加载节点框
    closeInitTreeData() {
      this.initTreeDataVisible = false;
      this.buttonPermissions = false;
    },
    // 树搜索方法
    searchTree(e) {
      if (!e || !e.trim().length) {
        this.treeParam.url = this.requestTreeUrl + this.defaultLevel + '/' + this.defaultParentId;
      } else {
        this.searchTreeParam.parameter = { searchText: e.trim() };
      }
    },
    // 加载树节点之前
    beforeLoadData(treeData, parentId) {
      this.loadTreeParentId = parentId;
      this.treeParam.url = this.requestTreeUrl + this.openLevel + '/' + parentId;
    },
    /**
     * @description 树面板刷新-若已选节点，则加载至该节点，否则加载默认
     **/
    reloadTree() {
      this.treeLoading = true;
      // if (this.selectedKeys.length) {
      //   this.reloadTreeDataByNode(this.selectedKeys[0], this.treeRefName);
      // } else {
      this.reloadTreeData(this.defaultParentId, this.treeRefName);
      // }
    },
    afterSearchFinished(res) {
      if (!res || !res.length) {
        this.$message.warning('没有搜索到相关结果！');
      } else {
        //设置默认选中
        if (res.length > 0) {
          this.selectedKeys = [res[0].id];
          this.$refs[this.tableRefName].loadData(this.queryParam);
        }
      }
    },
    // 重新加载刷新树
    reloadTreeData(parentId, refName) {
      if (parentId) {
        this.treeParam.url =
          this.requestTreeUrl +
          (this.defaultParentId == parentId ? this.defaultLevel : this.openLevel) +
          '/' +
          parentId;
        this.$refs[refName].refresh(parentId);
      }
    },
    // 重新加载刷新树, 默认选择指定节点(新增节点、已编辑节点)
    reloadTreeDataByNode(nodeId, refName) {
      if (nodeId && this.defaultParentId != nodeId) {
        this.treeParam.url = this.expandTreeUrl + nodeId;
        this.$refs[refName].loadTreeToNode(nodeId);
        this.selectedKeys = [nodeId];
      } else {
        this.treeParam.url = this.requestTreeUrl + this.defaultLevel + '/' + this.defaultParentId;
        this.$refs[this.treeRefName].refresh();
      }
    },
    // 选中树节点加载表格数据
    selectNodeTree(e, refName) {
      this.selectedRowKeys = [];
      this.selectedRows = [];
      this.$refs[refName].changeSelectedRowKeys([]);
      if (e && e.length > 0) {
        this.selectedKeys = e;
        this.page = 1;
        // 进入页面,加载表格数据
        this.$refs[refName].loadData(this.queryParam);
      }
    },
    // 树右键刷新
    treeMenuRefreshReset() {
      this.$refs[this.treeRefName].emptySearch();
      this.$refs[this.treeRefName].empty();
      this.treeParam.url = this.requestTreeUrl + this.defaultLevel + '/' + this.defaultParentId;
      this.$refs[this.treeRefName].refresh();
    },
    // 初始化树高度
    initTreeHeight(params) {
      this.treeStyle = {
        height: getContentHeight(params)
      };
    },
    /**
     * 点击添加按钮,调用添加弹窗方法
     */
    handleAdd(params) {
      this.showAddModal = true;
      this.treeNodeId = params.param.selectTreeId;
    }
  }
};
