/* 主题组件的公共混入文件，定义消息、菜单、收藏菜单等主题通用功能 */
import Vue from 'vue';
import '@assets/styles/common/base-theme.less';
import AvicAsyncComponent from '@comp/avic/pt/common/AvicAsyncComponent/index';
import ChangeMultiOrg from '@views/avic/pt/system/personalSetting/ChangeMultiOrg';
import SettingDrawer from '@portal/pageModules/drawer/SettingDrawer';
import NewsAdd from '@views/avic/pt/system/messages/NewsAdd';
import NewsDetail from '@views/avic/pt/system/messages/NewsDetail';
import VueContextMenu from 'vue-contextmenu';
import { mapActions, mapGetters } from 'vuex';
import { httpAction } from '@api/manage';
import { triggerWindowResizeEvent } from '@/utils/avic/common/util';
import {
  USER_INFO,
  ACCESS_KEY_ID,
  ACCESS_KEY_SECRET,
  ACCESS_TOKEN,
  MICRO_SERVICE_TOKEN
} from '@/store/mutationTypes';
import notification from 'ant-design-vue/es/notification';
import config from '@config/defaultSettings';
import AvicIcon from '@comp/avic/pt/common/AvicIcon';
Vue.use(VueContextMenu);

import { slugify } from 'transliteration';

let requestPublicUrl = '/api/appsys/message/MessageRest';
let getUnreadCount = requestPublicUrl + '/searchUnreadCount/v1'; //获取未读消息数量地址
let getUnreadData = requestPublicUrl + '/searchTopUnread/v1'; //获取未读消息数据地址
export const themeLayoutMixin = {
  components: {
    ChangeMultiOrg,
    SettingDrawer, //设置
    NewsAdd,
    NewsDetail,
    AvicAsyncComponent,
    AvicIcon
  },
  data() {
    return {
      isSettingDrawer: false, //是否展开主题设置
      systemTitle: config.systemTitle,
      collapsed: false, //导航栏展开状态
      keyWord: '',
      /* 未读消息数量 */
      count: 0,
      /* 是否全屏模式 */
      isFullScreenMode: false,
      /* 视图切换时的动画模式 */
      transitionName: 'open',
      loadingMessage: false,
      /* 请求地址中的公共部分 */
      requestPublicUrl,
      /* 显示消息发送窗 */
      showAddModal: false,
      /* 显示消息详情窗 */
      showDetailModal: false,
      /* 选择的消息Id */
      messageId: '',
      /* 添加消息弹窗ref名称 */
      addModalRefName: 'addModal',
      /* 添加消息弹窗ref名称 */
      detailModalRefName: 'detailModal',
      /* 个人设置弹窗ref名称 */
      personalSettingRef: 'personalSetting',
      showPersonalSetting: false,
      personalSettingPath: 'views/avic/pt/system/personalSetting/index',
      /* 修改密码 */
      changePwdRef: 'changePwdRef',
      showChangePwd: false,
      changePwdPath: 'views/avic/pt/system/personalSetting/ChangePwd',
      /* 切换组织组件的ref名称 */
      orgModalRefName: 'multiOrgModal',
      /* 消息列表 */
      newsList: [],
      /* 是否显示消息浮层 */
      showMesagePanel: false,
      /* 是否显示用户信息浮层 */
      showUserPanel: false,
      /* 用户头像 */
      userHead: '',
      isCloseView: false,
      /* 标签页中激活的视图索引值 */
      selected: 0,
      lastSelect: 0,
      /* 标签页中显示的视图列表 */
      pageList: [],
      /* 调用右键菜单的当前标签页索引,默认-1表示未调用 */
      optMenuIdx: -1,
      /* 右键菜单数据 */
      contextMenuData: {
        menuName: 'demo',
        // 菜单显示的位置
        axis: {
          x: null,
          y: null
        },
        // 菜单选项
        menulists: [
          {
            fnHandler: 'close2left', // Binding events(绑定事件)
            icoName: 'arrow-left', // icon (icon图标 )
            btnName: '关闭左边' // The name of the menu option (菜单名称)
          },
          {
            fnHandler: 'close2right',
            icoName: 'arrow-right',
            btnName: '关闭右边'
          },
          {
            fnHandler: 'closeAllOthers',
            icoName: 'close',
            btnName: '关闭其他'
          },
          {
            fnHandler: 'refresh',
            icoName: '',
            btnName: '刷新'
          }
        ]
      },
      /* 网络请求参数 */
      httpConfig: {
        queryCollectedMenuByUser: {
          url: '/api/appsys/user/menu/personalMenu/getMenuList/v1',
          method: 'post'
        },
        saveCollectedMenu: {
          url: '/api/appsys/user/menu/personalMenu/saveMenu/v1',
          method: 'post'
        },
        deleteCollectedMenu: {
          url: '/api/appsys/user/menu/personalMenu/deleteByMenuId/v1',
          method: 'post'
        },
        sortCollectMenu: {
          url: '/api/appsys/user/menu/personalMenu/orderMenu/v1',
          method: 'post'
        },
        getRecentVisitList: {
          url: '/api/appsys/user/menu/visitMenu/getMenuList/v1',
          method: 'post'
        },
        getOrgInfoParam: {
          // 读取当前用户的当前组织接口
          url: '/api/appsys/user/multiorg/current',
          method: 'get'
        }
      },
      /* 当前登录用户的已收藏菜单列表 */
      favMenuList: [],
      /* 当前权限过滤后的已收藏菜单列表 */
      siderList: [],
      /* 当前权限过滤后的最近访问记录列表 */
      recentVisitList: [],
      /* 最近访问记录显示的最大数量 */
      recentVisitCount: 6,
      /* 用户当前组织信息 */
      currentOrgName: '',
      /* 用户当前部门信息 */
      currentDeptName: '',
      /* 菜单选中项 */
      selectMenuKeys: []
    };
  },
  watch: {
    selected(nVal, oVal) {
      this.lastSelect = oVal;
    },
    pageList: {
      deep: true,
      handler(nVal, oVal) {
        let res = [];
        this.pageList.map(item => {
          res = res.concat(item.meta.compName);
        });
        this.$store.commit('SET_KEEPALIVE_COMP_LIST', res);
      }
    },
    $route(newRoute) {
      let isExistIndex = -1;
      this.pageList.map((item, index) => {
        if (item.name === newRoute.name) {
          isExistIndex = index;
        }
      });
      if (isExistIndex !== -1) {
        this.selected = isExistIndex;
        if (this.isCloseView) {
          this.transitionName = 'close';
          this.isCloseView = false;
        } else {
          this.transitionName = 'review';
        }
        this.pageList.splice(isExistIndex, 1, this.routeObj2PageObj(newRoute));
      } else {
        this.pageList.push(this.routeObj2PageObj(newRoute));
        this.selected = this.pageList.length - 1;
        this.transitionName = 'open';
      }
    },
    getUserUnreadCount(count) {
      this.count = count;
    },
    getMessageRequestInternal(val) {
      window.clearInterval(window._CONFIG['unreadmsg']);
      window._CONFIG['unreadmsg'] = window.setInterval(() => {
        setTimeout(this.getUnreadCount(), 0);
      }, val);
    }
  },
  computed: {
    ...mapGetters([
      'getOpenedViewList',
      'getKeepAliveCompList',
      'permissionList',
      'getUserUnreadCount',
      'getMessageRequestInternal',
      'getManageSystemUrl'
    ])
  },
  created() {
    if (this.$route.path !== this.homePageRoutePath) {
      const homePageRoute = {
        name: config.homePageConfig.name,
        path: this.homePageRoutePath,
        fullPath: this.homePageRoutePath,
        meta: {
          icon: config.homePageConfig.icon,
          permissionList: [],
          title: config.homePageConfig.title,
          compName: ['Dashboard']
        }
      };
      this.pageList.push(homePageRoute);
    }
    this.pageList.push(this.routeObj2PageObj(this.$route));
    this.selected = this.pageList.length - 1;

    this.getAvatar();
    this.queryCollectedMenuByUser();
    this.getUnreadCount();
    this.getOrgInfo();

    let that = this;
    window._CONFIG['unreadmsg'] = window.setInterval(() => {
      setTimeout(that.getUnreadCount(), 0);
    }, this.getMessageRequestInternal);

    this.$router.onError(e => {
      if (e.code === 'MODULE_NOT_FOUND') {
        this.$router.push('/404');
        // notification.error({
        //   message: '系统提示',
        //   description: '未找到模块资源！'
        // })
      } else {
        notification.error({
          message: '系统提示',
          description: e.message || '页面跳转时出现错误！'
        });
      }
      this.pageList.splice(this.pageList.length - 1, 1);
      this.selected = this.lastSelect;
      // NProgress.done()
    });
  },
  mounted() {
    // 阻止F11键默认事件，用HTML5全屏API代替
    window.addEventListener('keydown', this.handleKeydown);
    this.watchFullScreen();
  },
  beforeDestroy() {
    window.clearInterval(window._CONFIG['unreadmsg']);
    window.removeEventListener('keydown', this.handleKeydown);
  },
  methods: {
    ...mapActions(['Logout']),
    ...mapGetters(['nickname', 'avatar', 'userInfo']),
    handleCollapse() {
      this.collapsed = !this.collapsed;
      this.$nextTick(() => {
        setTimeout(() => {
          triggerWindowResizeEvent();
        }, 300);
      });
    },
    handleKeydown(e) {
      e = e || window.event;
      if (e.keyCode === 122) {
        e.preventDefault();
        this.handleFullScreen();
      }
    },
    handleFullScreen() {
      if (this.isFullScreenMode) {
        this.exitScreen();
      } else {
        this.fullScreen();
      }
      this.isFullScreenMode = !this.isFullScreenMode;
    },
    setFuleScreenMode() {
      let isFull =
        document.fullscreenElement ||
        document.msFullscreenElement ||
        document.mozFullScreenElement ||
        document.webkitFullscreenElement ||
        false;
      this.isFullScreenMode = !!isFull;
    },
    //监听全屏状态，全屏返回dom，否则返回false
    watchFullScreen() {
      document.addEventListener('fullscreenchange', this.setFuleScreenMode, false);
      document.addEventListener('mozfullscreenchange', this.setFuleScreenMode, false);
      document.addEventListener('webkitfullscreenchange', this.setFuleScreenMode, false);
      document.addEventListener('msfullscreenchange', this.setFuleScreenMode, false);
    },
    //全屏
    fullScreen() {
      let element = document.documentElement;
      // 判断各种浏览器，找到正确的方法
      let requestMethod =
        element.requestFullScreen || //W3C
        element.webkitRequestFullScreen || //FireFox
        element.mozRequestFullScreen || //Chrome等
        element.msRequestFullScreen; //IE11
      if (requestMethod) {
        requestMethod.call(element);
      } else if (typeof window.ActiveXObject !== 'undefined') {
        //for Internet Explorer
        let wscript = new ActiveXObject('WScript.Shell');
        if (wscript !== null) {
          wscript.SendKeys('{F11}');
        }
      }
    },
    //退出全屏
    exitScreen() {
      // 判断各种浏览器，找到正确的方法
      let exitMethod =
        document.exitFullscreen || //W3C
        document.mozCancelFullScreen || //FireFox
        document.webkitExitFullscreen || //Chrome等
        document.webkitExitFullscreen; //IE11
      if (exitMethod) {
        exitMethod.call(document);
      } else if (typeof window.ActiveXObject !== 'undefined') {
        //for Internet Explorer
        let wscript = new ActiveXObject('WScript.Shell');
        if (wscript !== null) {
          wscript.SendKeys('{F11}');
        }
      }
    },
    handleMenuSearch() {
      this.collapsed = false;
      this.$nextTick(() => {
        setTimeout(() => {
          triggerWindowResizeEvent();
        }, 300);
      });
    },
    /* 菜单项点击事件 */
    clickMenu(menu) {
      this.openMenuTab(menu);
      this.$router.push(menu.path);
    },
    rolls() {
      //回到顶部
      return document.getElementById('roll');
    },
    toHomePage() {
      this.$router.push(this.homePageRoutePath);
    },
    /* 搜索 根据关键字过滤菜单项,返回boolean */
    /* @param: item,过滤的菜单项 */
    /* @param: onlySelf,是否仅过滤item，不过滤item的后代菜单，默认 false */
    keyWordFilter(item, onlySelf = false) {
      let keyWord = this.keyWord.trim();
      if (keyWord === '') {
        return true;
      }
      // 直接包含关键字
      if (item && item.meta.title.indexOf(keyWord) !== -1) {
        return true;
      }
      // 拼音直接包含关键字
      if (item) {
        let itemFlstr = '';
        let pinyinList = slugify(item.meta.title).split('-');
        pinyinList.map(fl => {
          itemFlstr += fl.substring(0, 1);
        });
        if (itemFlstr.toLowerCase().indexOf(keyWord.toLowerCase()) !== -1) {
          return true;
        }
      }
      if (!onlySelf) {
        // 当前菜单的子菜单项包含关键字或其拼音包含关键字
        if (item.children && item.children.length > 0) {
          let res = false;
          let _kw = keyWord;
          item.children.map(it => {
            if (it.meta.title.indexOf(_kw) !== -1) {
              res = true;
            } else {
              let _itflStr = '';
              let itPinyinList = slugify(it.meta.title).split('-');
              itPinyinList.map(fl => {
                _itflStr += fl.substring(0, 1);
              });
              if (_itflStr.toLowerCase().indexOf(_kw.toLowerCase()) !== -1) {
                res = true;
              } else {
                if (it.children && it.children.length > 0) {
                  it.children.map(leaf => {
                    res = res || this.keyWordFilter(leaf, onlySelf);
                  });
                }
              }
            }
          });
          if (res) {
            return true;
          }
        }
      }
      return false;
    },
    //检验是否已收藏指定菜单，返回boolean
    checkCollect(menuData) {
      let res = false;
      this.siderList.map((item, index) => {
        if (item.path === menuData.path) {
          res = true;
        }
      });
      return res;
    },
    /* 获取用户当前组织机构信息 */
    getOrgInfo() {
      this.currentDeptName = '';
      this.currentOrgName = '';
      httpAction(
        this.httpConfig.getOrgInfoParam.url,
        null,
        this.httpConfig.getOrgInfoParam.method
      ).then(res => {
        if (res.success) {
          this.currentDeptName = res.result.currentDeptName;
          this.currentOrgName = res.result.currentOrgName;
        }
      });
    },
    /* 获取最近访问记录 */
    getRecentVisitList() {
      httpAction(
        this.httpConfig.getRecentVisitList.url,
        this.recentVisitCount,
        this.httpConfig.getRecentVisitList.method
      ).then(res => {
        if (res.result && res.result.length !== 0) {
          this.recentVisitList = this.filterVisitListByAuth(res.result);
        }
      });
    },
    /* 根据用户权限过滤最近访问菜单 */
    filterVisitListByAuth(dataList) {
      let _this = this;
      return dataList.filter(item => {
        return _this.checkMenuAuth(item.id, _this.permissionList);
      });
    },
    /* 根据用户权限过滤当前当前用户已收藏的菜单 */
    filterFavAuth() {
      let _this = this;
      this.siderList = this.favMenuList.filter(item => {
        return _this.checkMenuAuth(item.id, _this.permissionList);
      });
    },
    /* 校验目标菜单树中书否包含指定Id的菜单 */
    checkMenuAuth(authId, authList) {
      let res = false;
      let _this = this;
      authList.map(item => {
        if (!res) {
          if (item.id === authId) {
            res = true;
          } else {
            if (item.children && item.children.length) {
              res = _this.checkMenuAuth(authId, item.children);
            }
          }
        }
      });
      return res;
    },
    /* 请求接口获取当前登录用户已收藏的菜单 */
    queryCollectedMenuByUser() {
      httpAction(this.httpConfig.queryCollectedMenuByUser.url).then(res => {
        if (!res.result || res.result.length === 0) {
          // 最好不要写这样的判断
        } else {
          this.favMenuList = res.result;
          this.filterFavAuth();
        }
      });
    },
    // 浮动菜单中切换收藏状态
    changeCollectStatus(cdata) {
      let idx = -1;
      this.siderList.map((e, index) => {
        if (e.id === cdata.id) {
          idx = index;
        }
      });
      if (idx === -1) {
        this.pushSiderList(cdata);
      } else {
        this.deleteSider(idx);
      }
    },
    //添加收藏菜单
    pushSiderList(cdata) {
      if (cdata && JSON.stringify(this.siderList).indexOf(JSON.stringify(cdata)) === -1) {
        httpAction(
          this.httpConfig.saveCollectedMenu.url,
          cdata.id,
          this.httpConfig.saveCollectedMenu.method
        ).then(res => {
          if (res.success) {
            this.siderList.push(cdata);
          }
        });
      }
    },
    //删除边框栏中的所选数据
    deleteSider(index) {
      if (!this.siderList[index] || !this.siderList[index].id) {
        console.log('菜单数据无ID，无法取消收藏');
      } else {
        httpAction(
          this.httpConfig.deleteCollectedMenu.url,
          this.siderList[index].id,
          this.httpConfig.deleteCollectedMenu.method
        ).then(res => {
          if (res.success) {
            this.siderList.splice(index, 1);
          }
        });
      }
    },
    /* 获取头像地址 */
    getTokenAvatar(url) {
      let xmlhttp;
      let that = this;
      xmlhttp = new XMLHttpRequest();
      xmlhttp.open('GET', url, true);
      xmlhttp.responseType = 'arraybuffer';
      xmlhttp.setRequestHeader('X-Access-Token', Vue.ls.get(ACCESS_TOKEN)); //该处加上你当前的token
      if (Vue.ls.get(MICRO_SERVICE_TOKEN)) {
        xmlhttp.setRequestHeader('x-kong-api-key', 'bearer ' + Vue.ls.get(MICRO_SERVICE_TOKEN));
      }
      xmlhttp.onload = function () {
        let xml = this;
        if (xml.status == 200) {
          that.userHead =
            'data:image/png;base64,' +
            btoa(
              new Uint8Array(xml.response).reduce(
                (data, byte) => data + String.fromCharCode(byte),
                ''
              )
            );
        } else {
          that.userHead = '/assets/images/common/default-avatar.png';
        }
      };
      xmlhttp.send();
    },
    getAvatar() {
      let that = this;
      if (that.avatar()) {
        return that.getTokenAvatar(window._CONFIG['domainURL'] + that.avatar());
      } else {
        that.userHead = '/assets/images/common/default-avatar.png';
      }
    },
    /* 点击菜单，渲染菜单页签 */
    openMenuTab(menu) {
      let isExistIndex = -1;
      this.pageList.map((item, index) => {
        if (item.path === menu.path) {
          isExistIndex = index;
        }
      });
      if (isExistIndex !== -1) {
        this.selected = isExistIndex;
        if (this.isCloseView) {
          this.transitionName = 'close';
          this.isCloseView = false;
        } else {
          this.transitionName = 'review';
        }
        this.pageList[isExistIndex] = menu;
      } else {
        this.pageList.push(menu);
        this.selected = this.pageList.length - 1;
        this.transitionName = 'open';
      }
    },
    /* 将指定路由对象转换为页签选项卡对象 */
    routeObj2PageObj(obj) {
      let _comps = [];
      obj.matched.map(item => {
        if (item.components.default && item.components.default.name) {
          _comps.push(item.components.default.name);
        }
      });
      return {
        id: obj.meta.id || 'MENU_ID_UNDEFINED',
        name: obj.name,
        path: obj.path,
        fullPath: obj.fullPath,
        meta: {
          icon: obj.meta.icon,
          permissionList: obj.meta.permissionList,
          title: obj.meta.title,
          compName: _comps, // 路由匹配的所有组件名称
          routeObj: obj
        }
      };
    },
    //删除当前页面
    deletethis(key) {
      // 关闭当前标签页时，路由定向到后一页或者前一页
      if (key === this.selected) {
        let toPath = null;
        let selected = 0;
        if (key < this.pageList.length - 1) {
          toPath = this.pageList[key + 1].fullPath;
          selected = key;
        } else {
          toPath = this.pageList[key - 1].fullPath;
          selected = key - 1;
        }

        this.pageList.splice(key, 1);
        this.selected = selected;
        this.$router.push(toPath);
        this.isCloseView = true;
      } else {
        this.pageList.splice(key, 1);
        this.$nextTick(() => {
          this.selected = key < this.selected ? this.selected - 1 : this.selected;
        });
      }
    },
    select(key) {
      this.selected = key;
      // this.selectMenuKeys = [this.pageList[key].id];
    },
    /* 切换模块页签选项卡 */
    handleChange(activeKey) {
      this.select(activeKey);
      this.$nextTick(() => {
        this.$router.push(this.pageList[activeKey].path);
      });
    },
    /* 关闭targetKey对应的页签视图 */
    handleEdit(targetKey, action) {
      if (action === 'remove' && targetKey !== 0) {
        this.deletethis(targetKey);
      }
    },
    /* 按照指定位置展示右键菜单 */
    showContextMenu(e, index) {
      this.optMenuIdx = index;
      let x = e.clientX;
      let y = e.clientY;
      // Get the current location
      this.contextMenuData.axis = {
        x,
        y
      };
    },
    // 关闭左边
    close2left() {
      if (this.selected === 0) {
        return;
      }
      let closedNum = this.pageList.splice(1, this.optMenuIdx - 1).length;
      if (this.selected < this.optMenuIdx) {
        this.selected = 1;
        this.$router.push(this.pageList[1].fullPath);
      } else {
        this.selected = this.selected - closedNum;
      }
    },
    // 关闭右边
    close2right() {
      this.pageList.splice(this.optMenuIdx + 1, this.pageList.length - this.optMenuIdx - 1);
      if (this.selected > this.optMenuIdx) {
        this.selected = this.optMenuIdx;
        this.$router.push(this.pageList[this.optMenuIdx].fullPath);
      }
    },
    // 关闭其他, 不关闭首页
    closeAllOthers() {
      this.pageList.splice(this.optMenuIdx + 1, this.pageList.length - this.optMenuIdx - 1);
      this.pageList.splice(1, this.optMenuIdx - 1);
      this.selected = this.optMenuIdx === 0 ? 0 : 1;
      this.$router.push(this.pageList[this.selected].fullPath);
    },

    // 单标签页刷新事件
    refresh() {
      this.pageList.map((item, idx) => {
        if (
          this.optMenuIdx === idx &&
          item.meta.routeObj &&
          item.meta.routeObj.matched &&
          item.meta.routeObj.matched.length > 0
        ) {
          this.$store.commit(
            'SET_REFRESH_MENU_NAME',
            item.meta.routeObj.matched[item.meta.routeObj.matched.length - 1].components.default
              .name
          );
        }
      });
    },
    /* 用户注销 */
    handleLogout() {
      this.showUserPanel = false;
      const that = this;
      this.$confirm({
        title: '提示',
        content: '需要注销登录吗 ?',
        closable: true,
        onOk() {
          return that
            .Logout(true)
            .then(() => {
              window.location.href = '/';
              //window.location.reload()
            })
            .catch(err => {
              that.$message.error({
                title: '错误',
                description: err.message
              });
            });
        },
        onCancel() {}
      });
    },
    /* 个人设置 */
    handlePersonalSetting() {
      // this.showUserPanel = false;
      // this.showPersonalSetting = true;
      this.$router.push('/usersetting');
    },
    handleClosePersonalSetting() {
      this.showPersonalSetting = false;
    },
    handleChangeUserPanel(visible) {
      this.showUserPanel = visible;
    },
    /* 点击修改密码 */
    handleChangePwd() {
      this.showUserPanel = false;
      this.showChangePwd = true;
    },
    /* 关闭修改密码 */
    handleCloseChangePwd() {
      this.showChangePwd = false;
    },
    /* 切换组织 */
    handleMulti() {
      this.showUserPanel = false;
      this.$refs[this.orgModalRefName].openModal();
    },
    /* 切换后台 */
    handleManageSystem() {
      let url =
        this.getManageSystemUrl +
        '?' +
        ACCESS_KEY_ID +
        '=' +
        Vue.ls.get(USER_INFO)[ACCESS_KEY_ID] +
        '&' +
        ACCESS_KEY_SECRET +
        '=' +
        Vue.ls.get(USER_INFO)[ACCESS_KEY_SECRET];
      window.open(url, 'manageSystem');
    },
    handleChangeMessagePanel(visible) {
      this.showMesagePanel = visible;
      if (visible) {
        this.fetchNotice();
      }
    },
    handleMoreMessage() {
      this.showMesagePanel = false;
      this.$router.push('/newsManage');
    },
    //获取未读消息数量
    getUnreadCount() {
      let that = this;
      httpAction(getUnreadCount, '', 'get', true, true, true, true, '', 4, [], true)
        .then(res => {
          if (res.success) {
            that.count = res.result;
          }
          //授权过期结束轮询
          if (res.code == 401) {
            window.clearInterval(window._CONFIG['unreadmsg']);
          }
        })
        .catch(() => {
          window.clearInterval(window._CONFIG['unreadmsg']);
        });
    },
    fetchNotice() {
      this.newsList = [];
      if (this.loadingMessage) {
        this.loadingMessage = false;
        return;
      }
      this.loadingMessage = true;
      this.loadMessageByCount('6');
    },
    //加载未读的消息数据
    loadMessageByCount(params) {
      // 获取数据，将返回的data传回父组件,有回调函数执行回调函数
      let that = this;
      httpAction(getUnreadData, params, 'POST')
        .then(res => {
          if (res.success) {
            that.newsList = res.result;
            that.loadingMessage = false;
          } else {
            that.loadingMessage = false;
          }
        })
        .catch(e => {
          console.error(e);
          that.loadingMessage = false;
        });
    },
    /**
     * 点击添加按钮,调用添加弹窗方法
     * @param {Object} params 形式: {refName: refName(添加弹窗refName), param: {(其他数据,待定)}}
     */
    handleAdd(params) {
      this.showMesagePanel = false;
      this.showAddModal = true;
    },
    //展示详情页
    handleDetailOrEdit(params) {
      this.showMesagePanel = false;
      this.showDetailModal = true;
      this.messageId = params.param.id;
    },
    onSearch(value) {
      //搜索框
      console.log(value);
    },
    //点击skin按钮展开主题设置
    handleSettingDrawer() {
      this.isSettingDrawer = !this.isSettingDrawer;
    }
  }
};
