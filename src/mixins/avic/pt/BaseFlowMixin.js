import { Modal } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
export const baseFlowMixin = {
  components: {
    AvicBpmFormCode: () => import('@comp/avic/bpm/AvicBpmFormCode'),
    AvicBpmListSelect: () => import('@comp/avic/bpm/AvicBpmListSelect'),
    AvicBpmDetailList: () => import('@/components/avic/bpm/AvicBpmDetailList')
  },
  data() {
    return {
      showTip: true,
      openAddFlowLoading: false, //添加流程
      openEditFlowLoading: false, //打开流程加载
      openEditFlowId: '', // 点击打开当前的数据id
      openEditFlowDetailDataIndex: '',
      bpmState: '', //流程状态
      flowFormCodeLoad: false, //标志flowFormCode显示或隐藏，异步加载组件控制渲染参数
      flowFormDriveAddLoad: false //标志流程表单驱动的显示或隐藏，异步加载组件控制渲染参数
    };
  },
  methods: {
    /**
     * 启动流程,掉用FlowFormCode组件获取可启动的流程
     * @param {} params
     */
    handleAdd(params) {
      //判断流程驱动类型
      this.openAddFlowLoading = true;
      if (this.flowFormCodeLoad) {
        if (
          this.$refs[this.getFormCodeRefName] &&
          typeof this.$refs[this.getFormCodeRefName].loadFormcode === 'function'
        ) {
          //标志FlowFormCode组件已经加载完毕
          this.$refs[this.getFormCodeRefName].loadFormcode(params);
        }
      } else {
        this.flowFormCodeLoad = true; //渲染flowFormCode组件
        this.handleAdd(params);
      }
    },
    /**
     * 表单驱动,打开表单添加页面
     */
    handleAddFormdrive() {
      this.flowFormDriveAddLoad = true;
    },
    /**
     * 打开流程详情页面
     * @param {*} res
     */
    handleFlowDetail(res) {
      this.showTip = false;
      if (res.param && res.param.dbid) {
        let _this = this;
        this.openEditFlowLoading = true;
        this.openEditFlowId = res.param.id;
        this.openEditFlowDetailDataIndex = res.param.detailDataIndex;
        bpmUtils
          .detail(res.param.dbid)
          .then(res => {
            //打开成功
            _this.openEditFlowLoading = false;
            _this.openEditFlowId = '';
            _this.openEditFlowDetailDataIndex = '';
          })
          .catch(() => {
            //打开失败
            _this.openEditFlowLoading = false;
            _this.openEditFlowId = '';
            _this.openEditFlowDetailDataIndex = '';
          });
      }
    },
    //流程状态筛选
    changeFlowState(e, params) {
      this.bpmState = e;
      // 清空选中项
      this.selectedRowKeys = [];
      this.selectedRows = [];
      this.$refs[params.refName].changeSelectedRowKeys([]);
      // 进入页面,加载表格数据
      this.$refs[params.refName].loadData(this.queryParam);
    },
    /**
     * 点击编辑按钮,调用弹窗方法（异步）
     * @param {Object} params 形式: {param: {checkField: checkField(校验字段),checkFieldValue: checkFieldValue(校验字段值)}}
     */
    handleEdit(params) {
      if (this.selectedRowKeys.length !== 1) {
        this.$message.warning('请选择且只选择一条数据！');
        return;
      }
      if (this.selectedRows[0][params.param.checkField] !== params.param.checkFieldValue) {
        this.$message.warning('只有拟稿中的数据才可以编辑！');
        return;
      }
      this.paramId = this.selectedRowKeys[0];
      this.showEditModal = true;
    },
    /**
     * 单表流程删除处理
     * @param {Object} params 形式: {refName: refName(表格refName),ids: ids(删除项id集合数组),isNeedAsk: isNeedAsk(是否需要提示)}
     */
    handleDelete(params) {
      if (!params) return;
      if (this.selectedRowKeys.length == 0) {
        this.$message.warning('请选择要删除的数据！');
        return;
      }
      let flag = true;
      for (let i = 0; i < this.selectedRows.length; i++) {
        if (this.selectedRows[i][params.param.checkField] !== params.param.checkFieldValue) {
          flag = false;
          break;
        }
      }
      if (!flag) {
        this.$message.warning('只有拟稿中的数据才可以删除！');
        return;
      }
      if (params.isNeedAsk) {
        Modal.confirm({
          // 使用普通的按钮需要提示
          title: '确定删除吗？',
          onOk: () => {
            this.delLoading = true;
            httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
              .then(res => {
                if (res.success) {
                  this.$message.success('删除成功！'); // 提示成功
                  this.reloadData(false, params.refName); // 刷新表格
                }
                this.delLoading = false;
              })
              .catch(() => {
                this.delLoading = false;
              });
          }
        });
      } else {
        // 使用封装的删除按钮不必提示
        this.delLoading = true;
        httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
          .then(res => {
            if (res.success) {
              this.$message.success('删除成功！'); // 提示成功
              this.reloadData(false, params.refName); // 刷新表格
            }
            this.delLoading = false;
          })
          .catch(() => {
            this.delLoading = false;
          });
      }
    },
    //主子表流程--主表删除
    mainHandleDelete(params) {
      if (!params) return;
      if (this.selectedRowKeys.length == 0) {
        this.$message.warning('请选择要删除的数据！');
        return;
      }
      let flag = true;
      for (let i = 0; i < this.selectedRows.length; i++) {
        if (this.selectedRows[i][params.param.checkField] !== params.param.checkFieldValue) {
          flag = false;
          break;
        }
      }
      if (!flag) {
        this.$message.warning('只有拟稿中的数据才可以删除！');
        return;
      }
      if (params.isNeedAsk) {
        // 点击删除按钮
        Modal.confirm({
          title: '确定删除吗？',
          onOk: () => {
            this.delLoading = true;
            httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
              .then(res => {
                if (res.success) {
                  // 提示成功
                  this.$message.success('删除成功！');
                  this.reloadMainData(false, params.refName, params.param.type);
                }
                this.delLoading = false;
              })
              .catch(() => {
                this.delLoading = false;
              });
          }
        });
      } else {
        // 使用封装的删除按钮不必提示
        this.delLoading = true;
        httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
          .then(res => {
            if (res.success) {
              this.$message.success('删除成功！'); // 提示成功
              this.reloadMainData(false, params.refName); // 刷新表格
            }
            this.delLoading = false;
          })
          .catch(() => {
            this.delLoading = false;
          });
      }
    }
  },
  computed: {
    isShowDelete() {
      // 选中一个或多个可删除
      return this.selectedRowKeys.length > 0 && this.bpmState == 'start';
    },
    isShowEdit() {
      // 选中一个可编辑
      return this.selectedRowKeys.length === 1 && this.bpmState == 'start';
    }
  }
};
