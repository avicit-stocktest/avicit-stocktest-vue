export const baseFourTableMixin = {
  data() {
    return {
      selectedRowKeys: [],
      sidx: '', // 排序字段
      sord: '', // 排序方式: desc降序 asc升序
      dataRecord: [] // 记录每次刷新表格的数据
    };
  },
  methods: {
    getData(obj) {
      // 深拷贝data,记录编辑前表格数据(单表列表)
      this.dataRecord = JSON.parse(JSON.stringify(obj.data));
      // 接收table组件的数据,渲染表格
      this.data = obj.data;
    },
    handleRowSelection(obj) {
      // 接收表格选中信息
      if (obj.selectedRowKeys) {
        this.selectedRowKeys = obj.selectedRowKeys;
      }
    },
    handleTableChange(refName, obj) {
      // 接收分页、排序、筛选信息
      let pagination = obj.pagination; // 分页信息
      let sorter = obj.sorter; // 排序信息
      // let filters = obj.filters; // 筛选信息
      // 处理分页信息
      this.page = pagination.current; // 页数
      this.rows = pagination.pageSize; // 每页条数
      // 处理排序信息
      if (sorter) {
        this.sidx = sorter.field || ''; // 排序字段
        if (sorter.order) {
          this.sord = sorter.order == 'ascend' ? 'asc' : 'desc'; // 排序方式: desc降序 asc升序
        }
      }
      // 处理筛选信息
      // if (filters) {
      // }
      // 请求表格数据
      this.$refs[refName].loadData({
        url: this.queryParam.url,
        parameter: this.queryParam.parameter,
        method: this.queryParam.method
      });
    },
    handleAdd(refName) {
      // 点击添加按钮,调用添加弹窗方法
      this.$refs[refName].handleAdd();
    },
    handleDetail(refName, id) {
      // 点击第一列跳详情,调用详情弹窗方法
      this.$refs[refName].handleDetail(id);
    },
    handleEdit(refName, id) {
      // 点击编辑按钮,调用编辑弹窗方法
      this.$refs[refName].handleEdit(id);
    },
    handleImport() {
      // 点击导入按钮
      console.log('handleImport');
    },
    handleExport() {
      // 点击导出按钮
      console.log('handleExport');
    },
    reloadData(current, refName) {
      // 刷新表格 current: 是否定位到第一页; refName: 绑定表格的ref名称.
      let that = this;
      if (current) {
        this.page = 1;
      }
      this.$refs[refName].loadData(
        {
          url: this.queryParam.url,
          parameter: this.queryParam.parameter,
          method: this.queryParam.method
        },
        () => {
          // 刷新后,去掉选中项
          that.selectedRowKeys = [];
        }
      );
    },
    searchByKeyWord(refName) {
      // 普通搜索,根据搜索内容刷新表格,从第一页开始
      this.reloadData(true, refName);
    },
    handleSearch(refName) {
      // 点击高级查询按钮, 打开或者关闭高级查询弹窗
      this.$refs[refName].handleOnOff();
    },
    advanceSearch(refName, obj) {
      // 接收高级查询数据,isDeal为true,则刷新表格,且从第一页开始
      this.advanceSearchParam = obj.advanceSearchParam;
      if (obj.isDeal) {
        this.reloadData(true, refName);
      }
    }
  },
  computed: {
    getSelectedRowKeys() {
      // selectedRowKeys双向绑定,传回组件
      return this.selectedRowKeys;
    },
    isShowDelete() {
      // 选中一个或多个可删除
      return this.selectedRowKeys.length > 0;
    },
    isShowEdit() {
      // 选中一个可编辑
      return this.selectedRowKeys.length == 1;
    }
  }
};
