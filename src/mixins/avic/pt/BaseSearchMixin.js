import { loadLookUp } from '@utils/avic/http/loadLookUp';
import moment from 'moment';
// 高级搜索组件 AvicAdvancedSearchCollapse导入
import { AvicAdvancedSearchCollapse } from '@comp/avic/pt/common/AvicAdvancedSearchCollapse';
import { formatDate, formatNumber, formatDisabledDate } from '@utils/avic/common/format';
import { validateCheckboxMaxlength, validateInputMaxlength } from '@utils/avic/common/validate';
import { mapGetters } from 'vuex';
import { httpAction } from '@api/manage';
export const baseSearchMixin = {
  data() {
    return {
      showSearchForm: false,
      advancedTagParam: [], //筛选条件的参数
      advancedSearchRefName: 'AdvancedSearch', // 高级查询ref名称
      isActiveAllLookup: false, // 标记通用代码是否已查询
      realAdvancedSearchFormData: [], // 处理传入的表单数据: 添加默认空值;
      formColLayout: {
        // 高级查询通用四列布局定义
        cols: { xs: 12, sm: 8, lg: 8, xl: 6 },
        labelCol: { xs: 8, sm: 12, lg: 12, xl: 12 },
        wrapperCol: { xs: 16, sm: 12, lg: 12, xl: 12 },
        fixed: {
          // 高级查询通用跨四列布局定义
          cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
          labelCol: { xs: 4, sm: 3, lg: 3, xl: 3 },
          wrapperCol: { xs: 20, sm: 21, lg: 21, xl: 21 }
        }
      },
      formColLayout2: {
        // 高级查询通用跨两列布局定义
        cols: { xs: 24, sm: 24, lg: 16, xl: 12 },
        labelCol: { xs: 4, sm: 4, lg: 6, xl: 6 },
        wrapperCol: { xs: 20, sm: 20, lg: 18, xl: 18 }
      },
      formColLayout3: {
        // 高级查询通用跨三列布局定义
        cols: { xs: 24, sm: 24, lg: 24, xl: 18 },
        labelCol: { xs: 4, sm: 4, lg: 4, xl: 4 },
        wrapperCol: { xs: 20, sm: 20, lg: 20, xl: 20 }
      }
    };
  },
  components: {
    AvicAdvancedSearchCollapse
  },
  mounted() {
    this.realAdvancedSearchFormData = this.initRealAdvancedSearchFormData();
  },
  methods: {
    moment,
    formatDate,
    formatNumber,
    formatDisabledDate,
    validateCheckboxMaxlength,
    validateInputMaxlength,
    ...mapGetters(['userInfo']), // 获取通用代码时get 用户的id

    /**
     * 根据页面数据配置获取高级查询表单配置数据
     */
    getAdvancedSearchFormData() {
      let pageSettingData = [...this.pageSettingData];
      let advancedSearchFormData = [];
      for (let i = 0; i < pageSettingData.length; i++) {
        advancedSearchFormData.push(pageSettingData[i]);
      }
      return advancedSearchFormData;
    },
    /**
     * 接收高级查询数据,并处理
     * @param {Object} params 形式: {isDeal:isDeal(是否刷新表格), advancedSearchParam: advancedSearchParam(高级查询数据)}
     */
    advancedSearch(params) {
      // 接收高级查询数据,isDeal为true,则刷新表格,且从第一页开始
      if (!params) return;
      this.advancedSearchParam = params.param.advancedSearchParam;
      if (params.param.type === 'search' || params.param.type === 'reset') {
        this.isSearchByKeyWord = false;
        this.isAdvancedSearch = true;
      }
      if (params.param.isDeal) {
        this.reloadData(true, params.refName);
      }
    },
    /**
     * 点击高级查询按钮, 打开或者关闭高级查询form 表单
     * @param {String} refName 高级查询绑定的表格refName
     */
    handleAdvancedSearch(refName) {
      this.$refs[this.advancedSearchRefName].handleAdvancedSearch(refName);
      this.handleOnOff();
    },
    /**
     * 展开/收缩、查询、关闭、重置时的类型返回
     * @param {String} showSearchForm 高级查询框展开/收缩
     */
    changeShowAdvancedSearch(showSearchForm, type) {
      this.showSearchForm = showSearchForm;
      if (type === 'search') this.searchText = null; // 基本查询参数设为null
    },
    // 隐藏高级查询区和tag区
    hideAdvanceSearch() {
      if (this.$refs[this.advancedSearchRefName]) {
        this.$refs[this.advancedSearchRefName].hideAdvanceSearch();
      }
    },
    // 判断是否需要获取通用代码
    handleOnOff() {
      //lookupParams 为manage 页面中定义的通用代码
      if (this.lookupParams && this.lookupParams.length > 0) {
        // 表单中含有通用代码
        if (this.showSearchForm && !this.isActiveAllLookup) {
          this.getAllLookup();
        }
      }
    },
    // 获取通用代码,将结果写入高级查询表单设置
    getAllLookup() {
      loadLookUp(
        this.lookupParams,
        data => {
          // 将通用代码返回值写入表单数据中
          for (let i = 0; i < data.length; i++) {
            for (let j = 0; j < this.lookupParams.length; j++) {
              if (data[i].fileName === this.lookupParams[j].fileName) {
                for (let k = 0; k < this.realAdvancedSearchFormData.length; k++) {
                  if (
                    this.realAdvancedSearchFormData[k].dataIndex === this.lookupParams[j].fileName
                  ) {
                    if (this.lookupParams[j].fileName !== 'secretLevel') {
                      this.realAdvancedSearchFormData[k].list = data[i].value; // 拿到通用代码,赋给相应的通用代码数组
                      this[this.lookupParams[j].fileName + 'List'] = data[i].value;
                    } else {
                      httpAction(
                        '/api/appsys/lookup/LookupRest/getUserSecretWordList/v1/' +
                          this.userInfo().id,
                        '',
                        'get'
                      ) // 提交
                        .then(res => {
                          if (res.success) {
                            this.realAdvancedSearchFormData[k].list = res.result;
                            this[this.lookupParams[j].fileName + 'List'] = res.result;
                          } else {
                            this.$message.error('读取文档密级选项失败！');
                          }
                        })
                        .catch(e => {
                          console.error(e);
                        });
                    }
                  }
                }
                this.isActiveAllLookup = true;
              }
            }
          }
        },
        error => {
          // 提示通用代码获取失败
          this.$message.error('通用代码获取失败，请重新打开！');
        }
      );
    },
    // 处理高级查询表单参数
    initRealAdvancedSearchFormData() {
      // 处理高级查询表单参数:如果未传值,给默认空值;如果有日期格式字段,则去掉日期字段同时加上对应的日期区间字段,时间字段同理.
      let realAdvancedSearchFormData = [];
      for (let i = 0; i < this.advancedSearchFormData.length; i++) {
        let advancedSearchFormData = { ...this.advancedSearchFormData[i] };
        advancedSearchFormData.title =
          advancedSearchFormData.title || advancedSearchFormData.slots.content;
        advancedSearchFormData.decorator = advancedSearchFormData.decorator || {};
        if (advancedSearchFormData.type === 'date' || advancedSearchFormData.type === 'datetime') {
          realAdvancedSearchFormData.push({
            title: advancedSearchFormData.title,
            dataIndex: advancedSearchFormData.dataIndex,
            type: advancedSearchFormData.type + 'range',
            placeholder: advancedSearchFormData.placeholderRange || ['开始', '结束'],
            format: advancedSearchFormData.format,
            submitFormat: advancedSearchFormData.submitFormat || ''
          });
          realAdvancedSearchFormData.push({
            title: advancedSearchFormData.title + '开始时间',
            dataIndex: advancedSearchFormData.dataIndex + 'Begin',
            type: advancedSearchFormData.type,
            placeholder: advancedSearchFormData.placeholderRange || ['开始', '结束'],
            format: advancedSearchFormData.format,
            submitFormat: advancedSearchFormData.submitFormat || ''
          });
          realAdvancedSearchFormData.push({
            title: advancedSearchFormData.title + '结束时间',
            dataIndex: advancedSearchFormData.dataIndex + 'End',
            type: advancedSearchFormData.type,
            placeholder: advancedSearchFormData.placeholderRange || ['开始', '结束'],
            format: advancedSearchFormData.format,
            submitFormat: advancedSearchFormData.submitFormat || ''
          });
        } else {
          realAdvancedSearchFormData.push(advancedSearchFormData);
        }
      }
      return realAdvancedSearchFormData;
    },
    // 点击回车进行查询
    handleEnterForSearch() {
      this.$nextTick(function () {
        this.$refs[this.advancedSearchRefName].handleAdvancedSearchConfirm();
        if (this.searchText !== null || this.searchText !== '') this.searchText = null;
      });
    },
    // 设置查询按钮获取焦点
    setSearchFocus() {
      this.$nextTick(function () {
        this.$refs[this.advancedSearchRefName].setSearchFocus();
        if (this.searchText !== null || this.searchText !== '') this.searchText = null;
      });
    },
    // 开始时间处理，要大于结束时间
    disabledStartDate(startValue, dataindex) {
      const endValue = this.$refs[this.advancedSearchRefName].searchForm.getFieldValue(dataindex);
      if (!startValue || !endValue) {
        let year = moment().year() - 1;
        return startValue && startValue < moment().subtract(year, 'years');
      }
      return startValue.valueOf() > endValue.valueOf();
    },
    // 结束时间处理，要小于开始时间
    disabledEndDate(endValue, dataindex) {
      const startValue = this.$refs[this.advancedSearchRefName].searchForm.getFieldValue(dataindex);
      if (!endValue || !startValue) {
        let year = moment().year() - 1;
        return endValue && endValue < moment().subtract(year, 'years');
      }
      return startValue.valueOf() >= endValue.valueOf();
    }
  },
  computed: {
    /**
     * 获取高级查询配置数据
     */
    advancedSearchFormData() {
      return this.getAdvancedSearchFormData();
    }
  }
};
