import config from '@config/defaultSettings';

/**
 * 走菜单，走权限控制
 * @type {[null,null]}
 */
export const asyncRouterMap = [
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
];

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: () => import('@portal/pageLayouts/UserLayout'),
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import('@/views/login/Login')
      }
    ]
  },
  {
    path: '/flowPublicDetail/:id',
    component: () => import('@/views/avic/pt/system/flowPublicDetail/index')
  },
  {
    path: '/routerContentLoader/:id',
    component: () => import('@/views/avic/pt/system/routerContentLoader/index')
  },
  // 菜单管理移植到控制台
  {
    path: '/ms/menumanage',
    component: () => import('@/views/avic/pt/system/menu/MenuTreeTable')
  },
  // 多栏目门户移植到控制台
  {
    path: '/ms/PortalConfigDesignManage',
    component: () => import('@/views/avic/pt/system/portLet/portalConfig/PortalConfigDesignManage')
  },
  {
    path: '/404',
    component: () => import('@/views/exception/404')
  },
  {
    path: '/',
    component: () => import('@/views/avic/pt/portal/theme/LayoutIndex'),
    redirect: '/dashboard',
    children: [
      {
        redirect: null,
        path: config.homePageConfig.path,
        component: config.homePageConfig.component,
        route: '1',
        meta: {
          keepAlive: true,
          icon: config.homePageConfig.icon,
          title: config.homePageConfig.title
        },
        name: config.homePageConfig.name
      },
      {
        path: '/newsManage',
        component: () => import('@/views/avic/pt/system/messages/NewsManage'),
        meta: { keepAlive: true, title: '消息通知' },
        name: 'newsManage'
      },
      // 个人设置
      {
        path: '/usersetting/:tags',
        name: 'UserSetting',
        meta: { id: 'UserSetting', keepAlive: true, title: '个人设置' },
        component: () => import('@/views/avic/pt/system/personalSetting/UserSetttingPage')
      }
    ]
  }
];
