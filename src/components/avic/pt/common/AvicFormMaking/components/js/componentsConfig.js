import { columnsData } from './columnsConfig.js';
/**
 *模板列表的布局数据
 */
export const gridlayoutComponents = [
  {
    type: 'grid',
    name: 'grid1',
    icon: 'icon-grid-',
    columns: columnsData.columns1
  },
  {
    type: 'grid',
    name: 'grid255',
    icon: 'icon-grid-',
    columns: columnsData.columns2
  }
];
