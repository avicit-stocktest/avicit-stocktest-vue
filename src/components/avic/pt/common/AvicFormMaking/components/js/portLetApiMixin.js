import { httpAction } from '@/api/manage';
import { message } from 'ant-design-vue';

export const portLetApiMixin = {
  data() {
    return {
      deleteUrl: '/api/appsys/portlet/portalConfig/delete/v1/',
      getPagesUrl: '/api/appsys/portlet/portalConfig/togGetPortalAuth/v1'
    };
  },
  methods: {
    /*删除门户小页*/
    deletePortlet(id) {
      if (id) {
        let url = this.deleteUrl + '/' + id;
        httpAction(url, null, 'post').then(res => {
          if (res.success) {
            message.success('删除成功!');
            this.$emit('updatePortLetList');
          } else {
            message.error('删除失败!');
          }
        });
      } else {
        message.warning('缺少ID!');
      }
    },
    /*获取门户小页列表*/
    getBasicComponents(success) {
      httpAction(this.getPagesUrl, null, 'post').then(res => {
        if (res.success) {
          if (success) {
            let data = [];
            res.result.map(tim => {
              if (tim != null) {
                data.push(tim);
              }
            });
            success(data);
          }
        }
      });
    }
  }
};
