<template>
  <a-layout class="fm-container">
    <!--设计器顶部start-->
    <a-layout-header class="fm-header-bar" style="height: 50px">
      <a-row>
        <a-col :span="12">
          <h3 v-if="!isEditName">
            <div
              style="
                max-width: 90%;
                overflow-wrap: break-word;
                display: inline-block;
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
                margin-right: 5px;
              "
              :title="portletName"
            >
              【{{ portletName }}】设置
            </div>
            <span
              style="
                display: inline-block;
                line-height: 50px;
                vertical-align: text-bottom;
                color: #999;
              "
            >
              <a-icon type="edit" class="theme-main-hover-color" @click="handleEditTitle" />
            </span>
          </h3>
          <div class="edit-title" v-else>
            <a-input v-model="portletName" style="width: 180px" :max-length="50" />
            <a-button
              type="primary"
              @click="onSavePortalTitle"
              :style="btnStyle"
              :loading="editNameBtnLoading"
            >
              确定
            </a-button>
          </div>
        </a-col>
        <a-col :span="12">
          <div class="btn-group">
            <a-button type="primary" :style="btnStyle" @click="savePortalDesign"> 保存 </a-button>
            <a-button v-if="resourceType" :style="btnStyle" @click="handleCommand">
              {{ $t('fm.actions.deletPort') }}
            </a-button>
            <a-button v-if="clearable" :style="btnStyle" @click="handleClear">
              {{ $t('fm.actions.clear') }}
            </a-button>
            <a-button v-if="preview" :style="btnStyle" @click="handlePreview">
              {{ $t('fm.actions.preview') }}
            </a-button>
            <!--  <a-button v-if="generateJson" :style="btnStyle" @click="handleGenerateJson">
              {{ $t('fm.actions.json') }}
            </a-button>-->
            <a-button :style="btnStyle" @click="closeModal"> 取消 </a-button>
          </div>
        </a-col>
      </a-row>
    </a-layout-header>
    <!--设计器内容区-->
    <a-layout class="fm-container-center">
      <!--门户小页右边栏-->
      <a-layout-sider width="200px" v-model="leftCollapsed" :collapsed-width="0">
        <div
          class="drawer-left"
          :class="{ leftactive: isleftActive, leftclose: isleftClose }"
          @click="clickBtn('left')"
        >
          <a-icon :type="isleftClose ? 'right' : 'left'" />
        </div>
        <div class="fm-container-center-left">
          <!-- 基础组件 -->
          <template v-if="basicFields.length">
            <div class="fm-left-title">{{ $t('fm.components.portletPage.title') }}</div>
            <draggable
              tag="ul"
              :list="basicComponents"
              v-bind="{
                group: { name: 'people', pull: 'clone', put: false },
                sort: false,
                ghostClass: 'ghost'
              }"
              @end="handleMoveEnd"
              @start="handleMoveStart"
              :move="handleMove"
            >
              <template v-for="(item, index) in basicComponents">
                <li
                  v-if="basicFields.indexOf(item.type) >= 0"
                  class="fm-left-li-list"
                  :class="{ 'no-put': item.type == 'divider' }"
                  :key="index"
                >
                  <a>
                    <span>{{ item.name }}</span> <a-icon class="drag-icon" type="drag" />
                  </a>
                </li>
              </template>
            </draggable>
          </template>
          <template v-if="layoutFields.length">
            <div class="fm-left-title">{{ $t('fm.components.portletPage.layoutTemp') }}</div>
            <ul>
              <template v-for="(item, index) in gridlayoutComponents">
                <li
                  v-if="layoutFields.indexOf(item.type) >= 0"
                  class="fm-left-li-list no-put fm-left-li-modeltemp"
                  :key="index"
                  @click="handleAddTemp(item)"
                >
                  <a>
                    <span>{{ item.name }}</span>
                  </a>
                </li>
              </template>
            </ul>
          </template>
        </div>
      </a-layout-sider>
      <!--设计区-->
      <a-layout-content :class="{ 'widget-empty': widgetForm.list.length == 0 }">
        <widget-form
          v-if="!resetJson"
          ref="widgetForm"
          :data="widgetForm"
          :select.sync="widgetFormSelect"
          @handleWidgetOpensSet="clickBtn('right')"
        />
      </a-layout-content>
      <!--属性设置区-->
      <a-layout-sider
        width="200px"
        class="fm-widget-config-container drawer-box-right"
        :class="{ draweractive: isActive, drawerclose: isClose }"
        v-model="rightCollapsed"
        :collapsed-width="0"
        :default-collapsed="true"
      >
        <a-layout>
          <div
            class="drawer-handle"
            :class="{ rightactive: isActive, rightclose: isClose }"
            @click="clickBtn('right')"
          >
            <a-icon class="" :type="isClose ? 'left' : 'right'" />
          </div>
          <a-layout-header class="fm-header-bar" style="height: 45px">
            <div
              class="fm-config-tab"
              :class="{ active: configTab == 'widget' }"
              @click="handleConfigSelect('widget')"
            >
              {{ $t('fm.config.widget.title') }}
            </div>
          </a-layout-header>
          <a-layout-content class="fm-config-content">
            <widget-config v-show="configTab == 'widget'" :data.sync="widgetFormSelect" />
          </a-layout-content>
        </a-layout>
      </a-layout-sider>
    </a-layout>
    <!--预览弹窗-->
    <cus-dialog
      :visible="previewVisible"
      title="预览"
      :action="false"
      @on-close="previewVisible = false"
      ref="widgetPreview"
    >
      <generate-form
        @on-change="handleDataChange"
        v-if="previewVisible"
        :data="widgetForm"
        ref="generateForm"
      />
    </cus-dialog>
    <!--生成JSon弹窗-->
    <cus-dialog
      :visible="jsonVisible"
      title="JSON预览"
      @on-close="jsonVisible = false"
      ref="jsonPreview"
    >
      <div id="jsoneditor" style="height: 400px; width: 100%">{{ jsonTemplate }}</div>

      <template slot="action">
        <a-button type="primary" class="json-btn" :data-clipboard-text="jsonCopyValue">
          {{ $t('fm.actions.copyData') }}
        </a-button>
      </template>
    </cus-dialog>
  </a-layout>
</template>

<script>
import Draggable from 'vuedraggable';
import WidgetConfig from './WidgetConfig';
import WidgetForm from './WidgetForm';
import CusDialog from './CusDialog';
import GenerateForm from './GenerateForm';
//import Clipboard from 'clipboard';
import { gridlayoutComponents } from './js/componentsConfig.js';
import { portLetApiMixin } from './js/portLetApiMixin.js';
import { httpAction } from '@api/manage';
export default {
  name: 'FmMakingForm',
  components: {
    Draggable,
    WidgetConfig,
    WidgetForm,
    CusDialog,
    GenerateForm
  },
  props: {
    preview: {
      type: Boolean,
      default: false
    },
    generateCode: {
      type: Boolean,
      default: false
    },
    generateJson: {
      type: Boolean,
      default: false
    },
    upload: {
      type: Boolean,
      default: false
    },
    clearable: {
      type: Boolean,
      default: false
    },
    basicFields: {
      type: Array,
      default: () => [
        'input',
        'textarea',
        'number',
        'radio',
        'checkbox',
        'time',
        'date',
        'rate',
        'color',
        'select',
        'switch',
        'slider',
        'text',
        'text2',
        'bpmtask',
        'smallPage'
      ]
    },
    advanceFields: {
      type: Array,
      default: () => ['blank', 'imgupload', 'editor', 'cascader', 'avictable', 'avicnodata']
    },
    layoutFields: {
      type: Array,
      default: () => ['grid']
    }
  },
  data() {
    return {
      isEditName: false,
      editNameBtnLoading: false,
      portletName: '',
      basicComponents: [],
      gridlayoutComponents,
      resetJson: false,
      widgetForm: {
        list: [],
        config: {
          gutter: '15px',
          labelWidth: 100,
          labelPosition: 'right',
          size: 'small'
        }
      },
      configTab: 'widget',
      widgetFormSelect: null,
      previewVisible: false,
      jsonVisible: false,
      codeVisible: false,
      uploadVisible: false,
      drawerVisible: true,
      blank: '',
      htmlTemplate: '',
      vueTemplate: '',
      jsonTemplate: '',
      uploadEditor: null,
      jsonCopyValue: '',
      jsonClipboard: null,
      jsonEg: `{
         "list": [],
         "config": {
         "labelWidth": 100,
         "labelPosition": "top",
         "size": "small"
         }
      }`,
      codeActiveName: 'vue',
      isActive: false,
      isClose: false,
      isleftActive: false,
      isleftClose: false,
      leftCollapsed: false,
      rightCollapsed: true,
      btnStyle: { marginLeft: '10px' },
      editParam: {
        // 编辑弹窗保存时的请求接口
        url: '/api/appsys/portlet/portalConfig/update/v1',
        method: 'post'
      }
    };
  },
  mixins: [portLetApiMixin],
  mounted() {
    this._loadComponents();
    this.portletName = this.$attrs['port-let'].portletName;
  },
  computed: {
    resourceType() {
      if (this.$attrs['port-let'] && this.$attrs['port-let'].resourceType == '2') {
        return true;
      } else {
        return false;
      }
    },
    isDefault() {
      if (this.$attrs['port-let'] && this.$attrs['port-let'].isDefault == '0') {
        return true;
      } else {
        return false;
      }
    }
  },
  methods: {
    handleEditTitle() {
      this.isEditName = true;
    },
    onSavePortalTitle() {
      this.isEditName = false;
      this.editNameBtnLoading = true;
      let newValues = {
        id: this.$attrs['port-let'].id,
        portletName: this.portletName
      };
      httpAction(this.editParam.url, newValues, this.editParam.method) // 提交
        .then(res => {
          if (res.success) {
            this.$message.success('操作成功！');
            this.$emit('updatePortLetList', true); // 更新列表，不关闭设计器窗口
            this.isEditName = false;
          } else {
            this.$message.warning(res.message);
          }
          this.editNameBtnLoading = false;
        })
        .catch(e => {
          this.$message.error('操作失败！');
          this.editNameBtnLoading = false;
        });
    },
    _loadComponents() {
      this.getBasicComponents(res => {
        this.basicComponents = res.map((item, index) => {
          return {
            type: 'smallPage', // 组件类型，保持唯一
            name: item.menuName,
            url: item.menuUrl,
            icon: 'icon-input', //组件展示icon, 如果需要自定义，请参考 如何自定义图标
            options: {
              // 组件配置信息，根据自定义组件自己添加配置
              title: item.menuName, //标题
              titleRequired: item.showTitle === '1' ? true : false, //显示标题
              moreUrl: item.moreUrl, //跳转路由组件地址
              menuPath: item.menuPath, //路由路径
              menuRouteName: item.menuRouteName, //路由名称
              refresh: item.refresh //刷线频率
            },
            x: 0,
            y: 0,
            w: 12,
            h: item.height != null ? item.height / 10 : 20,
            i: index + '',
            id: item.id
          };
        });
      });
      this.gridlayoutComponents = this.gridlayoutComponents.map(item => {
        return {
          ...item,
          name: this.$t(`fm.components.portletPage.grid.${item.name}`)
        };
      });
    },
    handleConfigSelect(value) {
      this.configTab = value;
    },
    handleMoveEnd(evt) {
      console.log('end', evt);
    },
    handleMoveStart({ oldIndex }) {
      console.log('start', oldIndex, this.basicComponents);
    },
    handleMove() {
      return true;
    },
    handlePreview() {
      console.log(this.widgetForm);
      this.previewVisible = true;
    },
    handleClose() {
      this.previewVisible = false;
    },
    /* 开发使用生成 JSON 数据
    handleGenerateJson() {
      this.jsonVisible = true;
      this.jsonTemplate = this.widgetForm;
      this.$nextTick(() => {
        const editor = ace.edit('jsoneditor');
        editor.session.setMode('ace/mode/json');
        if (!this.jsonClipboard) {
          this.jsonClipboard = new Clipboard('.json-btn');
          this.jsonClipboard.on('success', e => {
            this.$message.success(this.$t('fm.message.copySuccess'));
          });
        }
        this.jsonCopyValue = JSON.stringify(this.widgetForm);
      });
    },*/

    handleClear() {
      let obj = {
        list: [],
        config: {
          ...this.widgetForm.config
        }
      };
      this.widgetForm = obj;
      this.widgetFormSelect = {};
    },
    setJSON(json) {
      this.widgetForm = json;

      if (json.list.length > 0) {
        this.widgetFormSelect = json.list[0];
      }
    },
    handleDataChange(field, value, data) {
      console.log(field, value, data);
    },
    handleAddTemp(item) {
      let widgetData = JSON.parse(item.columns);
      if (widgetData.list.length > 0) {
        //权限判断
        let wlen = widgetData.list.length;
        let bcLen = this.basicComponents.length;
        for (let i = 0; i < wlen; i++) {
          let isSwith = false;
          for (let j = 0; j < bcLen; j++) {
            if (widgetData.list[i].id == this.basicComponents[j].id) {
              isSwith = true;
            }
          }
          if (!isSwith) {
            widgetData.list[i].url = 'avic/pt/system/portLet/portLetCards/401';
          }
        }
      }
      this.setJSON(widgetData);
    },
    handleCommand() {
      let that = this;
      this.$confirm({
        title: '提示',
        content: '确认删除此门户吗 ?',
        closable: true,
        onOk() {
          return that.deletePortlet(that.$attrs['port-let'].id);
        },
        onCancel() {}
      });
    },
    clickBtn(type) {
      if (type == 'right') {
        if (this.isActive) {
          this.isClose = !this.isClose;
        } else {
          this.isActive = true;
        }
        this.rightCollapsed = !this.rightCollapsed;
      } else if (type == 'left') {
        this.leftCollapsed = !this.leftCollapsed;
        this.isleftActive = true;
        this.isleftClose = !this.isleftClose;
      }
    },
    closeModal() {
      this.$emit('closeModal');
    },
    savePortalDesign() {
      this.$emit('savePortalDesign');
    }
  },
  watch: {
    widgetForm: {
      deep: true,
      handler(val) {
        this.$emit('updateDesign', val);
      }
    },
    $lang() {
      this._loadComponents();
    }
  }
};
</script>

<style lang="less" scoped>
.widget-empty {
  background-position: 50%;
}
/* 收缩功能 start */
.drawer-handle {
  position: absolute;
  right: 0;
  width: 15px;
  top: 50%;
  height: 40px;
  z-index: 99999;
  padding: 10px 0;
  background: #fff;
  cursor: pointer;
  border-bottom-left-radius: 3px;
  border-top-left-radius: 3px;
  box-shadow: -4px 0 4px 0 #ddd;
  i {
    color: #666;
  }
}
.drawer-left {
  position: absolute;
  left: 200px;
  width: 15px;
  top: 50%;
  height: 40px;
  z-index: 99999;
  padding: 10px 0;
  background: #fff;
  cursor: pointer;
  border-bottom-right-radius: 3px;
  border-top-right-radius: 3px;
  i {
    color: #666;
  }
}
.drawer-box-left {
  position: static !important;
  background: #fff;
  left: 0;
  bottom: 0;
  top: 0;
  border: 0 solid #ddd;
  border-top: 0;
}
.drawer-box-right {
  position: absolute;
  background: #fff;
  right: 0;
  bottom: 0;
  top: 0;
  border: 0 solid #ddd;
  border-top: 0;
  box-shadow: -2px 0 6px 0 #ddd;
}
.draweractive {
  animation: opendoor 0.4s normal forwards;
}

@keyframes opendoor {
  from {
    width: 0;
  }
  to {
    width: 200px;
  }
}
// 关
.drawerclose {
  animation: close 0.4s normal forwards;
}

@keyframes close {
  0% {
    width: 200px;
  }
  100% {
    width: 0;
  }
}
.rightactive {
  animation: rhandleopen 0.2s normal forwards;
}
.rightclose {
  animation: rhandleclose 0.2s normal forwards;
}
.leftactive {
  animation: lhandleopen 0.2s normal forwards;
}
.leftclose {
  animation: lhandleclose 0.2s normal forwards;
}

@keyframes rhandleclose {
  0% {
    right: 200px;
  }
  100% {
    right: 0;
  }
}

@keyframes rhandleopen {
  0% {
    right: 0;
  }
  100% {
    right: 200px;
  }
}

@keyframes lhandleclose {
  0% {
    left: 200px;
  }
  100% {
    left: 0;
  }
}

@keyframes lhandleopen {
  0% {
    left: 0;
  }
  100% {
    left: 200px;
  }
}
/* 收缩功能 end */

/* antd 样式覆盖start */
.fm-container {
  .ant-layout-sider,
  .ant-layout-header {
    background: #fff !important;
  }
  .ant-layout-header {
    padding: 0;
    line-height: 45px;
    border-bottom: 1px solid #ddd;

    .edit-title {
      text-align: left;
      padding: 0 10px;
    }
    .btn-group {
      text-align: right;
      padding: 0 10px;
    }
  }
  .ant-layout-content {
    overflow: auto;
  }
  .fm-title {
    width: 100%;
    display: block;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }
}
/* antd 样式end */
</style>
