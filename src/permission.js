import Vue from 'vue';
import router from './router';
import store from './store';
import NProgress from 'nprogress'; // progress bar
import 'nprogress/nprogress.css'; // progress bar style
import notification from 'ant-design-vue/es/notification';
import modal from 'ant-design-vue/es/modal';
import config from '@config/defaultSettings';

import {
  ACCESS_TOKEN,
  USER_PERMISSIONLIST,
  USER_NAME,
  ACCESS_KEY_ID,
  ACCESS_KEY_SECRET,
  SESSION_ID
} from '@/store/mutationTypes';
import { generateIndexRouter } from '@/utils/avic/common/util';

NProgress.configure({
  showSpinner: false
}); // NProgress Configuration

const whiteList = config.whiteList; // no redirect whitelist
whiteList.push('/404');

let formatRouter = (menuData, to, from, next) => {
  let constRoutes = [];
  let redirect = decodeURIComponent(from.query.redirect || to.path);
  constRoutes = generateIndexRouter(menuData);
  // 添加主界面路由
  store
    .dispatch('UpdateAppRouter', {
      constRoutes
    })
    .then(() => {
      // 根据roles权限生成可访问的路由表
      // 动态添加可访问路由表
      router.addRoutes(store.getters.addRouters);

      if (to.path === redirect) {
        // hack方法 确保addRoutes已完成 ,set the replace: true so the navigation will not leave a history record
        next({
          ...to,
          replace: true
        });
        // next()
      } else {
        // 跳转到目的路由
        next({
          path: redirect
        });
      }
    });
};

router.beforeEach((to, from, next) => {
  NProgress.start(); // start progress bar

  if (to.query[SESSION_ID]) {
    // 免密登录状态变化时重新登录
    Vue.ls.remove(ACCESS_TOKEN);
  }

  if (Vue.ls.get(ACCESS_TOKEN)) {
    // 检查字典更新
    if (
      window._CONFIG['permissionChangeAlert'] === false &&
      to.path.indexOf('/flowPublicDetail/') === -1
    ) {
      store.dispatch('CheckPermissionStatue').then(res => {
        window._CONFIG['permissionChangeAlert'] = true;
        modal.warning({
          title: '系统提示',
          content: '权限已更新，请重新登录！',
          okText: '确定',
          onOk: () => {
            store.dispatch('Logout').then(() => {
              Vue.ls.remove(USER_PERMISSIONLIST);
              window._CONFIG['permissionChangeAlert'] = false;
              // todo 免刷新退回登录页 by qym
              // next({
              //   path: '/user/login',
              //   query: {
              //     redirect: to.fullPath
              //   }
              // })
              window.location.reload();
            });
          }
        });
      });
    }
    /* 已登录 */
    if (to.path === '/user/login') {
      next({
        path: config.homePageConfig.path
      });
      NProgress.done();
      if (document.getElementById('loaderWrapper')) {
        document.getElementById('loaderWrapper').remove();
      }
    } else {
      // 加载用户个性化配置数据 BY CJ
      store.dispatch('loadCustomCfg');
      if (!store.customCfg && to.path.indexOf('/flowPublicDetail/') === -1) {
        store.dispatch('loadPortlet');
        store.dispatch('loadPlatformConfig');
      }
      if (store.getters.permissionList.length === 0) {
        let userPermissionObj = Vue.ls.get(USER_PERMISSIONLIST);
        let username = Vue.ls.get(USER_NAME);
        if (userPermissionObj && userPermissionObj[username]) {
          store.dispatch('SetLocalPermission', userPermissionObj[username]);
          formatRouter(userPermissionObj[username], to, from, next);
        } else {
          // 清除他人字典
          Vue.ls.remove(USER_PERMISSIONLIST);

          store
            .dispatch('GetPermissionList')
            .then(res => {
              const menuData = res.result;
              // console.log(res.message)
              if (menuData === null || menuData === '' || menuData === undefined) {
                return;
              }
              formatRouter(menuData, to, from, next);
            })
            .catch(() => {
              notification.error({
                message: '系统提示',
                description: '请求用户权限信息失败，请重试！'
              });
              store.dispatch('Logout').then(() => {
                next({
                  path: '/user/login',
                  query: {
                    redirect: to.fullPath
                  }
                });
              });
            });
        }
      } else {
        if (
          !store.state.bpmEngine.installedBpmEditor &&
          !store.state.bpmEngine.installingBpmEditor
        ) {
          store.commit('installModule', 'bpmEditor');
          import('@/store/modules/bpm/bpmEditor').then(res => {
            store.registerModule(['bpmEditor'], res.default);
            store.commit('installedModule', 'bpmEditor');
          });
        }
        if (!store.state.bpmEngine.installedBpmTabs && !store.state.bpmEngine.installingBpmTabs) {
          store.commit('installModule', 'bpmTabs');
          import('@/store/modules/bpm/bpmTabs').then(res => {
            store.registerModule(['bpmTabs'], res.default);
            store.commit('installedModule', 'bpmTabs');
          });
        }
        if (to.path.indexOf('/flowPublicDetail/') !== -1) {
          // if (!store.state.bpmEngine.installedBpmCommonSelect && !store.state.bpmEngine.installingBpmCommonSelect) {
          //   store.commit('installModule', 'bpmCommonSelect')
          //   import('@/store/modules/bpm/bpmCommonSelect').then(res => {
          //     store.registerModule(['bpmCommonSelect'], res.default)
          //     store.commit('installedModule', 'bpmCommonSelect')
          //   })
          // }
          if (
            !store.state.bpmEngine.installedBpmButHelp &&
            !store.state.bpmEngine.installingBpmButHelp
          ) {
            store.commit('installModule', 'bpmButHelp');
            import('@/store/modules/bpm/bpmButHelp').then(res => {
              store.registerModule(['bpmButHelp'], res.default);
              store.commit('installedModule', 'bpmButHelp');
            });
          }
          if (
            !store.state.bpmEngine.installedFlowNodeSelect &&
            !store.state.bpmEngine.installingFlowNodeSelect
          ) {
            store.commit('installModule', 'bpmNodeSelect');
            import('@/store/modules/bpm/bpmNodeSelect').then(res => {
              store.registerModule(['bpmNodeSelect'], res.default);
              store.commit('installedModule', 'bpmNodeSelect');
            });
          }
        }
        next();
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next();
    } else {
      if (to.path !== '/user/login') {
        if (!to.query[SESSION_ID]) {
          next({
            path: '/user/login',
            query: {
              redirect: to.fullPath
            }
          });
        } else {
          let param = {};
          param[SESSION_ID] = to.query[SESSION_ID];
          store
            .dispatch('AuthLogin', param)
            .then(res => {
              console.log(to.path);
              next({
                path: to.path
              });
            })
            .catch(err => {
              next({
                path: '/user/login',
                query: {
                  redirect: to.path
                }
              });
            });
        }
      } else if (whiteList.indexOf('/user/login') === -1) {
        next({
          path: '/404',
          query: {
            redirect: to.fullPath
          }
        });
      } else {
        next({
          path: '/user/login',
          query: {
            redirect: to.fullPath
          }
        });
      }
      NProgress.done();
      if (document.getElementById('loaderWrapper')) {
        document.getElementById('loaderWrapper').remove();
      }
    }
  }
});

router.afterEach((to, from) => {
  NProgress.done(); // finish progress bar
  if (document.getElementById('loaderWrapper')) {
    document.getElementById('loaderWrapper').remove();
  }

  if (to.path !== '/user/login' && to.meta && to.meta.id) {
    store.dispatch('saveVisitMenu', to.meta.id);
  }
});

/* 增加路由跳转错误的后置处理逻辑 By Cj */
router.onError(e => {
  /* 404相关处理逻辑已移至主题混入对象ThemeLayoutMixin中 */
  NProgress.done();
});
